# CARTO (Nutiteq) 3D data converters

**Note: you need special arrangement with CARTO to have support for this tools**

## About

To use 3D files in Nutiteq or CARTO Mobile SDK you need to convert data to either:

  * **NML** - individual models, you need to set location in code. Typical source: Collada DAE file (OpenCollada suggested). This file can be used as a vector object in VectorLayer, you can set coordinates, rotate, scale it, have click events etc.
  * **NMLDB** LODTree-based more complex structure, based on sqlite and suitable for many models with coordinates. Typical source: KMZ file. This file can be used as a Layer in MapView, so it is already geocoded and manipulations are more limited. However it is performance optimized for big datasets, like whole 3D city with thousands of objects. Also this structure can be streamed online from your server.

This project has converters into both formats.

# Install

* Get and install Visual C runtime package vcredist_x86.exe from [Microsoft downloads](https://www.microsoft.com/en-us/download/confirmation.aspx?id=5555)
* KML/KMZ converter **kmz2db** binaries: [converter/kmz2db/bin/win/Win32/Release](https://bitbucket.org/nutiteq/nlm/src/62880508db850c09065ca184962b2e8dd3ab1b4a/converter/kmz2db/bin/win/Win32/Release/?at=master)  . This does not have installer. Once you run the exe, then it gives short guide with available parameters.
* Collada DAE converter **dae2nml** binaries: [/converter/dae2nml/bin/win/Win32/Release/](https://bitbucket.org/nutiteq/nlm/src/62880508db850c09065ca184962b2e8dd3ab1b4a/converter/dae2nml/bin/win/Win32/Release/?at=master)


# Usage
## Convert KMZ (KML) to NMLDB

### For Android devices

Convert all kmz files in a folder:

```
#!sh
kmz2db --compress-rgb=etc1 --compress-rgba=etc1 --freeze --kmz2sqlite=XXX.nmldb --add-all  --ignore-height=1 *.kmz
```

* For 3D models, --compress-rgba=etc1 should be replaced with --compress-rgba=raw , if model contains transparent textures
* PVRTC textures are NOT supported!

### For iOS devices / apps

```
#!sh
kmz2db --compress-rgb=pvrtc1_4bpp --compress-rgba=pvrtc1_4bpp --compress-lodtree-meshes=1 --square-texture-atlas=1 --freeze --kmz2sqlite=XXX.nmldb --add-all XXX.kmz
```

* --compress-lodtree-meshes=1 option turns on mesh compression. It takes more time while converting, but produces smaller .nmldb files and makes runtime faster.
* --square-texture-atlas=1 turns on square-shaped texture atlas generation (iOS requires compressed textures to be squares)
* --compress-rgb=pvrtc1_4bpp and --compress-rgba=pvrtc1_4bpp options can be replaced with --compress-rgb=pvrtc1_2bpp and --compress-rgba=pvrtc1_2bpp. Output files are up to 2 times smaller but quality is worse.
* iOS SDK supports ETC1 textures, but they have to be uncompressed before use. This results in textures that take as much memory as raw textures but quality is considerably worse.

### For WP SDK:

```
#!sh
kmz2db --compress-rgb=dxtc1 --compress-rgba=dxtc3 --compress-lodtree-meshes=1 --freeze --kmz2sqlite=XXX.nmldb --add-all XXX.kmz
```

* --compress-lodtree-meshes=1 option turns on mesh compression. It takes more time while converting, but produces smaller .nmldb files and makes runtime faster.

## Converting DAE files to NML

In general same comments regarding --compress-* options apply to dae2nml also, for example PVRTC is not supported in Android Java SDK. Also --square-texture-atlas option should be used when converting models for iOS.

# Online 3D city server

Map SDK can load data from remote API using HTTP connection, using Nutiteq own 3D vector tile protocol.

For 3D city kmz2db is used, and data can be served via two possible data sources:

* **NMLDB file** - one file has one dataset, e.g. one city or app
* **PostgreSQL database** - one big database, with all application data in single big machine

API for client is same for both options, only how data is prepared and how server script is configured is different.

## Prerequisites
Server with at least 16G RAM. About 3x more HDD than set of KMZ files is suggested as minimum. Install LAPP (Linux, Apache, Php, PostgreSQL) software stack. It is tested with Ubuntu Linux, but same should work also on Windows server.

## Server scripts

* copy **nmlserver.php** to your web server, so it can be executed
* open the file and set data parameter values in the start of the file. Note that pgsql: is for PostgreSQL connections, sqlite:// for SQLite file connections.
* make sure nmlserver.php is accessible from web

## Server publishing checklist

* PHP with PostgreSQL and SQLite PDO drivers included and enabled
* Apache or NGIX web server, with PHP integrated
* PostgreSQL 9.3 or newer, no special extensions needed
* SQLite library with Spatialite extension (spatialite package for Ubuntu)
* In client side (SDK): make sure that you use your web URL in NMLModelOnlineLayer constructor.