package com.nutiteq.nmlpackage;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import com.nutiteq.glutils.Bounds3;
import com.nutiteq.glutils.Frustum3;
import com.nutiteq.glutils.LRUCache;
import com.nutiteq.glutils.Matrix4;
import com.nutiteq.nmlpackage.NMLLoader.MeshBinding;
import com.nutiteq.nmlpackage.NMLLoader.TextureBinding;

public class GLModelLODTreeRenderer {

  // Renderer configuration
  public static class Configuration {
    public int residentLevels; // how many lower model LOD levels to keep resident
    public int geometryCacheSize; // how much memory can be used for caching model geometry data
    public int textureCacheSize; // how much memory can be used for caching model texture data
    public static Configuration getDefault() {
      Configuration config = new Configuration();
      config.residentLevels = 1;
      config.geometryCacheSize = 2048 * 1024;
      config.textureCacheSize = 4096 * 1024;
      return config;
    }
  }

  private final int CACHE_FLUSH_TIME = 1000;
  private final float[] LIGHTING_DIR = new float[] { 0.25f, 0.5f, 1.0f };

  private Map<Integer, GLModel> mGLModelMap = new HashMap<Integer, GLModel>();
  private Map<Long, GLMesh> mGLMeshMap = new HashMap<Long, GLMesh>();
  private Map<Long, GLTexture> mGLTextureMap = new HashMap<Long, GLTexture>();

  private Map<Integer, GLModel> mGLModelCache;
  private LRUCache<Long, GLMesh> mGLMeshCache;
  private LRUCache<Long, GLTexture> mGLTextureCache;

  private long mCacheFlushTime;
  private long mDrawStartTime;
  private int mLoadCallCount;

  private float mLODSharpness = 1;
  private final int mResidentLevels;
  private final GLModelLODTree mGLModelLODTree;

  private final NMLLoader mLoader;
  private final Thread mLoaderThread;

  private static long encodeTextureIdLevel(long id, int level) {
    return (id << 4) + level;
  }

  private static long decodeTextureId(long idLevel) {
    return idLevel >> 4;
  }

  private static int decodeTextureLevel(long idLevel) {
    return (int) (idLevel & 15);
  }

  private static float getProjectedBoundsDistance(Bounds3 bounds, Matrix4 mvpMatrix) {
    return mvpMatrix.transformPoint(bounds.getCenter())[2];
  }

  private static float[] getProjectedBoundsSize(Bounds3 bounds, Matrix4 mvpMatrix) {
    float[] min = new float[] {  Float.MAX_VALUE,  Float.MAX_VALUE,  Float.MAX_VALUE };
    float[] max = new float[] { -Float.MAX_VALUE, -Float.MAX_VALUE, -Float.MAX_VALUE };
    float[][] boundPoints = new float[][]{ bounds.getMin(), bounds.getMax() };
    for (int i = 0; i < 8; i++) {
      float[] point = new float[] { 0, 0, 0, 1 };
      for (int j = 0; j < 3; j++) {
        point[j] = boundPoints[(i >> j) & 1][j];
      }

      float[] projPoint = mvpMatrix.transformPoint(point);

      for (int j = 0; j < 3; j++) {
        min[j] = Math.min(min[j], projPoint[j]);
        max[j] = Math.max(max[j], projPoint[j]);
      }
    }
    return new float[] { max[0] - min[0], max[1] - min[1], max[2] - min[2] };
  }

  private void setupLighting(GL10 gl, float[] lightDir) {
    final float[] ambientMaterial   = new float[] { 0.7f, 0.7f, 0.7f, 1.0f };
    final float[] diffuseMaterial   = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
    final float[] specularMaterial  = new float[] { 0.0f, 0.0f, 0.0f, 1.0f };
    final float[] lightModelAmbient = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
    final float[] lightPos = new float[] { lightDir[0], lightDir[1], lightDir[2], 0 };

    gl.glEnable(GL10.GL_LIGHTING);
    gl.glEnable(GL10.GL_LIGHT0);
    gl.glShadeModel(GL10.GL_SMOOTH);
    gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, lightPos, 0);
    gl.glLightModelfv(GL10.GL_LIGHT_MODEL_AMBIENT, lightModelAmbient, 0);
    gl.glLightModelx(GL10.GL_LIGHT_MODEL_TWO_SIDE, GL10.GL_TRUE);
    gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_AMBIENT,  ambientMaterial, 0);
    gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE,  diffuseMaterial, 0);
    gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SPECULAR, specularMaterial ,0);
  }

  private void resetLighting(GL10 gl) {
    gl.glDisable(GL10.GL_LIGHTING);
  }

  private boolean isLoadTimeExhausted() {
    if (mLoadCallCount > 1)
      return true;
    mLoadCallCount++;
    return false;
  }

  private void drawModel(GL10 gl, int modelId, GLMeshInstance.DrawMode mode) {
    GLModel model = mGLModelMap.get(modelId);
    if (model == null)
      return;
    model.draw(gl, mode);
  }

  private void drawModelLODTreeNode(GL10 gl, GLModelLODTreeNode node, final Frustum3 frustum, final Matrix4 mvpMatrix, GLMeshInstance.DrawMode mode) {
    if (node == null)
      return;

    // Clip, if node bounds outside of frustum, nothing to draw
    if (!frustum.isInside(node.getModelBounds()))
      return;

    // Prefetch this level. If the level is not completely loaded, we will request its data to be loaded
    prefetchModel(gl, node);

    // Find current node bounding box projected to frustum near plane
    float[] boundsSize = getProjectedBoundsSize(node.getNodeBounds(), mvpMatrix);

    // Decide whether to recurse or not - if node takes more screen space than approx. viewport, then recurse
    if (Math.max(boundsSize[0], boundsSize[1]) * mLODSharpness > 2 && !node.isLeaf()) {
      // Create sorted list of children, based on distance from the camera (drawing/loading closer nodes first)
      GLModelLODTreeNode[] children = new GLModelLODTreeNode[node.getChildCount()];
      for (int i = 0; i < node.getChildCount(); i++) {
        children[i] = mGLModelLODTree.getNode(node.getChild(i));
      }
      Arrays.sort(children, new Comparator<GLModelLODTreeNode>() {
        public int compare(GLModelLODTreeNode node1, GLModelLODTreeNode node2) {
          return Float.compare(getProjectedBoundsDistance(node1.getNodeBounds(), mvpMatrix), getProjectedBoundsDistance(node2.getNodeBounds(), mvpMatrix));
        }
      });

      // Test that next level is (almost) complete - draw child nodes only in this case
      boolean completed = true;
      for (GLModelLODTreeNode child : children) {
        if (child.getLevel() > mResidentLevels && !frustum.isInside(child.getModelBounds()))
          continue;

        if (!prefetchModel(gl, child))
          completed = false;
      }

      // Draw recursively if complete
      if (completed) {
        // Draw the sorted list in order
        for (GLModelLODTreeNode child : children) {
          drawModelLODTreeNode(gl, child, frustum, mvpMatrix, mode);
        }
        return;
      }
    }

    // Stop at this level. Preload and draw all models
    drawModel(gl, node.getModelId(), mode);
  }

  private boolean prefetchModel(GL10 gl, GLModelLODTreeNode node) {
    if (node == null)
      return false;
    
    boolean update = false;
    GLModel glModel = mGLModelCache.get(node.getModelId());
    if (glModel == null) {
      glModel = new GLModel();
      glModel.create(gl, node.getModel());
      update = true;
    }
    mGLModelMap.put(node.getModelId(), glModel);

    boolean completed = true;
    for (MeshBinding binding : node.getMeshBindings()) {
      if (!prefetchMesh(gl, node, binding))
        completed = false;
    }
    for (TextureBinding binding : node.getTextureBindings()) {
      if (!prefetchTexture(gl, node, binding))
        completed = false;
    }
    
    if (update) {
      for (MeshBinding binding : node.getMeshBindings()) {
        GLMesh glMesh = mGLMeshMap.get(binding.meshId);
        if (glMesh != null)
          glModel.replaceMesh(gl, binding.localId, glMesh);
      }
      for (TextureBinding binding : node.getTextureBindings()) {
        long idLevel = encodeTextureIdLevel(binding.textureId, binding.level);
        GLTexture glTexture = mGLTextureMap.get(idLevel);
        if (glTexture != null)
          glModel.replaceTexture(gl, binding.localId, glTexture);
      }
    }
    return completed;
  }

  private boolean prefetchMesh(GL10 gl, GLModelLODTreeNode node, MeshBinding binding) {
    long id = binding.meshId;
    GLMesh glMesh = mGLMeshCache.get(id);
    if (glMesh != null) {
      mGLMeshMap.put(id, glMesh);
      return true;
    }

    // Check if model is already loaded but not yet registered
    NMLPackage.Mesh mesh = mLoader.getMesh(id); 
    if (mesh != null) {
      if (isLoadTimeExhausted())
        return false;

      // Create new GL model
      glMesh = new GLMesh();
      glMesh.create(gl, mesh);
      mGLMeshCache.put(id, glMesh, glMesh.getTotalGeometrySize());
      mGLMeshMap.put(id, glMesh);
      mLoader.removeMesh(id); // FIXME: wrong when mesh is shared

      // Update model mesh
      GLModel glModel = mGLModelMap.get(node.getModelId());
      if (glModel != null) {
        glModel.replaceMesh(gl, binding.localId, glMesh);
      }
      return true;
    }

    // Request model to be loaded
    mLoader.requestMesh(id);
    return false;
  }

  private boolean disposeMesh(GL10 gl, long id, GLMesh glMesh) {
    if (mGLMeshMap.get(id) != null)
      return false;
    glMesh.dispose(gl);
    return true;
  }

  private boolean prefetchTexture(GL10 gl, GLModelLODTreeNode node, TextureBinding binding) {
    long idLevel = encodeTextureIdLevel(binding.textureId, binding.level);
    GLTexture glTexture = mGLTextureCache.get(idLevel);
    if (glTexture != null) {
      mGLTextureMap.put(idLevel, glTexture);
      return true;
    }

    // Check if texture is already loaded but not registered
    NMLPackage.Texture texture = mLoader.getTexture(binding.textureId, binding.level);
    if (texture != null) {
      if (isLoadTimeExhausted())
        return false;

      // Create GLTexture
      glTexture = new GLTexture();
      glTexture.create(gl, texture);
      mGLTextureCache.put(idLevel, glTexture, glTexture.getTotalTextureSize());
      mGLTextureMap.put(idLevel, glTexture);
      mLoader.removeTexture(binding.textureId, binding.level);  // FIXME: wrong when texture is shared

      // Update model texture
      GLModel glModel = mGLModelMap.get(node.getModelId());
      if (glModel != null) {
        glModel.replaceTexture(gl, binding.localId, glTexture);
      }
      return true;
    }

    // Request texture to be loaded
    mLoader.requestTexture(binding.textureId, binding.level, true);
    return false;
  }

  private boolean disposeTexture(GL10 gl, long id, int level, GLTexture glTexture) {
    long idLevel = encodeTextureIdLevel(id, level);
    if (mGLTextureMap.get(idLevel) != null)
      return false;
    glTexture.dispose(gl);
    return true;
  }

  public GLModelLODTreeRenderer(GLModelLODTree glLODTree, NMLLoader loader, Configuration config) {
    mResidentLevels = config.residentLevels;
    mGLModelLODTree = glLODTree;
    mGLModelCache = new HashMap<Integer, GLModel>();
    mGLMeshCache = new LRUCache<Long, GLMesh>(config.geometryCacheSize);
    mGLTextureCache = new LRUCache<Long, GLTexture>(config.textureCacheSize);
    mLoader = loader;
    mLoaderThread = new Thread(mLoader);
    mLoaderThread.start();
  }

  public void setLODSharpness(float s) {
    mLODSharpness = s;
  }

  public void draw(final GL10 gl, GLMeshInstance.DrawMode mode) {
    mDrawStartTime = System.currentTimeMillis(); 
    mLoadCallCount = 0;

    // Read GL projection and modelview state 
    Matrix4 projMatrix = Matrix4.fromGLMatrix((GL11) gl, GL11.GL_PROJECTION_MATRIX);
    Matrix4 mvMatrix = Matrix4.fromGLMatrix((GL11) gl, GL11.GL_MODELVIEW_MATRIX);
    Frustum3 frustum = mvMatrix.getInverse().transformFrustum(Frustum3.fromGLProjectionMatrix(projMatrix));

    // Setup lighting
    if (mode == GLMeshInstance.DrawMode.DRAW_NORMAL) {
      setupLighting(gl, LIGHTING_DIR);
    }

    // Do actual drawing
    mGLModelMap.clear();
    mGLMeshMap.clear();
    mGLTextureMap.clear();
    drawModelLODTreeNode(gl, mGLModelLODTree.getNode(0), frustum, projMatrix.multiplyMatrix(mvMatrix), mode);

    // Clear loaded but unused data from caches, maps
    Map<Integer, GLModel> cacheMap = mGLModelCache;
    mGLModelCache = mGLModelMap;
    mGLModelMap = cacheMap;
    mGLMeshCache.evict(new LRUCache.Disposer<Long, GLMesh>() {
      public boolean dispose(Long id, GLMesh mesh) { return disposeMesh(gl, id, mesh); }
    });
    mGLTextureCache.evict(new LRUCache.Disposer<Long, GLTexture>() {
      public boolean dispose(Long idLevel, GLTexture texture) { return disposeTexture(gl, decodeTextureId(idLevel), decodeTextureLevel(idLevel), texture); }
    });
    if (mCacheFlushTime + CACHE_FLUSH_TIME < mDrawStartTime) {
      mLoader.flush();
      mCacheFlushTime = mDrawStartTime;
    }

    // Reset lighting
    if (mode == GLMeshInstance.DrawMode.DRAW_NORMAL) {
      resetLighting(gl);
    }
  }
  
  public boolean isSynchronized() {
    return mLoader.isEmpty();
  }
  
  public void stop() {
    mLoader.flush();
    mLoader.stop();
  }

  public synchronized void dispose(final GL10 gl) {
    mGLModelMap.clear();
    mGLModelCache.clear();
    mGLMeshMap.clear();
    mGLMeshCache.evictAll(new LRUCache.Disposer<Long, GLMesh>() {
      public boolean dispose(Long id, GLMesh mesh) { return disposeMesh(gl, id, mesh); }
    });
    mGLTextureMap.clear();
    mGLTextureCache.evictAll(new LRUCache.Disposer<Long, GLTexture>() {
      public boolean dispose(Long idLevel, GLTexture texture) { return disposeTexture(gl, decodeTextureId(idLevel), decodeTextureLevel(idLevel), texture); }
    });
  }
}
