package com.nutiteq.nmlpackage;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

public class GLMesh {
  private float[] mMinBounds = new float[3];
  private float[] mMaxBounds = new float[3];
  GLSubmesh[] mSubmeshes;

  public void create(GL10 gl, NMLPackage.Mesh mesh) {

    // Get bounds
    NMLPackage.Vector3 minBounds = mesh.getBounds().getMin(), maxBounds = mesh.getBounds().getMax();
    mMinBounds = new float[] { minBounds.getX(), minBounds.getY(), minBounds.getZ() };
    mMaxBounds = new float[] { maxBounds.getX(), maxBounds.getY(), maxBounds.getZ() };

    // Create submeshes
    List<GLSubmesh> submeshList = new ArrayList<GLSubmesh>();
    for (NMLPackage.Submesh submesh : mesh.getSubmeshesList()) {
      GLSubmesh glSubmesh = new GLSubmesh();
      glSubmesh.create(gl, submesh);
      submeshList.add(glSubmesh);
    }
    mSubmeshes = submeshList.toArray(new GLSubmesh[submeshList.size()]);
  }
  
  public void dispose(GL10 gl) {
  }

  public float[] getMinBounds() {
    return mMinBounds.clone();
  }

  public float[] getMaxBounds() {
    return mMaxBounds.clone();
  }

  public GLSubmesh[] getSubmeshes() {
    return mSubmeshes;
  }

  public int getTotalGeometrySize() {
    if (mSubmeshes == null)
      return 0;
    int size = 0;
    for (GLSubmesh submesh : mSubmeshes) {
      size += submesh.getTotalGeometrySize();
    }
    return size;
  }

}
