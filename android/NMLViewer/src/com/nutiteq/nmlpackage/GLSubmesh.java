package com.nutiteq.nmlpackage;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import com.google.protobuf.ByteString;
import com.nutiteq.glutils.Color;

public class GLSubmesh {
  private int mGLType = -1;
  private int[] mVertexCounts;
  private String mMaterialId;

  private FloatBuffer mPositionBuffer;
  private FloatBuffer mNormalBuffer;
  private FloatBuffer mUVBuffer;
  private ByteBuffer mColorBuffer;
  private ByteBuffer mVertexIdBuffer;
  
  // Allocate direct byte buffer
  private ByteBuffer allocateDirectByteBuffer(int size) {
    ByteBuffer dest = null;
    try {
      dest = ByteBuffer.allocateDirect(size);
    }
    catch (OutOfMemoryError error) {
      System.gc(); // try to collect garbage and then retry
      dest = ByteBuffer.allocateDirect(size);
    }
    return dest.order(ByteOrder.nativeOrder());
  }

  // Convert ProtoBuf big-endian floating point buffer to native
  // direct-allocated buffer that can be used as a GL vertex array
  private FloatBuffer convertToDirectFloatBuffer(ByteString bstr) {
    if (bstr == null)
      return null;
    if (bstr.size() == 0)
      return null;
    FloatBuffer src = bstr.asReadOnlyByteBuffer().order(ByteOrder.LITTLE_ENDIAN).asFloatBuffer();
    FloatBuffer dest = allocateDirectByteBuffer(bstr.size()).asFloatBuffer();
    dest.put(src);
    dest.position(0);
    return dest;
  }

  private ByteBuffer convertToDirectByteBuffer(ByteString bstr) {
    if (bstr == null)
      return null;
    if (bstr.size() == 0)
      return null;
    ByteBuffer src = bstr.asReadOnlyByteBuffer().order(ByteOrder.LITTLE_ENDIAN);
    ByteBuffer dest = allocateDirectByteBuffer(bstr.size());
    dest.put(src);
    dest.position(0);
    return dest;
  }

  public void create(GL10 gl, NMLPackage.Submesh submesh) {
    // Translate submesh type
    mGLType = -1;
    switch (submesh.getType()) {
    case POINTS:
      mGLType = GL10.GL_POINTS;
      break;
    case LINES:
      mGLType = GL10.GL_LINES;
      break;
    case LINE_STRIPS:
      mGLType = GL10.GL_LINE_STRIP;
      break;
    case TRIANGLES:
      mGLType = GL10.GL_TRIANGLES;
      break;
    case TRIANGLE_STRIPS:
      mGLType = GL10.GL_TRIANGLE_STRIP;
      break;
    case TRIANGLE_FANS:
      mGLType = GL10.GL_TRIANGLE_FAN;
      break;
    default:
      throw new RuntimeException("Unsupported submesh type");
    }

    // Copy basic attributes
    int totalVertexCount = 0;
    mVertexCounts = new int[submesh.getVertexCountsCount()];
    for (int i = 0; i < submesh.getVertexCountsCount(); i++) {
      mVertexCounts[i] = submesh.getVertexCounts(i);
      totalVertexCount += mVertexCounts[i];
    }
    mMaterialId = submesh.getMaterialId();

    // Create vertex buffers
    mPositionBuffer = convertToDirectFloatBuffer(submesh.getPositions());
    mNormalBuffer = convertToDirectFloatBuffer(submesh.getNormals());
    mUVBuffer = convertToDirectFloatBuffer(submesh.getUvs());
    mColorBuffer = convertToDirectByteBuffer(submesh.getColors());
    mVertexIdBuffer = allocateDirectByteBuffer(totalVertexCount * 4 * Byte.SIZE / 8);
    for (int i = 0; i < submesh.getVertexIdsCount(); i++) {
      int count = (int) (submesh.getVertexIds(i) >> 32);
      int id = (int) (submesh.getVertexIds(i) & (((long) 1 << 32) - 1));
      byte[] color = Color.encodeIntAsColor(id);
      byte[] vertexIdBuffer = new byte[4 * count]; // it is much faster to fill array on Java side and then call put method only once 
      for (int j = 0; j < vertexIdBuffer.length; j++) {
        vertexIdBuffer[j] = color[j & 3];
      }
      mVertexIdBuffer.put(vertexIdBuffer);
    }
    mVertexIdBuffer.position(0);
  }

  public void draw(GL10 gl, boolean bindVertexIds) {
    if (mVertexCounts == null)
      return;

    // Enable vertex buffers
    if (mPositionBuffer != null) {
      gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
      gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mPositionBuffer);
    }
    if (mNormalBuffer != null) {
      gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);
      gl.glNormalPointer(GL10.GL_FLOAT, 0, mNormalBuffer);
    }
    if (mUVBuffer != null) {
      gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
      gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, mUVBuffer);
    }
    if (!bindVertexIds && mColorBuffer != null) {
      gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
      gl.glColorPointer(4, GL10.GL_UNSIGNED_BYTE, 0, mColorBuffer);
    }
    if (bindVertexIds && mVertexIdBuffer != null) {
      gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
      gl.glColorPointer(4, GL10.GL_UNSIGNED_BYTE, 0, mVertexIdBuffer);
    }

    // Draw primitives
    int idx = 0;
    for (int count : mVertexCounts) {
      gl.glDrawArrays(mGLType, idx, count);
      idx += count;
    }

    // Disable vertex buffers
    if (bindVertexIds && mVertexIdBuffer != null) {
      gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
    }
    if (!bindVertexIds && mColorBuffer != null) {
      gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
    }
    if (mUVBuffer != null) {
      gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    }
    if (mNormalBuffer != null) {
      gl.glDisableClientState(GL10.GL_NORMAL_ARRAY);
    }
    if (mPositionBuffer != null) {
      gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
    }
  }

  public String getMaterialId() {
    return mMaterialId;
  }

  public int getTotalGeometrySize() {
    int size = 0;
    if (mPositionBuffer != null) {
      size += mPositionBuffer.capacity() * Float.SIZE / 8;
    }
    if (mNormalBuffer != null) {
      size += mNormalBuffer.capacity() * Float.SIZE / 8;
    }
    if (mUVBuffer != null) {
      size += mUVBuffer.capacity() * Float.SIZE / 8;
    }
    if (mColorBuffer != null) {
      size += mColorBuffer.capacity() * Byte.SIZE / 8;
    }
    if (mVertexIdBuffer != null) {
      size += mVertexIdBuffer.capacity() * Byte.SIZE / 8;
    }
    return size;
  }

}
