package com.nutiteq.nmlpackage;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.protobuf.InvalidProtocolBufferException;

/**
 * Asynchronous NML texture and model loader from sqlite database
 */
public class NMLSqliteLoader extends NMLLoader {
  private final SQLiteDatabase mDB;

  public NMLSqliteLoader(SQLiteDatabase db) {
    mDB = db;
  }

  private byte[] readBlob(String field, String table, long rowid, int size) {
    // Read data in chunks - there seems to be a hard limit of 1MB per BLOB, so use tricks to overcome this limit
    byte[] data = new byte[size];
    for (int pos = 0; pos < size; ) {
      int block = Math.min(size - pos, 262144); // use max 256KB block
      Cursor cursor = mDB.rawQuery("SELECT SUBSTR(" + field + ", 1+?, ?) FROM " + table + " WHERE ROWID=?", new String[] { Long.toString(pos), Long.toString(block), Long.toString(rowid) });
      if (cursor.moveToFirst())
        System.arraycopy(cursor.getBlob(0), 0, data, pos, block);
      cursor.close();
      pos += block;
    }
    return data;
  }

  @Override
  protected NMLPackage.Mesh loadMesh(long id) {
    NMLPackage.Mesh mesh = null;
    Cursor cursor = mDB.rawQuery("SELECT ROWID, LENGTH(nmlmesh) FROM Meshes WHERE id=?", new String[] { Long.toString(id) });
    if (cursor.moveToFirst()) {
      byte[] nmlmesh = readBlob("nmlmesh", "Meshes", cursor.getLong(0), cursor.getInt(1));
      try {
        mesh = NMLPackage.Mesh.parseFrom(nmlmesh);
      } catch (InvalidProtocolBufferException e) {
        e.printStackTrace();
      }
    }
    cursor.close();
    return mesh;
  }

  @Override
  protected NMLPackage.Texture loadTexture(long id, int level, boolean mipChain) {
    NMLPackage.Texture texture = null;
    Cursor cursor = mDB.rawQuery("SELECT ROWID, LENGTH(nmltexture) FROM Textures WHERE id=? AND level=? ORDER BY level ASC", new String[] { Long.toString(id), Integer.toString(level) });
    if (cursor.moveToFirst()) {
      byte[] nmltexture = readBlob("nmltexture", "Textures", cursor.getLong(0), cursor.getInt(1));
      try {
        texture = NMLPackage.Texture.parseFrom(nmltexture);
      } catch (InvalidProtocolBufferException e) {
        e.printStackTrace();
      }
    }
    cursor.close();
    return texture;
  }
}
