package com.nutiteq.nmlpackage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

public class GLModel {
  private float[] mMinBounds = new float[3];
  private float[] mMaxBounds = new float[3];
  private Map<String, GLMesh> mMeshMap = new HashMap<String, GLMesh>();
  private Map<String, GLTexture> mTextureMap = new HashMap<String, GLTexture>();
  GLMeshInstance[] mMeshInstances;

  public void create(GL10 gl, NMLPackage.Model model) {
    dispose(gl);

    // Update bounds
    NMLPackage.Vector3 minBounds = model.getBounds().getMin(), maxBounds = model.getBounds().getMax();
    mMinBounds = new float[] { minBounds.getX(), minBounds.getY(), minBounds.getZ() };
    mMaxBounds = new float[] { maxBounds.getX(), maxBounds.getY(), maxBounds.getZ() };

    // Build map from texture ids to GL textures
    for (NMLPackage.Texture texture : model.getTexturesList()) {
      GLTexture glTexture = new GLTexture();
      glTexture.create(gl, texture);
      mTextureMap.put(texture.getId(), glTexture);
    }

    // Build map from meshes to GL mesh objects
    for (NMLPackage.Mesh mesh : model.getMeshesList()) {
      GLMesh glMesh = new GLMesh();
      glMesh.create(gl, mesh);
      mMeshMap.put(mesh.getId(), glMesh);
    }

    // Create mesh instances
    List<GLMeshInstance> meshInstanceList = new ArrayList<GLMeshInstance>();
    for (NMLPackage.MeshInstance meshInstance : model.getMeshInstancesList()) {
      GLMeshInstance glMeshInstance = new GLMeshInstance();
      glMeshInstance.create(gl, meshInstance, mMeshMap, mTextureMap);
      meshInstanceList.add(glMeshInstance);
    }
    mMeshInstances = meshInstanceList.toArray(new GLMeshInstance[meshInstanceList.size()]);
  }

  public void replaceMesh(GL10 gl, String id, GLMesh glMesh) {
    mMeshMap.put(id, glMesh);
    if (mMeshInstances == null)
      return;
    for (GLMeshInstance meshInstance : mMeshInstances) {
      meshInstance.replaceMesh(id, glMesh);
    }
  }

  public void replaceTexture(GL10 gl, String id, GLTexture glTexture) {
    mTextureMap.put(id, glTexture);
    if (mMeshInstances == null)
      return;
    for (GLMeshInstance meshInstance : mMeshInstances) {
      meshInstance.replaceTexture(id, glTexture);
    }
  }

  public void updateMesh(GL10 gl, String id, NMLPackage.Mesh mesh) {
    GLMesh glMesh = mMeshMap.get(id);
    if (glMesh == null) {
      glMesh = new GLMesh();
    }
    glMesh.create(gl, mesh);
    replaceMesh(gl, id, glMesh);
  }

  public void updateTexture(GL10 gl, String id, NMLPackage.Texture texture) {
    GLTexture glTexture = mTextureMap.get(id);
    if (glTexture == null) {
      glTexture = new GLTexture();
    }
    glTexture.create(gl, texture);
    replaceTexture(gl, id, glTexture);
  }

  public float[] getMinBounds() {
    return mMinBounds.clone();
  }

  public float[] getMaxBounds() {
    return mMaxBounds.clone();
  }

  public void dispose(GL10 gl) {
    for (GLMesh mesh : mMeshMap.values()) {
      mesh.dispose(gl);
    }
    for (GLTexture texture : mTextureMap.values()) {
      texture.dispose(gl);
    }
    mMeshMap.clear();
    mTextureMap.clear();
    mMeshInstances = null;
  }

  public void draw(GL10 gl, GLMeshInstance.DrawMode mode) {
    if (mMeshInstances == null)
      return;
    for (GLMeshInstance meshInstance : mMeshInstances) {
      meshInstance.draw(gl, mode);
    }
  }

  public int getTotalGeometrySize() {
    int size = 0;
    for (GLMesh mesh : mMeshMap.values()) {
      size += mesh.getTotalGeometrySize();
    }
    return size;
  }

  public int getTotalTextureSize() {
    int size = 0;
    for (GLTexture texture : mTextureMap.values()) {
      size += texture.getTotalTextureSize();
    }
    return size;
  }
}
