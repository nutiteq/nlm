package com.nutiteq.nmlviewer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import com.nutiteq.nmlpackage.GLMeshInstance;
import com.nutiteq.nmlpackage.GLModelLODTree;
import com.nutiteq.nmlpackage.GLModelLODTreeRenderer;
import com.nutiteq.nmlpackage.NMLLoader;
import com.nutiteq.nmlpackage.NMLLoader.MeshBinding;
import com.nutiteq.nmlpackage.NMLLoader.TextureBinding;
import com.nutiteq.nmlpackage.NMLPackage;
import com.nutiteq.nmlpackage.NMLSqliteLoader;

public class NMLSurfaceView extends GLSurfaceView {
	
	// Model record
	private class ModelRecord {
		public GLModelLODTreeRenderer renderer;
		public GLModelLODTree glModelLODTree;
		public NMLPackage.ModelLODTree modelLODTree;
		public Map<Integer, List<MeshBinding>> meshBindingMap;
    public Map<Integer, List<TextureBinding>> textureBindingMap;
		public SQLiteDatabase db;
	}

	// Basic renderer for the view
	private class OpenGLRenderer implements GLSurfaceView.Renderer {
		private final float DISTANCE = 900.0f;

		// Profiling state
		private int mFrameCount = 0;
		private long mRenderT0 = -1;

		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
			// Initialize GL state
			gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			gl.glClearDepthf(1.0f);
			gl.glDisable(GL10.GL_DITHER);
			gl.glEnable(GL10.GL_DEPTH_TEST);
			gl.glDepthFunc(GL10.GL_LEQUAL);
			gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
			gl.glDisable(GL10.GL_CULL_FACE);
      gl.glEnable(GL10.GL_BLEND);
      gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
      gl.glEnable(GL10.GL_ALPHA_TEST);
      gl.glAlphaFunc(GL10.GL_GREATER, 0.25f);

			// Recreate GL models in next onDrawFrame call
			for (ModelRecord record : mRecords) {
				record.renderer = null;
				record.glModelLODTree = null;
			}
		}
		
		public void onDrawFrame(GL10 gl) {
			// Clears the screen and depth buffer.
			gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

			// Set view matrix
			gl.glMatrixMode(GL10.GL_MODELVIEW);
			gl.glLoadIdentity();
			GLU.gluLookAt(gl, 0, 0, DISTANCE * scaleFactor, 0, 0, 0, 0, 1, 0);
			gl.glRotatef(angleX, 0, 1, 0);
			gl.glRotatef(angleY, 1, 0, 0);
			
			// Sync/create all models
			long createT0 = System.currentTimeMillis();
			for (ModelRecord record : mRecords) {
				if (record.glModelLODTree == null) {
					record.glModelLODTree = new GLModelLODTree();
					record.glModelLODTree.create(record.modelLODTree, record.meshBindingMap, record.textureBindingMap);
					if (record.renderer != null)
						record.renderer.dispose(gl);
					NMLLoader loader = new NMLSqliteLoader(record.db);
					record.renderer = new GLModelLODTreeRenderer(record.glModelLODTree, loader, GLModelLODTreeRenderer.Configuration.getDefault());
				}
			}
			if (mFrameCount == 0)
				Log.i("NMLViewer", "* Create time " + (System.currentTimeMillis() - createT0));

			// Render all LOD trees
			for (ModelRecord record : mRecords) {
				record.renderer.draw(gl, GLMeshInstance.DrawMode.DRAW_NORMAL);
			}
			
			// Profile rendering performance
			if (mFrameCount % 100 == 0) {
				if (mFrameCount > 0)
					Log.i("NMLViewer", "* Render time for 100 frames " + (System.currentTimeMillis() - mRenderT0));
				mRenderT0 = System.currentTimeMillis();
			}
			mFrameCount++;
			
			requestRender();
		}

		public void onSurfaceChanged(GL10 gl, int width, int height) {
			// Sets the current view port to the new size
			gl.glViewport(0, 0, width, height);

			// Select the projection matrix
			gl.glMatrixMode(GL10.GL_PROJECTION);
			gl.glLoadIdentity();
			GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 10.0f, 5000.0f);

			// Reset the model view matrix
			gl.glMatrixMode(GL10.GL_MODELVIEW);
			gl.glLoadIdentity();
		}
	}

	// Scaling gesture listener
	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			scaleFactor /= detector.getScaleFactor();
			scaleFactor = Math.max(0.01f, Math.min(scaleFactor, 100.0f));
			requestRender();
			return true;
		}
	}

	private final float TOUCH_SCALE_FACTOR = 180.0f / 320;
	private final float TRACKBALL_SCALE_FACTOR = 36.0f;

	private OpenGLRenderer mRenderer;
	private ScaleGestureDetector mScaleDetector;
	private List<ModelRecord> mRecords = new ArrayList<ModelRecord>();
	private float mPreviousX;
	private float mPreviousY;

	public float angleX = 0.0f;
	public float angleY = 0.0f;
	public float scaleFactor = 1.0f;

	public NMLSurfaceView(Context context) {
		super(context);
		mRenderer = new OpenGLRenderer();
		mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
		setEGLConfigChooser(new EGLConfigChooser() {
			public EGLConfig chooseConfig(EGL10 egl, EGLDisplay eglDisplay) {
				final int[][] attribTables = new int[][] {
	    				// 8-8-8-bit color, 4x multisampling, 24-bit z buffer. Should work on most devices.
	    				new int[] { EGL10.EGL_RED_SIZE, 8, EGL10.EGL_GREEN_SIZE, 8, EGL10.EGL_BLUE_SIZE, 8, EGL10.EGL_DEPTH_SIZE, 24, EGL10.EGL_STENCIL_SIZE, 0, EGL10.EGL_SAMPLE_BUFFERS, 1, EGL10.EGL_SAMPLES, 4, EGL10.EGL_NONE },
	    				// 8-8-8-bit color, 24-bit z buffer. Should work on most devices.
	    				new int[] { EGL10.EGL_RED_SIZE, 8, EGL10.EGL_GREEN_SIZE, 8, EGL10.EGL_BLUE_SIZE, 8, EGL10.EGL_DEPTH_SIZE, 24, EGL10.EGL_STENCIL_SIZE, 0, EGL10.EGL_NONE },
	    				// 5-6-5-bit color, 24-bit z buffer. Should work on most devices.
	    				new int[] { EGL10.EGL_RED_SIZE, 5, EGL10.EGL_GREEN_SIZE, 6, EGL10.EGL_BLUE_SIZE, 5, EGL10.EGL_DEPTH_SIZE, 24, EGL10.EGL_STENCIL_SIZE, 0, EGL10.EGL_NONE },
	    				// 5-6-5-bit color, 16-bit z buffer. Last fallback. 
	    				new int[] { EGL10.EGL_RED_SIZE, 5, EGL10.EGL_GREEN_SIZE, 6, EGL10.EGL_BLUE_SIZE, 5, EGL10.EGL_DEPTH_SIZE, 16, EGL10.EGL_STENCIL_SIZE, 0, EGL10.EGL_NONE },
		    		};
				for (int i = 0; i < attribTables.length; i++) {
					int[] numConfigs = new int[] { 0 };
					EGLConfig[] configs = new EGLConfig[1];
					if (egl.eglChooseConfig(eglDisplay, attribTables[i], configs, 1, numConfigs)) {
						if (numConfigs[0] > 0) {
							Log.i("NMLViewer", "MapView: selected display configuration: " + i);
							return configs[0];
						}
					}
				}
				throw new IllegalArgumentException("eglChooseConfig failed");
			}
		});
		setRenderer(mRenderer);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}

	public void addModelLODTree(NMLPackage.ModelLODTree lodTree, Map<Integer, List<MeshBinding>> meshBindingMap, Map<Integer, List<TextureBinding>> textureBindingMap, SQLiteDatabase db) {
		ModelRecord record = new ModelRecord();
		record.modelLODTree = lodTree;
		record.meshBindingMap = meshBindingMap;
    record.textureBindingMap = textureBindingMap;
		record.db = db;
		mRecords.add(record);
	}
	
	public void clearModels() {
		mRecords.clear();
	}

	@Override
	public boolean onTrackballEvent(MotionEvent e) {
		angleX += e.getX() * TRACKBALL_SCALE_FACTOR;
		angleY += e.getY() * TRACKBALL_SCALE_FACTOR;
		requestRender();
		return true;
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		mScaleDetector.onTouchEvent(e);

		switch (e.getAction()) {
		case MotionEvent.ACTION_MOVE:
			float dx = e.getX() - mPreviousX;
			float dy = e.getY() - mPreviousY;
			angleX += dx * TOUCH_SCALE_FACTOR;
			angleY += dy * TOUCH_SCALE_FACTOR;
			requestRender();
		}
		mPreviousX = e.getX();
		mPreviousY = e.getY();
		return true;
	}

}
