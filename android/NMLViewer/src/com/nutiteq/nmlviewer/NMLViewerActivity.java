package com.nutiteq.nmlviewer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import com.nutiteq.nmlpackage.NMLLoader.MeshBinding;
import com.nutiteq.nmlpackage.NMLLoader.TextureBinding;
import com.nutiteq.nmlpackage.NMLPackage;
import com.nutiteq.nmlviewer.NMLSurfaceView;

public class NMLViewerActivity extends Activity {
	private List<String> mAssets = new ArrayList<String>();
	private File mTempFile = null;
	private SQLiteDatabase mDB;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		try {
			// Find all NML models
			for (String asset : getAssets().list("")) {
				if (asset.endsWith(".sqlite"))
					mAssets.add(asset);
			}
			String[] assets = new String[mAssets.size()];
			mAssets.toArray(assets);

			// Create dialog for choosing between models
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Select model");
			builder.setSingleChoiceItems(assets, -1,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int item) {
							dialog.dismiss();
							openAsset(mAssets.get(item));
						}
					});
			AlertDialog alert = builder.create();
			if (mAssets.size() == 1)
				openAsset(mAssets.get(0));
			else
				alert.show();
		} catch (IOException ex) {
			Log.e("NMLViewer", "Unable to start", ex);
		}
	}
	
	@Override
	protected void onDestroy() {
		// Do not close db here, it is used asynchronously in another thread
		mDB = null;
		if (mTempFile != null) {
			mTempFile.delete();
			mTempFile = null;
		}
		super.onDestroy();
	}

	File copyToTempFile(InputStream in) throws IOException {
		File tempFile = File.createTempFile("nmlviewerdb", "sqlite");
		OutputStream out = new FileOutputStream(tempFile);
	    byte[] buf = new byte[1024];
	    int len;
	    while ((len = in.read(buf)) > 0) {
	        out.write(buf, 0, len);
	    }
	    out.close();
	    return tempFile;
	}

	void openAsset(String asset) {
		try {
			NMLSurfaceView view = new NMLSurfaceView(this);

			// Load all models from database
			//InputStream is = getAssets().open(asset);
			//mTempFile = copyToTempFile(is);
			//mDB = SQLiteDatabase.openDatabase(mTempFile.getAbsolutePath(), null, SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			mDB = SQLiteDatabase.openDatabase(Environment.getExternalStorageDirectory().getPath() + "/mapxt/sf.sqlite", null, SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			Cursor cursor = mDB.rawQuery("SELECT id, LENGTH(nmlmodellodtree), nmlmodellodtree FROM ModelLODTrees", null);
			if (cursor.moveToFirst()) {
				do {
				  long lodTreeId = cursor.getLong(0); 
					byte[] nmllodtree = cursor.getBlob(2);
					NMLPackage.ModelLODTree lodTree = NMLPackage.ModelLODTree.parseFrom(nmllodtree);
					
          Map<Integer, List<MeshBinding>> meshBindingMap = new HashMap<Integer, List<MeshBinding>>();
		      Cursor cursor2 = mDB.rawQuery("SELECT node_id, local_id, mesh_id FROM ModelLODTreeNodeMeshes WHERE modellodtree_id=?", new String[] { Long.toString(lodTreeId) });
		      if (cursor2.moveToFirst()) {
		        do {
		          int nodeId = cursor2.getInt(0);
		          String localId = cursor2.getString(1);
		          long meshId = cursor2.getLong(2);
		          if (meshBindingMap.get(nodeId) == null)
		            meshBindingMap.put(nodeId, new ArrayList<MeshBinding>());
		          meshBindingMap.get(nodeId).add(new MeshBinding(localId, meshId));
		        } while (cursor2.moveToNext());
		      }
		      cursor2.close();

          Map<Integer, List<TextureBinding>> textureBindingMap = new HashMap<Integer, List<TextureBinding>>();
          cursor2 = mDB.rawQuery("SELECT node_id, local_id, texture_id, level FROM ModelLODTreeNodeTextures WHERE modellodtree_id=?", new String[] { Long.toString(lodTreeId) });
          if (cursor2.moveToFirst()) {
            do {
              int nodeId = cursor2.getInt(0);
              String localId = cursor2.getString(1);
              long textureId = cursor2.getLong(2);
              int level = cursor2.getInt(3);
              if (textureBindingMap.get(nodeId) == null)
                textureBindingMap.put(nodeId, new ArrayList<TextureBinding>());
              textureBindingMap.get(nodeId).add(new TextureBinding(localId, textureId, level));
            } while (cursor2.moveToNext());
          }
          cursor2.close();

          view.addModelLODTree(lodTree, meshBindingMap, textureBindingMap, mDB);
				} while (cursor.moveToNext());
			}
			cursor.close();
			
			setContentView(view);
		} catch (IOException ex) {
			Log.e("NMLViewer", "Unable to load model", ex);
		}
	}
	
}