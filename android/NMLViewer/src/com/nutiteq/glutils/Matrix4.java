package com.nutiteq.glutils;

import javax.microedition.khronos.opengles.GL11;

import android.opengl.Matrix;

public class Matrix4 {
  private float[] mRowCol;

  public Matrix4() {
    mRowCol = new float[16];
  }

  public Matrix4(float[][] m) {
    mRowCol = new float[16];
    for (int i = 0; i < 16; i++) {
      mRowCol[i] = m[i % 4][i / 4];
    }
  }

  public float get(int row, int col) {
    return mRowCol[row + col * 4];
  }

  public Matrix4 getInverse() {
    float[] inv = new float[16];
    Matrix.invertM(inv, 0, mRowCol, 0);
    return fromRowCol(inv);
  }

  public float[] transformVector(float[] v) {
    float[] r = new float[4];
    Matrix.multiplyMV(r, 0, mRowCol, 0, new float[] { v[0], v[1], v[2], 0 }, 0);
    return new float[] { r[0], r[1], r[2] };
  }

  public float[] transformPoint(float[] p) {
    float[] r = new float[4];
    Matrix.multiplyMV(r, 0, mRowCol, 0, new float[] { p[0], p[1], p[2], 1 }, 0);
    return new float[] { r[0] / r[3], r[1] / r[3], r[2] / r[3] };
  }

  public Frustum3 transformFrustum(Frustum3 fru) {
    float[] inv = new float[16];
    Matrix.invertM(inv, 0, mRowCol, 0);
    float[] invTrans = new float[16];
    Matrix.transposeM(invTrans, 0, inv, 0);
    float[][] planes = new float[6][];
    for (int p = 0; p < 6; p++) {
      planes[p] = new float[4];
      Matrix.multiplyMV(planes[p], 0, invTrans, 0, fru.getPlane(p), 0);
    }
    return new Frustum3(planes);
  }

  public Matrix4 multiplyMatrix(Matrix4 m) {
    float[] product = new float[16];
    Matrix.multiplyMM(product, 0, mRowCol, 0, m.mRowCol, 0);
    return fromRowCol(product);
  }

  public static Matrix4 fromRowCol(float[] rowCol) {
    Matrix4 m = new Matrix4();
    m.mRowCol = rowCol.clone();
    return m;
  }

  public static Matrix4 fromGLMatrix(GL11 gl, int matrixType) {
    float[] rowCol = new float[16];
    gl.glGetFloatv(matrixType, rowCol, 0);
    return fromRowCol(rowCol);
  }
}
