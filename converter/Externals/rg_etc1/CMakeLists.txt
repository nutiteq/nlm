project(rg_etc1)

set(SOURCES ./rg_etc1.h ./rg_etc1.cpp)

set(rg_etc1_include_dirs
        ${CMAKE_CURRENT_SOURCE_DIR}
)

set(rg_etc1_include_dirs ${rg_etc1_include_dirs} PARENT_SCOPE)

add_library(rg_etc1 STATIC ${SOURCES})

