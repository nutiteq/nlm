project(squish)

set(SOURCES ./alpha.cpp ./clusterfit.cpp ./colourblock.cpp ./colourfit.cpp ./colourset.cpp ./maths.cpp ./rangefit.cpp ./singlecolourfit.cpp ./squish.cpp)

set(squish_include_dirs
        ${CMAKE_CURRENT_SOURCE_DIR}/.
)

set(squish_include_dirs ${squish_include_dirs} PARENT_SCOPE)

add_library(squish STATIC ${SOURCES})

