/*
Copyright (c) 2012 ...

This file is part of DAE2NML.
*/

#include "Prerequisites.h"
#include "DAELoader.h"
#include "DAE2NMLConverter.h"
#include "TextureExporter.h"
#include "LicenseManager.h"

#include <iostream>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#if defined(WIN32) && !defined(NDEBUG)
#	include <crtdbg.h> 
#endif

namespace po = boost::program_options;
namespace fs = boost::filesystem;

// static const std::string LICENSE_PUBLIC_KEY = "MIIBtzCCASwGByqGSM44BAEwggEfAoGBAOHD5Oa9gZ/b6mJKf5xyVUAtEGzTD+ynSSxp/jLwdBiCq1MDKCDFrSJjCCp2V84SGcPzS+YRMXkPp6zAH8/8hXyqQrvocp+FJ0HZ51MCSesKzLDwgHOfat6BKQD/bzxX5LDKj84+1gzMcMA55JioVVG/cFoltKEFtS16zoLbloexAhUA8yRmkSLicTd2RYHuCtpNLOHuyIsCgYEAm+ZQpfx9tVtNh1eYqaWbVO9xNNJbZA2MpLbt4BYmUWtLdNq4I814ES/aWRlWEf2lXmLIdlAXWaRdvJ3Co66o2ZDeHWPqyNqAUpnf+OQeQu3DzuWTm5wx0bcwwZAhas7MyXGY6rc88TY+//H5l/QBG0P0J7a3V1cwACqZ4nQarkgDgYQAAoGAHf2jI4AsnPnYOQBzcrbU15dk7ygreVf4+7NpS0S8e+n/oHwS1lpUdstIx9KyqCrGB7G6maZkhztBu2/5he6/H3GnzCyvFq2iQZ7WVe6fkk86bW2blQki6Adj1dUCiFei/zhDd4wqFldfJaMBDVpXoXmMmoqCCFJ4eacPwZhBHGM=";
static const std::string LICENSE_PUBLIC_KEY = "MIIBtjCCASsGByqGSM44BAEwggEeAoGBAIlFs9OqwdM4lcfhXQVCyYDzPc6rO6sbtlKuEYa/uSzeuk1tvYcGybBwB3mYLYvOh0Qd/65TUO6rYI9xTAHK0HtjGj+XLaNDiP4eEvaVfg2fhX26XDdaAGXfKWKCHKiZ0cWLnBtTap2woVvSt6TLxcxrDnwG9mrL3Lt06rflF4oJAhUAyWhMdWcGzgq37TUJcKauhX7OEHcCgYAsXe5Q9JRA3yQsAGijSifQS8Pth1FfF69dU7VUeYD55VZ5x4UVAQP+wg7K5e6RQgJpaL1R4duVkFRgr3RuTwevv1szi/3ENiIQW4vNhPxc/sN+Y2YdlNnguOhV2LEcYmneX+F5cb2UXQZBBDhVgEtU7c9bxyA6tSwKuC70EqfZSQOBhAACgYAzp7pUQ1XxR79N8NbaB1PUPE7n1bZdFLF1QsK9thjL4Q3dbg6fub3qZfSPL286nZjfD+15oya1ORFKwSplindHNx6vSL+AmNhI4GYdyIasPnLAqCV9rIMTgQ+RfmyWDvSLxSDVqWqA83M6m/FFuWMKWKqMOEueJZTwrr/HNNTk+w==";

int dae2nml(const std::string &inputFile, const std::string &outputFile)
{
	// Convert to internal representation
	COLLADABU::URI uri(COLLADABU::URI::nativePathToUri(inputFile));
	DAE2NML::URILoader uriLoader(uri.getPathDir());
	DAE2NML::Model model;
 	DAE2NML::DAELoader loader(uri.getPathFile(), uriLoader, COLLADABU::Math::Matrix4::IDENTITY, true, model);
	std::cout << "Loading DAE file" << std::endl;
	if (!loader.load())
	{
		std::cerr << "Could not load DAE file!" << std::endl;
		return 1;
	}

	// Save as optimized NML
	NMLPackage::Model nmlModel;
	DAE2NML::DAE2NMLConverter converter(DAE2NML::DAE2NMLConverter::getDefaultOptions(), model, nmlModel);
	if (!converter.convert())
	{
		std::cout << "Could not convert model!" << std::endl;
	}

	// Serialize
	std::string modelString;
	nmlModel.SerializeToString(&modelString);

	// Save converted file
	FILE *outFP = fopen(outputFile.c_str(), "wb");
	if (outFP == NULL)
	{
		std::cerr << "Can not open output file for writing!" << std::endl;
		return 1;
	}
	fwrite(modelString.data(), sizeof(char), modelString.size(), outFP);
	fclose(outFP);
	return 0;
}

int main(int argc, char* argv[]) 
{
	po::options_description options;

	options.add(DAE2NML::DAE2NMLConverter::Options::getDescription());
	options.add(DAE2NML::TextureExporter::Options::getDescription());

#ifdef REQUIRE_LICENSE
	options.add(Nutiteq::LicenseManager::Options::getDescription());
#endif

	po::options_description hidden("Hidden options");
	hidden.add_options()
		("output-file", po::value<std::string>(), "output file")
		("input-file", po::value<std::string>(), "input file")
		;

	po::options_description cmdline_options;
	cmdline_options.add(options).add(hidden);

	po::positional_options_description file_options;
	file_options.add("output-file", 1);
	file_options.add("input-file", -1);

	try
	{
		po::variables_map vm;
		store(po::command_line_parser(argc, argv).
			options(cmdline_options).positional(file_options).run(), vm);
		notify(vm);

		if (!(vm.count("output-file") && vm.count("input-file")))
		{
			std::cerr << "Usage: dae2nml [options] nmlfile daefile" << std::endl;
			std::cerr << std::endl << options << std::endl;
			return -1;
		}

#ifdef REQUIRE_LICENSE
		Nutiteq::LicenseManager::Options::verifyLicense("dae2nml", LICENSE_PUBLIC_KEY, vm);
#endif

		std::string outputFile = vm["output-file"].as<std::string>();
		std::string inputFile = vm["input-file"].as<std::string>();

		fs::remove(outputFile);

		DAE2NML::DAE2NMLConverter::getDefaultOptions().setVariables(vm);
		DAE2NML::TextureExporter::getDefaultOptions().setVariables(vm);

		return dae2nml(inputFile, outputFile);
	}
	catch (const std::exception &ex)
	{
		std::cerr << ex.what() << std::endl;
	}
	return -1;
}
