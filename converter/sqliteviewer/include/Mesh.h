#pragma once

#include "BaseTypes.h"
#include "Submesh.h"

#include <list>
#include <cassert>
#include <boost/shared_ptr.hpp>

namespace SqliteViewer
{

	class GLMesh
	{

	private:

		std::list<GLSubmeshPtr> mSubmeshList;

	public:

		void create(GL10 gl, const NMLPackage::Mesh &mesh)
		{
			mSubmeshList.clear();
			for (int i = 0; i < mesh.submeshes_size(); i++)
			{
				const NMLPackage::Submesh &submesh = mesh.submeshes(i);
				GLSubmeshPtr glSubmesh(new GLSubmesh());
				glSubmesh->create(gl, submesh);
				mSubmeshList.push_back(glSubmesh);
			}
		}

		void createFromOp(GL10 gl, const NMLPackage::Mesh &mesh, const NMLPackage::MeshOp &meshOp)
		{
			mSubmeshList.clear();
			for (int i = 0; i < meshOp.submesh_op_lists_size(); i++)
			{
				const NMLPackage::SubmeshOpList &submeshOpList = meshOp.submesh_op_lists(i);
				GLSubmeshPtr glSubmesh(new GLSubmesh());
				glSubmesh->createFromOpList(gl, mesh, submeshOpList);
				mSubmeshList.push_back(glSubmesh);
			}
		}

		const std::list<GLSubmeshPtr> &getSubmeshList() const
		{
			return mSubmeshList;
		}

		int getTotalGeometrySize() const
		{
			int size = 0;
			for (std::list<GLSubmeshPtr>::const_iterator it = mSubmeshList.begin(); it != mSubmeshList.end(); it++)
			{
				size += (*it)->getTotalGeometrySize();
			}
			return size;
		}

		void dispose(GL10 gl)
		{
		}
	};

	typedef boost::shared_ptr<GLMesh> GLMeshPtr;

}