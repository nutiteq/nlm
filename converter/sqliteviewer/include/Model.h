#pragma once

#include "BaseTypes.h"
#include "Texture.h"
#include "Mesh.h"
#include "MeshInstance.h"

#include <list>
#include <map>
#include <string>
#include <cassert>
#include <boost/shared_ptr.hpp>

namespace SqliteViewer
{

	class GLModel
	{

	private:

		std::map<std::string, GLMeshPtr> mMeshMap;
		std::map<std::string, GLTexturePtr> mTextureMap;
		std::list<GLMeshInstancePtr> mMeshInstanceList;

	public:

		void create(GL10 gl, const NMLPackage::Model &model)
		{
			dispose(gl);

			// Build map from texture ids to GL textures
			for (int i = 0; i < model.textures_size(); i++)
			{
				const NMLPackage::Texture &texture = model.textures(i);
				GLTexturePtr glTexture(new GLTexture());
				glTexture->create(gl, texture);
				mTextureMap[texture.id()] = glTexture;
			}

			// Build map from meshes to GL mesh objects
			for (int i = 0; i < model.meshes_size(); i++)
			{
				const NMLPackage::Mesh &mesh = model.meshes(i);
				GLMeshPtr glMesh(new GLMesh());
				glMesh->create(gl, mesh);
				mMeshMap[mesh.id()] = glMesh;
			}

			// Create mesh instances
			for (int i = 0; i < model.mesh_instances_size(); i++)
			{
				const NMLPackage::MeshInstance &meshInstance = model.mesh_instances(i);
				GLMeshInstancePtr glMeshInstance(new GLMeshInstance());
				glMeshInstance->create(gl, meshInstance, mMeshMap, mTextureMap);
				mMeshInstanceList.push_back(glMeshInstance);
			}
		}

		void replaceMesh(GL10 gl, const std::string &id, const GLMeshPtr &glMesh)
		{
			if (mMeshMap[id] == glMesh)
				return;
			mMeshMap[id] = glMesh;
			for (std::list<GLMeshInstancePtr>::iterator it = mMeshInstanceList.begin(); it != mMeshInstanceList.end(); it++)
			{
				(*it)->replaceMesh(id, glMesh);
			}
		}

		void replaceTexture(GL10 gl, const std::string &id, const GLTexturePtr &glTexture)
		{
			if (mTextureMap[id] == glTexture)
				return;
			mTextureMap[id] = glTexture;
			for (std::list<GLMeshInstancePtr>::iterator it = mMeshInstanceList.begin(); it != mMeshInstanceList.end(); it++)
			{
				(*it)->replaceTexture(id, glTexture);
			}
		}

		void dispose(GL10 gl)
		{
			for (std::map<std::string, GLMeshPtr>::iterator it = mMeshMap.begin(); it != mMeshMap.end(); it++)
			{
				it->second->dispose(gl);
			}
			for (std::map<std::string, GLTexturePtr>::iterator it = mTextureMap.begin(); it != mTextureMap.end(); it++)
			{
				it->second->dispose(gl);
			}
			mMeshMap.clear();
			mTextureMap.clear();
			mMeshInstanceList.clear();
		}

		void draw(GL10 gl, GLMeshInstance::DrawMode mode) const
		{
			for (std::list<GLMeshInstancePtr>::const_iterator it = mMeshInstanceList.begin(); it != mMeshInstanceList.end(); it++)
			{
				(*it)->draw(gl, mode);
			}
		}

		int getDrawCallCount() const
		{
			int count = 0;
			for (std::list<GLMeshInstancePtr>::const_iterator it = mMeshInstanceList.begin(); it != mMeshInstanceList.end(); it++)
			{
				count += (*it)->getDrawCallCount();
			}
			return count;
		}

		int getTotalGeometrySize() const
		{
			int size = 0;
			for (std::map<std::string, GLMeshPtr>::const_iterator it = mMeshMap.begin(); it != mMeshMap.end(); it++)
			{
				size += it->second->getTotalGeometrySize();
			}
			return size;
		}
	};

	typedef boost::shared_ptr<GLModel> GLModelPtr;

}
