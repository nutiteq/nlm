#pragma once

#include "BaseTypes.h"
#include "Material.h"

#include <list>
#include <map>
#include <string>
#include <cassert>
#include <boost/shared_ptr.hpp>

namespace SqliteViewer
{

	class GLMeshInstance
	{

	private:

		std::string mMeshId;
		GLMeshPtr mMesh;
		std::map<std::string, GLMaterialPtr> mMaterialMap;
		bool mTransformEnabled;
		float mTransformMatrix[16]; // 4x4 matrix

	public:

		enum DrawMode { DRAW_NORMAL, DRAW_VERTEX_IDS, DRAW_COLOR };

		GLMeshInstance() : mTransformEnabled(false) { }

		void create(GL10 gl, const NMLPackage::MeshInstance &meshInstance, const std::map<std::string, GLMeshPtr> &meshMap, const std::map<std::string, GLTexturePtr> &textureMap)
		{
			mMeshId = meshInstance.mesh_id();

			// Look up mesh
			mMesh.reset();
			std::map<std::string, GLMeshPtr>::const_iterator mesh_it = meshMap.find(meshInstance.mesh_id());
			if (mesh_it != meshMap.end())
				mMesh = mesh_it->second;

			// Set node transformation matrix
			mTransformEnabled = meshInstance.has_transform();
			if (mTransformEnabled)
			{
				const NMLPackage::Matrix4 &transform = meshInstance.transform();
				mTransformMatrix[0] = transform.m00();
				mTransformMatrix[1] = transform.m10();
				mTransformMatrix[2] = transform.m20();
				mTransformMatrix[3] = transform.m30();

				mTransformMatrix[4] = transform.m01();
				mTransformMatrix[5] = transform.m11();
				mTransformMatrix[6] = transform.m21();
				mTransformMatrix[7] = transform.m31();

				mTransformMatrix[8] = transform.m02();
				mTransformMatrix[9] = transform.m12();
				mTransformMatrix[10] = transform.m22();
				mTransformMatrix[11] = transform.m32();

				mTransformMatrix[12] = transform.m03();
				mTransformMatrix[13] = transform.m13();
				mTransformMatrix[14] = transform.m23();
				mTransformMatrix[15] = transform.m33();
			}

			// Create material map
			mMaterialMap.clear();
			for (int i = 0; i < meshInstance.materials_size(); i++)
			{
				const NMLPackage::Material &material = meshInstance.materials(i);
				GLMaterialPtr glMaterial(new GLMaterial());
				glMaterial->create(gl, material, textureMap);
				mMaterialMap[material.id()] = glMaterial;
			}
		}

		void replaceMesh(const std::string &meshId, const GLMeshPtr &glMesh)
		{
			if (mMeshId == meshId)
			{
				mMesh = glMesh;
			}
		}

		void replaceTexture(const std::string &textureId, const GLTexturePtr &glTexture)
		{
			for (std::map<std::string, GLMaterialPtr>::iterator it = mMaterialMap.begin(); it != mMaterialMap.end(); it++)
			{
				it->second->replaceTexture(textureId, glTexture);
			}
		}

		void draw(GL10 gl, DrawMode mode) const
		{
			if (!mMesh)
				return;
			if (mTransformEnabled)
			{
				glMatrixMode(GL_MODELVIEW);
				glPushMatrix();
				glMultMatrixf(mTransformMatrix);
			}
			const std::list<GLSubmeshPtr> &submeshList = mMesh->getSubmeshList();
			for (std::list<GLSubmeshPtr>::const_iterator it = submeshList.begin(); it != submeshList.end(); it++)
			{
				GLSubmeshPtr submesh = *it;
				std::map<std::string, GLMaterialPtr>::const_iterator mat_it = mMaterialMap.find(submesh->getMaterialId());
				if (mat_it != mMaterialMap.end())
				{
					GLMaterialPtr material = mat_it->second;
					if (mode == DRAW_NORMAL)
						material->bind(gl);
					submesh->draw(gl, mode == DRAW_VERTEX_IDS);
					if (mode == DRAW_NORMAL)
						material->unbind(gl);
				}
			}
			if (mTransformEnabled)
			{
				glPopMatrix();
			}
		}

		int getDrawCallCount() const
		{
			if (!mMesh)
				return 0;
			int count = 0;
			for (std::list<GLSubmeshPtr>::const_iterator it = mMesh->getSubmeshList().begin(); it != mMesh->getSubmeshList().end(); it++)
			{
				count += (*it)->getDrawCallCount();
			}
			return count;
		}
	};

	typedef boost::shared_ptr<GLMeshInstance> GLMeshInstancePtr;

}
