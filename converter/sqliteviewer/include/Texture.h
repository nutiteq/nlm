#pragma once

#include "BaseTypes.h"
#include "Texture.h"

#include "rg_etc1.h"
#include "PVRTTexture.h"
#include "PVRTDecompress.h"

#include <list>
#include <map>
#include <string>
#include <cassert>
#include <boost/shared_ptr.hpp>
#include <GL/gl.h>
#include <GL/glu.h>

#ifndef GL_CLAMP_TO_EDGE
#define GL_CLAMP_TO_EDGE 0x812F
#endif

#ifndef GL_TEXTURE_MAX_LEVEL
#define GL_TEXTURE_MAX_LEVEL 0x813D
#endif

#ifndef GL_TEXTURE_MAX_ANISOTROPY_EXT
#define GL_TEXTURE_MAX_ANISOTROPY_EXT 0x84FE
#endif

#ifndef GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT
#define GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT 0x84FF
#endif

#define cimg_use_png

#pragma warning(push)
#pragma warning(disable: 4267 4244 4319)
#include "CImg.h"
#pragma warning(pop)

namespace SqliteViewer
{

	class GLTexture
	{

	private:

		GLuint mGLTextureId;
		int mSize;

		int getSamplerWrapMode(NMLPackage::Sampler::WrapMode wrapMode)
		{
			switch (wrapMode) {
			case NMLPackage::Sampler::CLAMP:
				return GL_CLAMP_TO_EDGE;
			default:
				return GL_REPEAT; // ignore MIRROR, etc
			}
		}

		void updateSampler(GL10 gl, bool hasSampler, NMLPackage::Sampler sampler, bool complete)
		{
			if (hasSampler)
			{
				switch (sampler.filter())
				{
				case NMLPackage::Sampler::NEAREST:
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
					break;
				case NMLPackage::Sampler::BILINEAR:
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					break;
				case NMLPackage::Sampler::TRILINEAR:
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, complete ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					break;
				}
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, getSamplerWrapMode(sampler.wrap_s()));
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, getSamplerWrapMode(sampler.wrap_t()));
			} else {
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, complete ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			}

			GLint aniso = 0;
			glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &aniso);
			if (aniso > 0)
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, aniso);
		}

		void updateMipLevel(GL10 gl, int level, const NMLPackage::Texture &texture)
		{
			int glFormat = -1, glFormatInternal = -1;
			std::string glTextureData = texture.mipmaps(level);
			switch (texture.format()) {
			case NMLPackage::Texture::LUMINANCE8:
				glFormat = GL_LUMINANCE;
				glFormatInternal = GL_LUMINANCE8;
				break;
			case NMLPackage::Texture::RGB8:
				glFormat = GL_RGB;
				glFormatInternal = GL_RGB8;
				break;
			case NMLPackage::Texture::RGBA8:
				glFormat = GL_RGBA;
				glFormatInternal = GL_RGBA8;
				break;
			case NMLPackage::Texture::ETC1:
				{
					NMLPackage::Texture textureCopy(texture);
					uncompressTexture(textureCopy);
					updateMipLevel(gl, level, textureCopy);
				}
				return;
			case NMLPackage::Texture::PVRTC:
				{
					NMLPackage::Texture textureCopy(texture);
					uncompressTexture(textureCopy);
					updateMipLevel(gl, level, textureCopy);
				}
				return;
			default:
				assert(false);
				return;
			}
			if (!glTextureData.empty()) {
				glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
				glTexImage2D(GL_TEXTURE_2D, level, glFormatInternal, texture.width(), texture.height(), 0, glFormat, GL_UNSIGNED_BYTE, glTextureData.data());

#ifdef DEBUG_TEXTURES_DIR
				int components = glTextureData.size() / texture.width() / texture.height();
				cimg_library::CImg<unsigned char> cimage(texture.width(), texture.height(), 1, components);
				cimg_forXYC(cimage, x, y, c)
				{
					cimage(x, y, 0, c) = glTextureData[(y * cimage.width() + x) * components + c];
				}
				for (int k = 0; cimage.size() > 0; k++)
				{
					char fname[1024]; sprintf(fname, DEBUG_TEXTURES_DIR "/%s-%d-%d.png", texture.id().c_str(), cimage.width(), k);
					cimage.save_png(fname);
					cimg_library::CImg<float> resized(cimage);
					resized.sRGBtoRGB();
					resized.resize(cimage.width() / 2, cimage.height() / 2, cimage.depth(), cimage.spectrum(), 3);
					resized.RGBtosRGB();
					cimage.assign(resized);
				}
#endif
			}
		}

		void updateMipMaps(GL10 gl, const NMLPackage::Texture &texture)
		{
			mSize = 0;
			for (int i = 0; i < texture.mipmaps_size(); i++)
			{
				updateMipLevel(gl, i, texture);
				mSize += texture.mipmaps(i).size();
			}
			if (texture.mipmaps_size() > 0)
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, texture.mipmaps_size() - 1); // NOTE: not available in GL ES
			}
		}

	public:

		GLTexture() : mGLTextureId(-1), mSize(0)
		{
		}

		void create(GL10 gl, const NMLPackage::Texture &texture)
		{
			dispose(gl);

			if (texture.width() < 1 || texture.height() < 1)
				return;

			glGenTextures(1, &mGLTextureId);

			glBindTexture(GL_TEXTURE_2D, mGLTextureId);
			updateMipMaps(gl, texture);
			updateSampler(gl, texture.has_sampler(), texture.sampler(), texture.mipmaps_size() > 1);
		}

		void swap(GLTexture &glTexture)
		{
			std::swap(mGLTextureId, glTexture.mGLTextureId);
		}

		bool isEmpty() const
		{
			return mGLTextureId == -1;
		}

		void dispose(GL10 gl)
		{
			if (mGLTextureId != -1) {
				glDeleteTextures(1, &mGLTextureId);
			}
			mGLTextureId = -1;
			mSize = 0;
		}

		void bind(GL10 gl) const
		{
			if (mGLTextureId != -1) {
				glBindTexture(GL_TEXTURE_2D, mGLTextureId);
			}
		}

		void unbind(GL10 gl) const
		{
		}

		int getTotalTextureSize() const
		{
			return mSize;
		}

		static void uncompressTexture(NMLPackage::Texture &texture)
		{
			switch (texture.format())
			{
			case NMLPackage::Texture::ETC1:
				{
					for (int i = 0; i < texture.mipmaps_size(); i++)
					{
						std::string textureData = texture.mipmaps(i);
						int etc1Width = (texture.width() + 3) & ~3, etc1Height = (texture.height() + 3) & ~3;
						std::vector<unsigned int> etc1Image(texture.width() * texture.height());
						size_t offset = 16;
						for (int y = 0; y + 4 <= etc1Height; y += 4)
						{
							for (int x = 0; x + 4 <= etc1Width; x += 4)
							{
								unsigned int block[4 * 4];
								rg_etc1::unpack_etc1_block(&textureData[offset], block);
								offset += 4 * 4 / 2;
								for (int yb = 0; yb < 4; yb++)
								{
									if (y + yb >= texture.height())
										continue;
									for (int xb = 0; xb < 4; xb++)
									{
										if (x + xb >= texture.width())
											continue;
										etc1Image[(y + yb) * texture.width() + x + xb] = block[yb * 4 + xb];
									}
								}
							}
						}
						textureData.clear();
						if (!etc1Image.empty())
							textureData.assign(reinterpret_cast<const char *>(&etc1Image[0]), reinterpret_cast<const char *>(&etc1Image[0] + etc1Image.size()));
						texture.set_mipmaps(i, textureData);
					}
					texture.set_format(NMLPackage::Texture::RGBA8);
				}
				break;
			case NMLPackage::Texture::PVRTC:
				{
					for (int i = 0; i < texture.mipmaps_size(); i++)
					{
						std::string textureData = texture.mipmaps(i);
						assert (textureData.size() >= sizeof(PVRTextureHeaderV3));
						const PVRTextureHeaderV3 *header = reinterpret_cast<const PVRTextureHeaderV3 *>(&textureData[0]);
						bool bpp2 = header->u64PixelFormat == ePVRTPF_PVRTCI_2bpp_RGB || header->u64PixelFormat == ePVRTPF_PVRTCI_2bpp_RGBA;
						bool rgba = header->u64PixelFormat == ePVRTPF_PVRTCI_2bpp_RGBA || header->u64PixelFormat == ePVRTPF_PVRTCI_4bpp_RGBA;
						std::vector<unsigned long> pvrtcImage(texture.width() * texture.height());
						PVRTDecompressPVRTC(&textureData[sizeof(PVRTextureHeaderV3)], bpp2, texture.width(), texture.height(), reinterpret_cast<unsigned char *>(&pvrtcImage[0]));

						textureData.clear();
						if (!pvrtcImage.empty())
							textureData.assign(reinterpret_cast<const char *>(&pvrtcImage[0]), reinterpret_cast<const char *>(&pvrtcImage[0] + pvrtcImage.size()));
						texture.set_mipmaps(i, textureData);
					}
					texture.set_format(NMLPackage::Texture::RGBA8);
				}
				break;
			default:
				break;
			}
		}
	};

	typedef boost::shared_ptr<GLTexture> GLTexturePtr;

}
