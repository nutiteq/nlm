#pragma once

#include "BaseTypes.h"
#include "Texture.h"
#include "Model.h"

#include "stdext/lru_cache.h"

#include <map>
#include <vector>
#include <queue>
#include <set>
#include <limits>

#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include <sqlite3pp.h>

namespace SqliteViewer
{

	typedef google::protobuf::int64 MeshId;
	typedef google::protobuf::int64 TextureId;
	typedef std::pair<TextureId, int> TextureIdLevel;
	typedef int ModelId;

	struct MeshBinding
	{
		int id;
		std::string localId;
		MeshId meshId;
		boost::shared_ptr<NMLPackage::MeshOp> meshOp;
	};

	struct TextureBinding
	{
		std::string localId;
		TextureIdLevel textureIdLevel;
	};

	class GLModelLODTreeNode
	{

	public:

		typedef std::vector<MeshBinding> MeshBindingList;
		typedef std::vector<TextureBinding> TextureBindingList;

		GLModelLODTreeNode() : mModelId(-1), mLevel(-1) { }

		void create(const NMLPackage::ModelLODTreeNode & node, const MeshBindingList & meshBindingList, const TextureBindingList & textureBindingList, int level)
		{
			mModelId = node.id();
			mModel = node.model();
			mLevel = level;
			mNodeBounds = Bounds3(getVector(node.bounds().min()), getVector(node.bounds().max()));
			mModelBounds = Bounds3(getVector(node.model().bounds().min()), getVector(node.model().bounds().max()));
			mMeshBindingList = meshBindingList;
			mTextureBindingList = textureBindingList;
			for (int i = 0; i < node.children_ids_size(); i++)
			{
				mChildren.push_back(node.children_ids(i));
			}
		}

		ModelId getModelId() const
		{
			return mModelId;
		}

		const NMLPackage::Model & getModel() const
		{
			return mModel;
		}

		int getLevel() const
		{
			return mLevel;
		}

		const Bounds3 & getNodeBounds() const
		{
			return mNodeBounds;
		}

		const Bounds3 & getModelBounds() const
		{
			return mModelBounds;
		}

		bool isLeaf() const
		{
			return mChildren.empty();
		}

		const MeshBindingList & getMeshBindingList() const
		{
			return mMeshBindingList;
		}

		const TextureBindingList & getTextureBindingList() const
		{
			return mTextureBindingList;
		}

		int getChildCount() const
		{
			return (int) mChildren.size();
		}

		int getChild(int index) const
		{
			assert(index >= 0 && index < getChildCount());
			return mChildren[index];
		}

	private:

		static Vector3 getVector(const NMLPackage::Vector3 & v) { return Vector3(v.x(), v.y(), v.z()); }

		ModelId mModelId;
		NMLPackage::Model mModel;
		int mLevel;
		Bounds3 mNodeBounds;
		Bounds3 mModelBounds;
		MeshBindingList mMeshBindingList;
		TextureBindingList mTextureBindingList;
		std::vector<int> mChildren;
	};

	typedef boost::shared_ptr<GLModelLODTreeNode> GLModelLODTreeNodePtr;

	class GLModelLODTree
	{

	public:

		typedef std::map<int, GLModelLODTreeNode::MeshBindingList> NodeMeshBindingMap;
		typedef std::map<int, GLModelLODTreeNode::TextureBindingList> NodeTextureBindingMap;

		void create(const NMLPackage::ModelLODTree & lodTree, const NodeMeshBindingMap & nodeMeshBindingMap, const NodeTextureBindingMap & nodeTextureBindingMap)
		{
			mNodes.resize(lodTree.nodes_size());
			mNodeMeshBindingMap = nodeMeshBindingMap;
			mNodeTextureBindingMap = nodeTextureBindingMap;
			if (lodTree.nodes_size() > 0)
			{
				createNode(lodTree, 0, 0);
			}
		}

		GLModelLODTreeNode * getNode(int index)
		{
			if (index < 0 || index >= (int) mNodes.size())
				return 0;
			return &mNodes[index];
		}

		const GLModelLODTreeNode * getNode(int index) const
		{
			if (index < 0 || index >= (int) mNodes.size())
				return 0;
			return &mNodes[index];
		}

	private:

		std::vector<GLModelLODTreeNode> mNodes;
		NodeMeshBindingMap mNodeMeshBindingMap;
		NodeTextureBindingMap mNodeTextureBindingMap;

		void createNode(const NMLPackage::ModelLODTree & lodTree, int index, int level)
		{
			if (index == -1)
				return;
			GLModelLODTreeNode &node = mNodes[index];
			node.create(lodTree.nodes(index), mNodeMeshBindingMap[index], mNodeTextureBindingMap[index], level);
			for (int i = 0; i < node.getChildCount(); i++)
			{
				createNode(lodTree, node.getChild(i), level + 1);
			}
		}
	};

	typedef boost::shared_ptr<GLModelLODTree> GLModelLODTreePtr;

	class GLModelLODTreeRenderer
	{

	protected:

		typedef boost::shared_ptr<NMLPackage::Model> NMLModelPtr;
		typedef boost::shared_ptr<NMLPackage::Mesh> NMLMeshPtr;
		typedef boost::shared_ptr<NMLPackage::Texture> NMLTexturePtr;
		typedef std::map<ModelId, GLModelPtr> GLModelMap;
		typedef std::map<MeshId, GLMeshPtr> GLMeshMap;
		typedef std::map<TextureIdLevel, GLTexturePtr> GLTextureMap;
		typedef std::map<MeshId, NMLMeshPtr> NMLMeshMap;
		typedef std::map<TextureIdLevel, NMLTexturePtr> NMLTextureMap;
		typedef std::map<ModelId, GLModelPtr> GLModelCache;
		typedef stdext::lru_cache<MeshId, GLMeshPtr> GLMeshCache;
		typedef stdext::lru_cache<TextureIdLevel, GLTexturePtr> GLTextureCache;

		class Loader
		{
		public:

			Loader(GLModelLODTreeRenderer & renderer, sqlite3pp::database & db) : mRenderer(renderer), mDB(db) { }

			void requestMesh(MeshId id)
			{
				Task task;
				task.type = Task::LOAD_MESH;
				task.meshId = id;
				boost::mutex::scoped_lock lock(mTaskQueueMutex);
				TaskQueue::const_iterator it = mTaskQueue.begin();
				for (; it != mTaskQueue.end(); it++)
				{
					if (it->type == task.type && it->meshId == task.meshId)
						break;
				}
				if (it == mTaskQueue.end())
					mTaskQueue.push_back(task);
			}

			void requestTexture(TextureIdLevel idLevel, bool mipChain)
			{
				Task task;
				task.type = Task::LOAD_TEXTURE;
				task.textureIdLevel = idLevel;
				task.mipChain = mipChain;
				boost::mutex::scoped_lock lock(mTaskQueueMutex);
				TaskQueue::const_iterator it = mTaskQueue.begin();
				for (; it != mTaskQueue.end(); it++)
				{
					if (it->type == task.type && it->textureIdLevel == task.textureIdLevel)
						break;
				}
				if (it == mTaskQueue.end())
					mTaskQueue.push_back(task);
			}

			void run()
			{
				bool sleep = false;
				while (true)
				{
					if (sleep)
						boost::this_thread::sleep(boost::posix_time::milliseconds(10));
					sleep = true;
					Task task;
					{
						boost::mutex::scoped_lock lock(mTaskQueueMutex);
						if (mTaskQueue.empty())
							continue;
						task = mTaskQueue.front();
					}
					switch (task.type)
					{
					case Task::STOP:
						return;
					case Task::LOAD_MESH:
						loadMesh(task.meshId);
						break;
					case Task::LOAD_TEXTURE:
						loadTexture(task.textureIdLevel, task.mipChain);
						break;
					}
					{
						boost::mutex::scoped_lock lock(mTaskQueueMutex);
						if (mTaskQueue.empty())
							continue;
						mTaskQueue.pop_front();
					}
					sleep = false;
				}
			}

			void stop()
			{
				Task task;
				task.type = Task::STOP;
				boost::mutex::scoped_lock lock(mTaskQueueMutex);
				mTaskQueue.clear();
				mTaskQueue.push_back(task);
			}

		protected:

			struct Task { enum Type { LOAD_MESH, LOAD_TEXTURE, STOP }; Type type; MeshId meshId; TextureIdLevel textureIdLevel; bool mipChain; };

			typedef std::list<Task> TaskQueue;

			GLModelLODTreeRenderer & mRenderer;
			sqlite3pp::database & mDB;
			TaskQueue mTaskQueue;
			boost::mutex mTaskQueueMutex;

			void loadMesh(MeshId id)
			{
				sqlite3pp::query qry(mDB, "SELECT LENGTH(nmlmesh), nmlmesh FROM Meshes WHERE id=:source_id");
				qry.bind(":source_id", id);
				for (sqlite3pp::query::iterator qit = qry.begin(); qit != qry.end(); qit++)
				{
					size_t nmlmesh_size = qit->get<int>(0);
					const void * nmlmesh_data = qit->get<const void *>(1);
					NMLMeshPtr mesh(new NMLPackage::Mesh);
					mesh->ParseFromArray(nmlmesh_data, nmlmesh_size);
					mRenderer.updateMesh(id, mesh);
				}
			}

			void loadTexture(TextureIdLevel idLevel, bool mipChain)
			{
				sqlite3pp::query qry(mDB, "SELECT LENGTH(nmltexture), nmltexture FROM Textures WHERE id=:source_id AND textures.level=:level ORDER BY textures.level ASC");
				qry.bind(":source_id", idLevel.first);
				qry.bind(":level", idLevel.second);
				for (sqlite3pp::query::iterator qit = qry.begin(); qit != qry.end(); qit++)
				{
					size_t nmltexture_size = qit->get<int>(0);
					const void * nmltexture_data = qit->get<const void *>(1);
					NMLTexturePtr texture(new NMLPackage::Texture);
					texture->ParseFromArray(nmltexture_data, nmltexture_size);
					GLTexture::uncompressTexture(*texture);
					mRenderer.updateTexture(idLevel, texture);
				}
			}
		};

		GLModelMap mGLModelMap;
		GLTextureMap mGLTextureMap;
		GLMeshMap mGLMeshMap;

		GLModelCache mGLModelCache;
		GLTextureCache mGLTextureCache;
		GLMeshCache mGLMeshCache;

		NMLMeshMap mNMLMeshMap;
		NMLTextureMap mNMLTextureMap;
		boost::mutex mNMLMapMutex;

		Loader mLoader;
		boost::thread mLoaderThread;

		const GLModelLODTree & mLODTree;
		float mLODSharpness;
		int mResidentLevels;

		int mDrawCallCount;
		int mLoadedGeometrySize;
		int mLoadedTexturesSize;

		void setupLighting(GL10 gl, const Vector3 &lightDir)
		{
			const float ambientMaterial[]  = { 0.7f, 0.7f, 0.7f, 1.0f };
			const float diffuseMaterial[]  = { 0.3f, 0.3f, 0.3f, 1.0f };
			const float specularMaterial[] = { 0.0f, 0.0f, 0.0f, 1.0f };
			const float lightModelAmbient[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
			const float lightPos[] = { lightDir(0), lightDir(1), lightDir(2), 0 };

			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
			glShadeModel(GL_SMOOTH);
			glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
			glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightModelAmbient);
			glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT,  ambientMaterial);
			glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE,  diffuseMaterial);
			glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specularMaterial);
		}

		void resetLighting(GL10 gl)
		{
			glDisable(GL_LIGHTING);
		}

		void drawBounds(GL10 gl, const Bounds3 & bounds, int color)
		{
			const Vector3 boundPoints[2] = { bounds.min, bounds.max };
			Vector3 points[8];
			for (int i = 0; i < 8; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					points[i](j) = boundPoints[(i >> j) & 1](j);
				}
			}

			glDisable(GL_TEXTURE_2D);
			glColor4f(color & 1 ? 1 : 0, color & 2 ? 1 : 0, color & 4 ? 1 : 0, 1);
			glBegin(GL_LINES);
			for (int i = 0; i < 12; ++i)
			{
				const int ax = (i >> 2) & 3;
				const int as = (4-ax) >> 2;
				const int bs = (5-ax) >> 2;
				const int v0 = ((i&1) << as) | ((i&2) << bs);
				const int v1 = v0|(1<<ax);
				glVertex3d(points[v0](0), points[v0](1), points[v0](2));
				glVertex3d(points[v1](0), points[v1](1), points[v1](2));
			}
			glEnd();
		}

		void drawModel(GL10 gl, ModelId id)
		{
			GLModelMap::const_iterator it = mGLModelMap.find(id);
			if (it == mGLModelMap.end())
				return;
			GLModelPtr model = it->second;
			if (!model)
				return;
			model->draw(gl, GLMeshInstance::DRAW_NORMAL);
			mDrawCallCount += model->getDrawCallCount();
		}

		void drawModelLODTreeNode(GL10 gl, const GLModelLODTreeNode * node, const Frustum & frustum, const Matrix4 & modelViewProjM)
		{
			if (node == 0)
				return;

			// Clip, if node bounds outside of frustum, nothing to draw
			if (!frustum.inside(node->getModelBounds()))
				return;

			drawBounds(gl, node->getNodeBounds(), 6);
			//drawBounds(gl, node->getModelBounds(), 1);

			// Prefetch this level. If the level is not completely loaded, we will request its data to be loaded
			prefetchModel(gl, node);

			// Find current node bounding box projected to frustum near plane
			Vector3 boundsSize = getProjectedBoundsSize(node->getNodeBounds(), modelViewProjM);

			// Decide whether to recurse or not - if node takes more screen space than approx. viewport, then recurse
			if (std::max(boundsSize(0), boundsSize(1)) * mLODSharpness > 2 && !node->isLeaf())
			{
				// Create sorted list of children, based on distance from the camera (drawing/loading closer nodes first)
				std::vector<const GLModelLODTreeNode *> children;
				children.reserve(node->getChildCount());
				for (int i = 0; i < node->getChildCount(); i++)
				{
					const GLModelLODTreeNode * child = mLODTree.getNode(node->getChild(i));
					std::vector<const GLModelLODTreeNode *>::iterator it = children.begin();
					while (it != children.end())
					{
						if (getProjectedBoundsDistance(child->getNodeBounds(), modelViewProjM) < getProjectedBoundsDistance((*it)->getNodeBounds(), modelViewProjM))
							break;
						it++;
					}
					children.insert(it, child);
				}

				// Test that next level is (almost) complete - draw child nodes only in this case
				bool completed = true;
				for (size_t i = 0; i < children.size(); i++)
				{
					const GLModelLODTreeNode * child = children[i];
					if (child->getLevel() > mResidentLevels && !frustum.inside(child->getModelBounds()))
						continue;

					if (!prefetchModel(gl, child))
						completed = false;
				}

				// Draw recursively if complete
				if (completed)
				{
					// Draw the sorted list in order
					for (size_t i = 0; i < children.size(); i++)
					{
						drawModelLODTreeNode(gl, children[i], frustum, modelViewProjM);
					}
					return;
				}
			}

			// Stop at this level. Preload and draw all models
			drawModel(gl, node->getModelId());
		}

		double getProjectedBoundsDistance(const Bounds3 & bounds, const Matrix4 & frustumMVP) const
		{
			Vector3 point = (bounds.min + bounds.max) * 0.5;
			Vector3 projPoint = cglib::proj_p(cglib::transform(cglib::expand(point, 1.0), frustumMVP));
			return projPoint(2);
		}

		Vector3 getProjectedBoundsSize(const Bounds3 & bounds, const Matrix4 & frustumMVP) const
		{
			static const double DOUBLE_MAX = std::numeric_limits<double>::max();
			static const double NEAR_DISTANCE = 0.5;

			const Vector3 boundPoints[2] = { bounds.min, bounds.max };
			Vector3 min(DOUBLE_MAX, DOUBLE_MAX, DOUBLE_MAX), max(-DOUBLE_MAX, -DOUBLE_MAX, -DOUBLE_MAX);
			for (int i = 0; i < 8; i++)
			{
				Vector3 point;
				for (int j = 0; j < 3; j++)
				{
					point(j) = boundPoints[(i >> j) & 1](j);
				}

				Vector4 projPoint = cglib::transform(cglib::expand(point, 1.0), frustumMVP);
				if (projPoint(3) < NEAR_DISTANCE)
				{
					projPoint(3) = NEAR_DISTANCE;
				}

				for (int j = 0; j < 3; j++)
				{
					double val = projPoint(j) / projPoint(3);
					min(j) = std::min(min(j), val);
					max(j) = std::max(max(j), val);
				}
			}
			return max - min;
		}

		bool prefetchModel(GL10 gl, const GLModelLODTreeNode * node)
		{
			if (node == 0)
				return false;

			GLModelPtr glModel = mGLModelCache[node->getModelId()];
			if (!glModel)
			{
				glModel.reset(new GLModel());
				glModel->create(gl, node->getModel());
			}
			mGLModelMap[node->getModelId()] = glModel;

			bool completed = true;
			for (GLModelLODTreeNode::MeshBindingList::const_iterator bindingIt = node->getMeshBindingList().begin(); bindingIt != node->getMeshBindingList().end(); bindingIt++)
			{
				if (!prefetchMesh(gl, node, *bindingIt))
					completed = false;
			}
			for (GLModelLODTreeNode::TextureBindingList::const_iterator bindingIt = node->getTextureBindingList().begin(); bindingIt != node->getTextureBindingList().end(); bindingIt++)
			{
				if (!prefetchTexture(gl, node, *bindingIt))
					completed = false;
			}
			return completed;
		}

		bool prefetchMesh(GL10 gl, const GLModelLODTreeNode * node, const MeshBinding & binding)
		{
			MeshId id = binding.id;
			GLMeshPtr glMesh;
			if (!mGLMeshCache.get(id, glMesh))
			{
				if (mNMLMeshMap.count(binding.meshId) == 0)
				{
					mLoader.requestMesh(binding.meshId);
					return false;
				}

				// Create GLModel, update cache
				glMesh.reset(new GLMesh());
				if (binding.meshOp)
				{
					glMesh->createFromOp(gl, *mNMLMeshMap[binding.meshId], *binding.meshOp);
				}
				else
				{
					glMesh->create(gl, *mNMLMeshMap[binding.meshId]);
				}
				mGLMeshCache.store(id, glMesh, glMesh->getTotalGeometrySize());
			}

			// Update model mesh
			GLModelPtr glModel = mGLModelMap[node->getModelId()];
			if (glModel)
			{
				glModel->replaceMesh(gl, binding.localId, glMesh);
			}

			mGLMeshMap[id] = glMesh;
			return true;
		}

		void updateMesh(MeshId id, NMLMeshPtr mesh)
		{
			boost::mutex::scoped_lock lock(mNMLMapMutex);
			mNMLMeshMap[id] = mesh;
		}

		bool disposeMesh(GL10 gl, MeshId id, GLMeshPtr glMesh)
		{
			GLMeshMap::iterator it = mGLMeshMap.find(id);
			if (it != mGLMeshMap.end())
				return false;
			glMesh->dispose(gl);
			return true;
		}

		bool prefetchTexture(GL10 gl, const GLModelLODTreeNode * node, const TextureBinding & binding)
		{
			TextureIdLevel idLevel = binding.textureIdLevel;
			GLTexturePtr glTexture;
			if (!mGLTextureCache.get(idLevel, glTexture))
			{
				if (mNMLTextureMap.count(idLevel) == 0)
				{
					mLoader.requestTexture(idLevel, true);
					return false;
				}

				// Create GLTexture, update cache
				glTexture.reset(new GLTexture());
				glTexture->create(gl, *mNMLTextureMap[idLevel]);
				mGLTextureCache.store(idLevel, glTexture, glTexture->getTotalTextureSize());
			}

			// Update model to use this texture
			GLModelPtr glModel = mGLModelMap[node->getModelId()];
			if (glModel)
			{
				glModel->replaceTexture(gl, binding.localId, glTexture);
			}

			mGLTextureMap[idLevel] = glTexture;
			return true;
		}

		void updateTexture(TextureIdLevel idLevel, const NMLTexturePtr texture)
		{
			boost::mutex::scoped_lock lock(mNMLMapMutex);
			mNMLTextureMap[idLevel] = texture;
		}

		bool disposeTexture(GL10 gl, TextureIdLevel idLevel, GLTexturePtr glTexture)
		{
			GLTextureMap::iterator it = mGLTextureMap.find(idLevel);
			if (it != mGLTextureMap.end())
				return false;
			glTexture->dispose(gl);
			return true;
		}

	public:

		struct Configuration
		{
			int residentLevels; // how many model levels to keep resident
			int geometryCacheSize; // how much memory can be used for caching model geometry data
			int textureCacheSize; // how much memory can be used for caching model texture data
			static Configuration getDefault() { Configuration config; return config; }

		private:
			Configuration() : residentLevels(1), geometryCacheSize(8192 * 1024), textureCacheSize(8 * 8192 * 1024) { }
		};

		GLModelLODTreeRenderer(const GLModelLODTree & tree, sqlite3pp::database & db, const Configuration & config) : mLoader(*this, db), mLODTree(tree), mLODSharpness(1), mDrawCallCount(0), mLoadedGeometrySize(0), mLoadedTexturesSize(0), mGLMeshCache(config.geometryCacheSize), mGLTextureCache(config.textureCacheSize), mResidentLevels(config.residentLevels)
		{
			mLoaderThread = boost::thread(&Loader::run, &mLoader);
		}

		virtual ~GLModelLODTreeRenderer()
		{
			mLoader.stop();
			mLoaderThread.join();
		}

		void setLODSharpness(float s)
		{
			mLODSharpness = s;
		}

		void draw(GL10 gl, const Frustum & frustum, const Matrix4 & modelViewM, const Matrix4 & projM)
		{
			boost::mutex::scoped_lock lock(mNMLMapMutex);

			// Setup lighting
			//setupLighting(gl, cglib::transform_vector(Vector3(0.25f, 0.5f, 1.0f), modelViewM));

			// Draw
			long lT0 = GetTickCount();
			int prevDrawCallCount = mDrawCallCount;
			mDrawCallCount = 0;
			drawModelLODTreeNode(gl, mLODTree.getNode(0), frustum, projM * modelViewM);
			long lT1 = GetTickCount();
			if (lT1 - lT0 > 100)
				printf("Model drawing time: %d\n", lT1 - lT0);
			if (prevDrawCallCount != mDrawCallCount)
				printf("Model draw calls: %d\n", mDrawCallCount);

			// Reset lighting
			//resetLighting(gl);

			// Debugging
			int prevLoadedGeometrySize = mLoadedGeometrySize;
			mLoadedGeometrySize = 0;
			for (GLMeshMap::const_iterator it = mGLMeshMap.begin(); it != mGLMeshMap.end(); it++)
			{
				mLoadedGeometrySize += it->second->getTotalGeometrySize();
			}
			if (prevLoadedGeometrySize != mLoadedGeometrySize)
				printf("Model geometry footprint: %d\n", mLoadedGeometrySize);

			int prevLoadedTexturesSize = mLoadedTexturesSize;
			mLoadedTexturesSize = 0;
			for (GLTextureMap::const_iterator it = mGLTextureMap.begin(); it != mGLTextureMap.end(); it++)
			{
				mLoadedTexturesSize += it->second->getTotalTextureSize();
			}
			if (prevLoadedTexturesSize != mLoadedTexturesSize)
				printf("Model texture footprint: %d\n", mLoadedTexturesSize);

			// Clear loaded but unused data from caches, maps
			mGLModelCache.swap(mGLModelMap);
			mGLMeshCache.evict(false, boost::bind(&GLModelLODTreeRenderer::disposeMesh, this, gl, _1, _2));
			mGLTextureCache.evict(false, boost::bind(&GLModelLODTreeRenderer::disposeTexture, this, gl, _1, _2));
			mGLModelMap.clear();
			mGLMeshMap.clear();
			mGLTextureMap.clear();
			mNMLMeshMap.clear();
			mNMLTextureMap.clear();
		}

		void dispose(GL10 gl)
		{
			boost::mutex::scoped_lock lock(mNMLMapMutex);

			mGLModelMap.clear();
			mGLModelCache.clear();
			mGLMeshMap.clear();
			mGLMeshCache.evict(true, boost::bind(&GLModelLODTreeRenderer::disposeMesh, this, gl, _1, _2));
			mGLTextureMap.clear();
			mGLTextureCache.evict(true, boost::bind(&GLModelLODTreeRenderer::disposeTexture, this, gl, _1, _2));
			mNMLMeshMap.clear();
			mNMLTextureMap.clear();
		}
	};

	typedef boost::shared_ptr<GLModelLODTreeRenderer> GLModelLODTreeRendererPtr;

}
