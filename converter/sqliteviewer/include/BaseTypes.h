#pragma once

#define NOMINMAX

#include "cglib/vec.h"
#include "cglib/mat.h"
#include "cglib/frustum3.h"

#include <windows.h>

#pragma warning(push)
#pragma warning(disable: 4244)
#include "NMLPackage.pb.h"
#pragma warning(pop)

namespace SqliteViewer
{
	typedef cglib::vec3<double> Vector3;
	typedef cglib::vec4<double> Vector4;
	typedef cglib::mat4x4<double> Matrix4;
	typedef cglib::bounding_box<double, 3> Bounds3;
	typedef cglib::frustum3<double> Frustum;

	typedef void * GL10;
}
