#ifndef _lru_cache_using_std_
#define _lru_cache_using_std_

#include <cassert>
#include <list>
#include <tuple>
#include <map>
#include <boost/function.hpp>

namespace stdext
{

	// Class providing fixed-size LRU-replacement cache
	template <
		typename K,
		typename V
	> class lru_cache
	{
	public:

		typedef K key_type;
		typedef V value_type;

		// Key access history, most recent at back
		typedef std::list<key_type> key_tracker_type;

		// Tuple type (cache record type)
		typedef std::tr1::tuple<
			value_type,
			size_t,
			typename key_tracker_type::iterator
		> tuple_type;

		// Key to value and key history iterator
		typedef std::map<
			key_type, tuple_type
		> key_to_value_type;

		// Constuctor specifies maximum size of records to be stored
		lru_cache(size_t c):_capacity(c),_resident(0) {}

		// Clear cache
		void clear(){
			_key_tracker.clear();
			_key_to_value.clear();
			_resident=0;
		}

		// Test if key/value exists in cache
		bool exists(const key_type& k) const{
			return _key_to_value.count(k)>0;
		}

		// Try to get values with given key
		bool get(const key_type& k, value_type &v){

			// Attempt to find existing record
			const typename key_to_value_type::iterator it
				=_key_to_value.find(k);

			if (it==_key_to_value.end())
				return false;

			// Update access record by moving accessed key to back of list
			access((*it).second);

			// Return the retrieved value
			v = std::tr1::get<0>((*it).second);
			return true;
		}

		// Store value
		void store(const key_type& k, const value_type &v, size_t size){
			const typename key_to_value_type::iterator it
				=_key_to_value.find(k);

			if (it==_key_to_value.end()) {

				// We don't have it:

				// Record k as most-recently-used key
				typename key_tracker_type::iterator key_it
					=_key_tracker.insert(_key_tracker.end(),k);

				// Create the key-value entry, linked to the usage record. Update size
				_key_to_value.insert(
					std::make_pair(k, tuple_type(v,size,key_it))
				);
				_resident += size;
			} else {

				// We do have it:

				// Update access record by moving
				// accessed key to back of list
				access((*it).second);

				// Update data and size
				_resident -= std::tr1::get<1>((*it).second);
				std::tr1::get<0>((*it).second) = v;
				std::tr1::get<1>((*it).second) = size;
				_resident += size;
			}
		}

		// Evict values
		void evict(){
			// Identify least recently used key
			typename key_tracker_type::iterator key_it =_key_tracker.begin();
			while (key_it!=_key_tracker.end()){
				if (_resident <= _capacity)
					break;
				const typename key_to_value_type::iterator it
					=_key_to_value.find(*key_it);
				assert(it!=_key_to_value.end());
				
				// Erase both elements to completely purge record
				key_it = _key_tracker.erase(key_it);
				_resident -= std::tr1::get<1>((*it).second);
				_key_to_value.erase(it);
			}
		}

		// Evict values, based on given functor
		void evict(bool all, boost::function<bool(key_type, value_type)> fn){
			// Identify least recently used key
			typename key_tracker_type::iterator key_it =_key_tracker.begin();
			while (key_it!=_key_tracker.end()){
				if (!all && _resident <= _capacity)
					break;
				const typename key_to_value_type::iterator it
					=_key_to_value.find(*key_it);
				assert(it!=_key_to_value.end());
				
				// Check if this element has non-zero size - keep elements with zero size resident
				if (!fn((*it).first, std::tr1::get<0>((*it).second))){
					key_it++;
					continue;
				}

				// Erase both elements to completely purge record
				key_it = _key_tracker.erase(key_it);
				_resident -= std::tr1::get<1>((*it).second);
				_key_to_value.erase(it);
			}
		}

		// Get resident size
		size_t resident() const {
			return _resident;
		}

	private:

		// Shift given element to the end of LRU list, return new element iterator
		void access(tuple_type& record) {
			_key_tracker.splice(
				_key_tracker.end(),
				_key_tracker,
				std::tr1::get<2>(record)
			);
			typename key_tracker_type::iterator key_it =_key_tracker.end();
			std::tr1::get<2>(record) = --key_it;
		}

		// Maximum resident size
		const size_t _capacity;

		// Current cache size
		size_t _resident;

		// Key access history
		key_tracker_type _key_tracker;

		// Key-to-value lookup
		key_to_value_type _key_to_value;
	};

}

#endif
