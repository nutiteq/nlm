#include "Submesh.h"
#include "Mesh.h"
#include "Texture.h"
#include "Material.h"
#include "MeshInstance.h"
#include "Model.h"
#include "ModelLODTree.h"

#include <stdio.h>
#include <windows.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <sqlite3pp.h>

#define cimg_use_png

#pragma warning(push)
#pragma warning(disable: 4267 4244 4319)
#include "CImg.h"
#pragma warning(pop)

float angleX = 0.0f;
float angleY = 0.0f;
float scaleFactor = 1.0f;

int motionX = 0;
int motionY = 0;
bool motionMode = false;

SqliteViewer::GL10 gl = 0;
boost::shared_ptr<sqlite3pp::database> db;
std::vector<SqliteViewer::GLModelPtr> glModels;
std::vector<SqliteViewer::GLModelLODTreePtr> glModelLODTrees;
std::vector<SqliteViewer::GLModelLODTreeRendererPtr> glModelLODTreeRenderers;

void loadModelLODTrees(const std::string &dbFile)
{
	db.reset(new sqlite3pp::database(dbFile.c_str()));
	db->execute("PRAGMA encoding='UTF-8'");

	int meshBindingId = 1;
	sqlite3pp::query qry(*db, "SELECT id, LENGTH(nmlmodellodtree), nmlmodellodtree FROM ModelLODTrees");
	for (sqlite3pp::query::iterator qit = qry.begin(); qit != qry.end(); qit++)
	{
		int modellodtree_id = qit->get<int>(0);
		size_t nmlmodellodtree_size = qit->get<int>(1);
		const void * nmlmodellodtree_data = qit->get<const void *>(2);
		NMLPackage::ModelLODTree modelLODTree;
		modelLODTree.ParseFromArray(nmlmodellodtree_data, nmlmodellodtree_size);

		sqlite3pp::query qryMeshBindings(*db, "SELECT node_id, local_id, mesh_id, CASE WHEN nmlmeshop is NULL THEN 0 else LENGTH(nmlmeshop) END, nmlmeshop FROM ModelLODTreeNodeMeshes WHERE modellodtree_id=:modellodtree_id");
		qryMeshBindings.bind(":modellodtree_id", modellodtree_id);
		SqliteViewer::GLModelLODTree::NodeMeshBindingMap meshBindingMap;
		for (sqlite3pp::query::iterator qitMeshBindings = qryMeshBindings.begin(); qitMeshBindings != qryMeshBindings.end(); qitMeshBindings++)
		{
			SqliteViewer::MeshBinding meshBinding;
			meshBinding.id = meshBindingId++; // TODO: should create map (meshId, meshOpString) -> id
			meshBinding.localId = qitMeshBindings->get<const char *>(1);
			meshBinding.meshId = qitMeshBindings->get<SqliteViewer::MeshId>(2);
			if (qitMeshBindings->get<int>(3) > 0)
			{
				size_t nmlmeshop_size = qitMeshBindings->get<int>(3);
				const void * nmlmeshop_data = qitMeshBindings->get<const void *>(4);
				meshBinding.meshOp.reset(new NMLPackage::MeshOp());
				meshBinding.meshOp->ParseFromArray(nmlmeshop_data, nmlmeshop_size);
			}
			meshBindingMap[qitMeshBindings->get<int>(0)].push_back(meshBinding);
		}

		sqlite3pp::query qryTexBindings(*db, "SELECT node_id, local_id, texture_id, level FROM ModelLODTreeNodeTextures WHERE modellodtree_id=:modellodtree_id");
		qryTexBindings.bind(":modellodtree_id", modellodtree_id);
		SqliteViewer::GLModelLODTree::NodeTextureBindingMap textureBindingMap;
		for (sqlite3pp::query::iterator qitTexBindings = qryTexBindings.begin(); qitTexBindings != qryTexBindings.end(); qitTexBindings++)
		{
			SqliteViewer::TextureBinding textureBinding;
			textureBinding.localId = qitTexBindings->get<const char *>(1);
			textureBinding.textureIdLevel = SqliteViewer::TextureIdLevel(qitTexBindings->get<SqliteViewer::TextureId>(2), qitTexBindings->get<int>(3));
			textureBindingMap[qitTexBindings->get<int>(0)].push_back(textureBinding);
		}

		SqliteViewer::GLModelLODTreePtr glModelLODTree(new SqliteViewer::GLModelLODTree());
		glModelLODTree->create(modelLODTree, meshBindingMap, textureBindingMap);
		glModelLODTrees.push_back(glModelLODTree);
		SqliteViewer::GLModelLODTreeRenderer::Configuration config = SqliteViewer::GLModelLODTreeRenderer::Configuration::getDefault();
		SqliteViewer::GLModelLODTreeRendererPtr glModelLODTreeRenderer(new SqliteViewer::GLModelLODTreeRenderer(*glModelLODTree, *db, config));
		glModelLODTreeRenderer->setLODSharpness(1.0f);
		glModelLODTreeRenderers.push_back(glModelLODTreeRenderer);
	}
}

void initialize()
{
	// Initialize GL state
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glDisable(GL_DITHER);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.25f);
}

void display()
{
	static const float DISTANCE = 2000.0f;

	// Clears the screen and depth buffer.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set view matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0, 0, DISTANCE * scaleFactor, 0, 0, 0, 0, 1, 0);
	glRotatef(angleX, 0, 1, 0);
	glRotatef(angleY, 1, 0, 0);

	// Draw models
	for (size_t i = 0; i < glModels.size(); i++)
	{
		glModels[i]->draw(gl, SqliteViewer::GLMeshInstance::DRAW_NORMAL);
	}

	// Render LOD model trees
	GLdouble projData[16], modelViewData[16];
	glGetDoublev(GL_PROJECTION_MATRIX, projData);
	glGetDoublev(GL_MODELVIEW_MATRIX, modelViewData);
	SqliteViewer::Matrix4 projM; projM.copy(projData);
	SqliteViewer::Matrix4 modelViewM; modelViewM.copy(modelViewData);
	SqliteViewer::Frustum frustum = cglib::transform_frustum(cglib::gl_projection_frustum(projM), cglib::inverse(modelViewM));
	for (size_t i = 0; i < glModelLODTreeRenderers.size(); i++)
	{
		glModelLODTreeRenderers[i]->draw(gl, frustum, modelViewM, projM);
	}

	// Swap buffers
	glutSwapBuffers();
}

void idle()
{
	glutPostRedisplay();
}

void reshape(int width, int height)
{
	// Update viewport
	glViewport(0, 0, width, height);

	// Select the projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (float) width / (float) height, 0.5f,	10000.0f);

	// Reset the model view matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void mouse(int button, int state, int x, int y)
{
	static const float SCALESTEP = 0.95f;

	// Test wheel event
	if ((button == 3) || (button == 4))
	{
		if (state == GLUT_UP)
		{
			return;
		}
		scaleFactor *= (button == 3 ? SCALESTEP : 1.0f / SCALESTEP);
	}

	// Test for left button event
	if (button == 0)
	{
		if (state == GLUT_UP)
		{
			motionMode = false;
			return;
		}
		if (!motionMode)
		{
			motionMode = true;
			motionX = x;
			motionY = y;
		}
	}
}

void motion(int x, int y)
{
	static const float MOTIONSCALE = 0.5f;

	if (motionMode)
	{
		int dx = x - motionX;
		int dy = -(y - motionY);
		angleX += MOTIONSCALE * dx;
		angleY += MOTIONSCALE * dy;
		motionX = x;
		motionY = y;
	}
}

int main(int argc, char ** argv)
{
	if (argc < 2)
	{
		fprintf(stderr, "Usage: %s [sqlitefile]\n", argv[0]);
		return 1;
	}
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_STENCIL | GLUT_DOUBLE | GLUT_MULTISAMPLE);
	glutInitWindowSize(800, 800);
	glutCreateWindow("sqliteviewer");
	glutReshapeFunc(reshape);
	glutIdleFunc(idle);
	glutDisplayFunc(display);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	initialize();
	loadModelLODTrees(argv[1]);
	glutMainLoop();
	return 0;
}
