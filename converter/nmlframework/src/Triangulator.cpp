/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "Triangulator.h"

#include "Math/COLLADABUMathMatrix3.h"

#include <algorithm>


namespace NMLFramework
{

	//--------------------------------------------------------------------
	Triangulator::Triangulator(const float *points) : mPoints(points)
	{
	}

	//--------------------------------------------------------------------
	bool Triangulator::triangulate(const std::vector<int> &inIndices, std::vector<int> &outIndices) const
	{
		std::vector<int> inIndicesTemp;
		for (size_t i = 0; i < inIndices.size(); i++)
		{
			size_t j = (i + 1) % inIndices.size();
			if (getPoint(inIndices[i]) != getPoint(inIndices[j]))
				inIndicesTemp.push_back(inIndices[i]);
		}
		outIndices.clear();
		if (process(inIndicesTemp, outIndices))
		{
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	bool Triangulator::process(const std::vector<int> &inIndices, std::vector<int> &outIndices) const
	{
		// Simple ear-removal method, with bad algorithmic complexity
		if (inIndices.size() < 3)
			return false;
		Vector3 N = calculateNormal(inIndices);
		std::vector<int> V = inIndices;
		int count = 2 * (int) V.size();
		for (int v = (int) V.size() - 1; V.size() > 2;)
		{
			if (0 >= (count--))
				return false;

			int u = (v + 0) % V.size();
			v = (u + 1) % V.size();
			int w = (v + 1) % V.size();

			if (snip(u, v, w, V, N))
			{
				outIndices.push_back(V[u]);
				outIndices.push_back(V[v]);
				outIndices.push_back(V[w]);
				
				V.erase(V.begin() + v);
				count = 2 * (int) V.size();
			}
		}
		return true;
	}

	//--------------------------------------------------------------------
	Vector3 Triangulator::calculateNormal(const std::vector<int> &V) const
	{
		// Newell method
		Vector3 N(0, 0, 0);
		for (int p = 0; p < (int) V.size(); p++)
		{
			Vector3 A = getPoint(V[p]);
			Vector3 B = getPoint(V[(p + 1) % V.size()]);
			N.x += (A.y - B.y) * (A.z + B.z);
			N.y += (A.z - B.z) * (A.x + B.x);
			N.z += (A.x - B.x) * (A.y + B.y);
		}
		return N;
	}

	//--------------------------------------------------------------------
	bool Triangulator::snip(int u, int v, int w, const std::vector<int> &V, const Vector3 &N) const
	{
		static const double EPSILON = 1.0e-20;

		Vector3 A = getPoint(V[u]);
		Vector3 B = getPoint(V[v]);
		Vector3 C = getPoint(V[w]);

		Vector3 v0 = B - A;
		Vector3 v1 = C - A;
		double d = N.dotProduct((B - A).crossProduct(C - A));
		if (d < EPSILON)
			return false;

		for (int p = 0; p < (int) V.size(); p++)
		{
			if ((p == u) || (p == v) || (p == w))
				continue;
			Vector3 P = getPoint(V[p]);
			if (insideTriangle(A, B, C, P))
				return false;
		}
		return true;
	}

	//--------------------------------------------------------------------
	bool Triangulator::insideTriangle(const Vector3& A, const Vector3& B, const Vector3& C, const Vector3& P) const
	{
		return sameSide(P, A, B, C) && sameSide(P, B, C, A) && sameSide(P, C, A, B);
	}

	//--------------------------------------------------------------------
	bool Triangulator::sameSide(const Vector3& p0, const Vector3& p1, const Vector3& A, const Vector3& B) const
	{
		Vector3 cp0 = (B - A).crossProduct(p0 - A);
		Vector3 cp1 = (B - A).crossProduct(p1 - A);
		if (cp0.dotProduct(cp1) >= 0)
			return true;
		return false;
	}

} // namespace NMLFramework
