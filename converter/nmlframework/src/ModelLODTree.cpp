/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "ModelLODTree.h"


namespace NMLFramework
{

	//--------------------------------------------------------------------
	int ModelLODTree::Node::saveNMLModelLODTreeNode(NMLPackage::ModelLODTree &nmlModelLODTree, NodeMap &nodeMap, const TextureExporter &textureExporter) const
	{
		int id = nmlModelLODTree.nodes_size();
		NMLPackage::ModelLODTreeNode &nmlModelLODTreeNode = *nmlModelLODTree.add_nodes();
		nmlModelLODTreeNode.set_id(id);
		*nmlModelLODTreeNode.mutable_bounds() = mNodeBounds.getNMLBounds();
		mModel.saveNMLModel("", *nmlModelLODTreeNode.mutable_model(), textureExporter);
		*nmlModelLODTreeNode.mutable_model()->mutable_bounds() = mModelBounds.getNMLBounds(); // NOTE: overwrite bounds as bounds info after saving is really empty as meshes are detached
		for (size_t i = 0; i < mChildren.size(); i++)
		{
			int childId = mChildren[i]->saveNMLModelLODTreeNode(nmlModelLODTree, nodeMap, textureExporter);
			nmlModelLODTreeNode.add_children_ids(childId);
		}
		nodeMap[id] = const_cast<Node *>(this);
		return id;
	}

	//--------------------------------------------------------------------
	bool ModelLODTree::Node::loadNMLModelLODTreeNode(const NMLPackage::ModelLODTree &nmlModelLODTree, int id, NodeMap &nodeMap)
	{
		const NMLPackage::ModelLODTreeNode &nmlModelLODTreeNode = nmlModelLODTree.nodes(id);
		assert(nmlModelLODTreeNode.id() == id);
		mNodeBounds = Bounds3::fromNMLBounds(nmlModelLODTreeNode.bounds());
		mModel.loadNMLModel(nmlModelLODTreeNode.model());
		mModelBounds = Bounds3::fromNMLBounds(nmlModelLODTreeNode.model().bounds());
		clearChildren();
		for (int i = 0; i < nmlModelLODTreeNode.children_ids_size(); i++)
		{
			addChild().loadNMLModelLODTreeNode(nmlModelLODTree, nmlModelLODTreeNode.children_ids(i), nodeMap);
		}
		nodeMap[id] = this;
		return true;
	}

	//--------------------------------------------------------------------
	size_t ModelLODTree::Node::hash() const
	{
		size_t seed = 0;
		boost::hash_combine(seed, mModelId);
		for (size_t i = 0; i < mChildren.size(); i++)
		{
			boost::hash_combine(seed, mChildren[i]);
		}
		return seed;
	}

	//--------------------------------------------------------------------
	ModelLODTree::Node &ModelLODTree::Node::operator = (const Node &node)
	{
		if (&node == this)
			return *this;
		mModel = node.mModel;
		mModelId = node.mModelId;
		mModelBounds = node.mModelBounds;
		mNodeBounds = node.mNodeBounds;
		mMeshBindingMap = node.mMeshBindingMap;
		mTextureBindingMap = node.mTextureBindingMap;
		clearChildren();
		for (size_t i = 0; i < node.mChildren.size(); i++)
		{
			addChild() = *node.mChildren[i];
		}
		return *this;
	}

	//--------------------------------------------------------------------
	bool ModelLODTree::saveNMLModelLODTree(NMLPackage::ModelLODTree &nmlModelLODTree, NodeMap &nodeMap, const TextureExporter &textureExporter) const
	{
		nmlModelLODTree.clear_nodes();
		nodeMap.clear();
		mRoot.saveNMLModelLODTreeNode(nmlModelLODTree, nodeMap, textureExporter);
		return true;
	}

	//--------------------------------------------------------------------
	bool ModelLODTree::loadNMLModelLODTree(const NMLPackage::ModelLODTree &nmlModelLODTree, NodeMap &nodeMap)
	{
		nodeMap.clear();
		return mRoot.loadNMLModelLODTreeNode(nmlModelLODTree, 0, nodeMap);
	}

} // namespace NMLFramework
