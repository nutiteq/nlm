/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "DAEPreprocessor.h"

#include <pugixml.hpp>


namespace NMLFramework
{

	//--------------------------------------------------------------------
	DAEPreprocessor::DAEPreprocessor(const COLLADABU::URI& inputUri, URILoader& loader, const std::string& outputFile)
		: mInputUri(inputUri), mURILoader(loader), mOutputFile(outputFile)
	{
	}

	//--------------------------------------------------------------------
	DAEPreprocessor::~DAEPreprocessor()
	{
	}

	//--------------------------------------------------------------------
	bool DAEPreprocessor::preprocess()
	{
		pugi::xml_document doc;
		if (!doc.load_file(mURILoader.resolve(mInputUri).c_str()))
			return false;

		// Copy all <image> elements under <effect> to image library.
		// This bypasses a long-standing defect in OpenCollada (such <image> elements are ignored by OpenCollada)
		pugi::xpath_node_set images = pugi::xpath_query("COLLADA//effect/profile_COMMON/image").evaluate_node_set(doc);
		for (pugi::xpath_node_set::const_iterator image_it = images.begin(); image_it != images.end(); ++image_it)
		{
			pugi::xml_node image_node = (*image_it).node();
			if (doc.child("COLLADA").child("library_images").empty())
			{
				doc.child("COLLADA").prepend_child("library_images");
			}
			doc.child("COLLADA").child("library_images").append_copy(image_node);
			image_node.parent().remove_child(image_node);
		}

		// Hack-fix for TOMTOM data - explicitly create surface and sampler elements and relink textures
		pugi::xpath_node_set textures = pugi::xpath_query("COLLADA//effect/profile_COMMON//texture").evaluate_node_set(doc);
		for (pugi::xpath_node_set::const_iterator texture_it = textures.begin(); texture_it != textures.end(); ++texture_it)
		{
			pugi::xml_node texture_node = (*texture_it).node();
			pugi::xml_node profile_node = texture_node;
			while (pugi::xpath_query("profile_COMMON").evaluate_node_set(profile_node.parent()).empty())
			{
				profile_node = profile_node.parent();
			}
			if (pugi::xpath_query("newparam").evaluate_node_set(profile_node).empty())
			{
				std::string image_id = texture_node.attribute("texture").value();
				std::string surface_id = image_id + "-surface";
				std::string sampler_id = image_id + "-sampler";
				
				pugi::xml_node newparam_sampler_node = profile_node.prepend_child("newparam");
				newparam_sampler_node.append_attribute("sid").set_value(sampler_id.c_str());
				pugi::xml_node sampler_node = newparam_sampler_node.append_child("sampler2D");
				sampler_node.append_child("source").append_child(pugi::node_cdata).set_value(surface_id.c_str());

				pugi::xml_node newparam_surface_node = profile_node.prepend_child("newparam");
				newparam_surface_node.append_attribute("sid").set_value(surface_id.c_str());
				pugi::xml_node surface_node = newparam_surface_node.append_child("surface");
				surface_node.append_attribute("type").set_value("2D");
				surface_node.append_child("init_from").append_child(pugi::node_cdata).set_value(image_id.c_str());

				texture_node.attribute("texture").set_value(sampler_id.c_str());
			}
		}

		return doc.save_file(mOutputFile.c_str());
	}

} // namespace NMLFramework
