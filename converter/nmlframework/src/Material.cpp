/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "Material.h"

#include <boost/functional/hash.hpp>


namespace NMLFramework
{

	//--------------------------------------------------------------------
	int Material::getTextureCount() const
	{
		int count = 0;
		for (ChannelMap::const_iterator it = mChannelMap.begin(); it != mChannelMap.end(); it++)
		{
			if (it->second.type() == ColorOrTexture::TEXTURE)
				count++;
		}
		return count;
	}

	//--------------------------------------------------------------------
	Material::ColorOrTexture *Material::getColorOrTexture(Channel channel)
	{
		ChannelMap::iterator it = mChannelMap.find(channel);
		if (it == mChannelMap.end())
			return 0;
		return &it->second;
	}

	//--------------------------------------------------------------------
	const Material::ColorOrTexture *Material::getColorOrTexture(Channel channel) const
	{
		ChannelMap::const_iterator it = mChannelMap.find(channel);
		if (it == mChannelMap.end())
			return 0;
		return &it->second;
	}

	//--------------------------------------------------------------------
	void Material::setColorOrTexture(Channel channel, const ColorOrTexture &colorOrTex)
	{
		mChannelMap[channel] = colorOrTex;
	}

	//--------------------------------------------------------------------
	bool Material::saveNMLMaterial(const std::string &id, NMLPackage::Material &nmlMaterial) const
	{
		nmlMaterial.set_id(id);
		nmlMaterial.set_type(mType);
		nmlMaterial.set_culling(mCulling);
		nmlMaterial.set_translucent(mTranslucent);
		for (ChannelMap::const_iterator it = mChannelMap.begin(); it != mChannelMap.end(); it++)
		{
			const NMLPackage::ColorOrTexture &nmlColorOrTexture = it->second;
			switch (it->first)
			{
			case AMBIENT:
				*nmlMaterial.mutable_ambient() = nmlColorOrTexture;
				break;
			case EMISSION:
				*nmlMaterial.mutable_emission() = nmlColorOrTexture;
				break;
			case DIFFUSE:
				*nmlMaterial.mutable_diffuse() = nmlColorOrTexture;
				break;
			}
		}
		return true;
	}

	//--------------------------------------------------------------------
	bool Material::loadNMLMaterial(const NMLPackage::Material &nmlMaterial)
	{
		mType = nmlMaterial.type();
		mCulling = nmlMaterial.culling();
		mTranslucent = nmlMaterial.has_translucent() ? nmlMaterial.translucent() : false;
		mChannelMap.clear();
		if (nmlMaterial.has_ambient())
			mChannelMap[AMBIENT] = nmlMaterial.ambient();
		if (nmlMaterial.has_emission())
			mChannelMap[EMISSION] = nmlMaterial.emission();
		if (nmlMaterial.has_diffuse())
			mChannelMap[DIFFUSE] = nmlMaterial.diffuse();
		return true;
	}

	//--------------------------------------------------------------------
	size_t Material::hash() const
	{
		std::size_t seed = 0;
		boost::hash_combine(seed, (int) mType);
		boost::hash_combine(seed, (int) mCulling);
		boost::hash_combine(seed, (int) mTranslucent);
		for (ChannelMap::const_iterator it = mChannelMap.begin(); it != mChannelMap.end(); it++)
		{
			const ColorOrTexture &colorOrTex = it->second;
			if (colorOrTex.has_texture_id())
				boost::hash_combine(seed, colorOrTex.texture_id());
		}
		return seed;
	}

	//--------------------------------------------------------------------
	bool Material::operator == (const Material &material) const
	{
		if (!(mType == material.mType && mCulling == material.mCulling && mTranslucent == material.mTranslucent && mChannelMap.size() == material.mChannelMap.size()))
			return false;
		for (ChannelMap::const_iterator it = mChannelMap.begin(); it != mChannelMap.end(); it++)
		{
			ChannelMap::const_iterator it2 = material.mChannelMap.find(it->first);
			if (it2 == material.mChannelMap.end())
				return false;
			if (it->second.SerializeAsString() != it2->second.SerializeAsString())
				return false;
		}
		return true;
	}

} // namespace NMLFramework
