/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "Image.h"


namespace NMLFramework
{

	//--------------------------------------------------------------------
	bool Image::load(const COLLADABU::URI& uri, URILoader& loader)
	{
		try
		{
			mCImage.load(loader.resolve(uri).c_str());
			mURI = uri;

			// Hack-fix for images with non-premultiplied alpha - try to detect such textures and premultiply
			if (mCImage.spectrum() == 4) {
				bool premultiplied = true;
				cimg_forXYZ(mCImage, x, y, z)
				{
					unsigned int a = mCImage(x, y, z, 3);
					for (int c = 0; c < 3; c++)
					{
						if (a < mCImage(x, y, z, c))
						{
							premultiplied = false;
						}
					}
				}
				if (!premultiplied)
				{
					cimg_forXYZ(mCImage, x, y, z)
					{
						unsigned int a = mCImage(x, y, z, 3);
						for (int c = 0; c < 3; c++)
						{
							mCImage(x, y, z, c) = static_cast<unsigned char>(mCImage(x, y, z, c) * a / 255);
						}
					}
				}
			}
		}
		catch (const std::exception &ex)
		{
			return false;
		}
		return true;
	}

	//--------------------------------------------------------------------
	void Image::resize(int width, int height)
	{
		if (width == 0 || height == 0)
		{
			// Just clear the image
			mCImage.assign(width, height, mCImage.depth(), mCImage.spectrum());
		}
		else if (mCImage.width() != width || mCImage.height() != height)
		{
			// Resize in linear color space for better quality
			cimg_library::CImg<float> resized(mCImage);
			resized.sRGBtoRGB();
			resized.resize(width, height, resized.depth(), resized.spectrum(), 3); // 3 = linear filter
			resized.RGBtosRGB();
			mCImage.assign(resized);
		}
	}

	//--------------------------------------------------------------------
	void Image::replicate(int xRepeat, int yRepeat)
	{
		if (xRepeat == 1 && yRepeat == 1)
			return;
		if (mCImage.size() == 0)
			return;
		CImage cimage(xRepeat * mCImage.width(), yRepeat * mCImage.height(), mCImage.depth(), mCImage.spectrum());
		for (int y = 0; y < yRepeat; y++)
		{
			for (int x = 0; x < xRepeat; x++)
			{
				cimage.draw_image(x * mCImage.width(), y * mCImage.height(), mCImage);
			}
		}
		mCImage.assign(cimage);
	}

} // namespace NMLFramework

