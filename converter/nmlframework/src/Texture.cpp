/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "Texture.h"
#include "TextureExporter.h"


namespace NMLFramework
{

	//--------------------------------------------------------------------
	Texture Texture::clone() const
	{
		Texture texture(*this);
		texture.mImage.reset(new Image(*mImage));
		return texture;
	}

	//--------------------------------------------------------------------
	ColorRGBA Texture::calculateAvgColor() const
	{
		const Image::CImage &cimage = mImage->getCImage();

		// Just sum over all pixels. Ideally we should convert image to linear space first and afterwards convert sum to gamma space
		std::vector<float> color(cimage.spectrum());
		float scale = 1.0f / cimage.width() / cimage.height() / 255;
		cimg_forXYZC(cimage, x, y, z, c)
		{
			color[c] += cimage(x, y, z, c) * scale;
		}

		// Convert to RGBA
		ColorRGBA nmlColor;
		switch (cimage.spectrum())
		{
		case 1:
			nmlColor = ColorRGBA(color[0], color[0], color[0], 1);
			break;
		case 3:
			nmlColor = ColorRGBA(color[0], color[1], color[2], 1);
			break;
		case 4:
			nmlColor = ColorRGBA(color[0], color[1], color[2], color[3]);
			break;
		}
		return nmlColor;
	}

	//--------------------------------------------------------------------
	int Texture::estimateMemoryFootprint(const TextureExporter &textureExporter) const
	{
		return textureExporter.estimateMemoryFootprint(*this);
	}

	//--------------------------------------------------------------------
	void Texture::resize(float scale)
	{
		const Image::CImage &cimage = mImage->getCImage();

		// Optionally resize image if too large
		int width  = std::max(1, static_cast<int>(cimage.width()  * scale));
		int height = std::max(1, static_cast<int>(cimage.height() * scale));
		if (scale == 0)
			width = height = 0;
		mImage->resize(width, height);
	}

	//--------------------------------------------------------------------
	void Texture::resizeToPOT(float scale, int maxSize)
	{
		const Image::CImage &cimage = mImage->getCImage();

		// Select new size for image, resize
		int width  = std::max(1, static_cast<int>(cimage.width()  * scale));
		int height = std::max(1, static_cast<int>(cimage.height() * scale));
		if (width > maxSize || height > maxSize)
		{
			// Keep aspect ratio intact
			// width  = maxSize * cimage.width()  / std::max(cimage.width(), cimage.height());
			// height = maxSize * cimage.height() / std::max(cimage.width(), cimage.height());
			width  = std::min(maxSize, width);
			height = std::min(maxSize, height);
		}
		width  = roundToPOT(width);
		height = roundToPOT(height);
		mImage->resize(width, height);
		if (width < 4 || height < 4) // special case - assuming we have ETC1 texture compression, do not make textures smaller than 4x4
		{
			width  = std::max(4, width);
			height = std::max(4, height);
			mImage->resize(width, height);
		}
	}

	//--------------------------------------------------------------------
	bool Texture::saveNMLTexture(const std::string &id, NMLPackage::Texture &nmlTexture, const TextureExporter &textureExporter) const
	{
		return textureExporter.exportNMLTexture(*this, id, nmlTexture);
	}

	//--------------------------------------------------------------------
	bool Texture::loadNMLTexture(const NMLPackage::Texture &nmlTexture)
	{
		return TextureExporter::importNMLTexture(nmlTexture, *this);
	}

	//--------------------------------------------------------------------
	int Texture::roundToPOT(int value)
	{
		int potValue = 1;
		while (potValue < value)
			potValue *= 2;
		return potValue;
	}

} // namespace NMLFramework
