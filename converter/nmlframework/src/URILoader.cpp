/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "URILoader.h"


namespace NMLFramework
{
	//--------------------------------------------------------------------
	URILoader::URILoader(const std::string &rootDir) : mRootDir(rootDir)
	{
	}

	//--------------------------------------------------------------------
	std::string URILoader::resolve(const COLLADABU::URI& inputUri)
	{
		if (!inputUri.getScheme().empty())
			throw URIException("Unsupported protocol: " + inputUri.getScheme());

		if (inputUri.getPath().substr(0, 1) == "/")
			return inputUri.toNativePath();

		std::string uriPath = mRootDir + inputUri.getPath();
		std::vector<char> uriPathBuf(uriPath.size() + 1);
		std::copy(uriPath.begin(), uriPath.end(), uriPathBuf.begin());
		COLLADABU::URI::normalizeURIPath(&uriPathBuf[0]);
		uriPath = &uriPathBuf[0];
		std::string inputFileName = COLLADABU::URI(uriPath).toNativePath();
		return inputFileName;
	}

	//--------------------------------------------------------------------
	std::string URILoader::stripUri(const std::string &str)
	{
		size_t i = 0; for (; i < str.size() && isspace(str[i]); ) i++;
		size_t j = str.size(); for (; j > i && isspace(str[j - 1]); ) j--;
		return str.substr(i, j - i);
	}

} // namespace NMLFramework

