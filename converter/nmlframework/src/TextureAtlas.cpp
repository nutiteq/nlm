/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "TextureAtlas.h"

#include <algorithm>


namespace NMLFramework
{

	//--------------------------------------------------------------------
	bool TextureAtlas::TextureTransformation::applyToSubmesh(Submesh &submesh) const
	{
		if (textureNumber == -1)
			return false;

		// Apply transformation
		Submesh::FloatVector &uvs = submesh.getUVs();
		for (size_t i = 0; i + 2 <= uvs.size(); i += 2)
		{
			uvs[i + 0] = uvs[i + 0] * scale[0] + offset[0];
			uvs[i + 1] = uvs[i + 1] * scale[1] + offset[1];
		}
		return true;
	}

	//--------------------------------------------------------------------
	Texture TextureAtlas::getTexture(int num) const
	{
		if (num < 0 || num >= (int) mTextures.size())
			return Texture();
		std::list<Texture>::const_iterator it = mTextures.begin();
		std::advance(it, num);
		return *it;
	}

	//--------------------------------------------------------------------
	TextureAtlas::TextureTransformation TextureAtlas::getTransformation(TextureId id) const
	{
		std::map<TextureId, TextureTransformation>::const_iterator it = mTextureTransformations.find(id);
		if (it == mTextureTransformations.end())
			return TextureTransformation();
		return it->second;
	}

	//--------------------------------------------------------------------
	void TextureAtlasGenerator::reset()
	{
		mAtlasTileMap.clear();
	}

	//--------------------------------------------------------------------
	TextureAtlas::TextureId TextureAtlasGenerator::addTexture(const Submesh &submesh, const Texture &tex, int maxRepeatedPixels)
	{
		static const float UV_EPSILON = 1.0e-6f;

		// Calculate texture size for atlas
		const Image::CImage &cimage = tex.getImage().getCImage();
		int width  = cimage.width();
		int height = cimage.height();
		if (width > mMaxTextureSize || height > mMaxTextureSize)
		{
			width  = mMaxTextureSize * cimage.width()  / std::max(cimage.width(), cimage.height());
			height = mMaxTextureSize * cimage.height() / std::max(cimage.width(), cimage.height());
		}
		width  = Texture::roundToPOT(width);
		height = Texture::roundToPOT(height);

		// Calculate UV bounds
		float min_u, min_v, max_u, max_v;
		calculateSubmeshUVBounds(submesh, min_u, min_v, max_u, max_v);

		// Check UV coordinates, decide whether to reduce atlas size or repeat texture is needed
		Texture::Sampler sampler = tex.getSampler();
		int maxGridWidth = 16384;
		int maxGridHeight = 16384;
		int xRepeat = 1;
		int yRepeat = 1;
		int xOffset = 0;
		int yOffset = 0;
		if (sampler.wrap_s() == Texture::Sampler::REPEAT)
		{
			xOffset = (int) std::floor(min_u + UV_EPSILON);
			xRepeat = std::max(1, (int) std::ceil(max_u - UV_EPSILON) - xOffset);
			if (xRepeat * width > mMaxTextureSize || xRepeat * width * yRepeat * height > maxRepeatedPixels)
			{
				if (xRepeat > 1)
					maxGridWidth = width;
				xRepeat = 1;
			}
		}
		else
		{
			if (min_u + UV_EPSILON < 0 || max_u - UV_EPSILON > 1)
				maxGridWidth = width;
			else
				sampler.set_wrap_s(Texture::Sampler::REPEAT);
		}
		if (sampler.wrap_t() == Texture::Sampler::REPEAT)
		{
			yOffset = (int) std::floor(min_v + UV_EPSILON);
			yRepeat = std::max(1, (int) std::ceil(max_v - UV_EPSILON) - yOffset);
			if (yRepeat * height > mMaxTextureSize || xRepeat * width * yRepeat * height > maxRepeatedPixels)
			{
				if (yRepeat > 1)
					maxGridHeight = height;
				yRepeat = 1;
			}
		}
		else
		{
			if (min_v + UV_EPSILON < 0 || max_v - UV_EPSILON > 1)
				maxGridHeight = height;
			else
				sampler.set_wrap_t(Texture::Sampler::REPEAT);
		}

		// Note: currently we DO NOT add RGBA images - this is due to potential annoying filtering issues on borders. Thus keep each such texture separately.
		if (cimage.spectrum() == 4)
		{
			xRepeat = 1;
			yRepeat = 1;
			maxGridWidth = width;
			maxGridHeight = height;
		}

		// Check if this texture needs to be added or we already have it.
		TextureAtlas::TextureId id = (int) mAtlasTileMap.size() + 1;
		for (AtlasTileMap::const_iterator it = mAtlasTileMap.begin(); it != mAtlasTileMap.end(); it++)
		{
			const AtlasTile &tile = it->second;
			if (tile.image == tex.getImage() && tile.sampler.SerializeAsString() == sampler.SerializeAsString() && tile.xOffset == xOffset && tile.yOffset == yOffset && tile.maxGridWidth == maxGridWidth && tile.maxGridHeight == maxGridHeight)
			{
				id = it->first;
				break;
			}
		}

		// Update tile data, store texture
		AtlasTile &tile = mAtlasTileMap[id];
		tile.xSize = std::max(tile.xSize, width * xRepeat);
		tile.ySize = std::max(tile.ySize, height * yRepeat);
		tile.xRepeat = std::max(tile.xRepeat, xRepeat);
		tile.yRepeat = std::max(tile.yRepeat, yRepeat);
		tile.xOffset = xOffset;
		tile.yOffset = yOffset;
		tile.maxGridWidth = maxGridWidth;
		tile.maxGridHeight = maxGridHeight;
		tile.image = tex.getImage();
		tile.sampler = sampler;
		return id;
	}

	//--------------------------------------------------------------------
	TextureAtlas TextureAtlasGenerator::generate(int maxAtlasTextureSize, bool squareAtlas) const
	{
		typedef std::map<std::string, AtlasTileMap> SortedAtlasTileMap;

		// Presort tiles based on minimum tile size.
		SortedAtlasTileMap sortedAtlasTileMap;
		for (AtlasTileMap::const_iterator it = mAtlasTileMap.begin(); it != mAtlasTileMap.end(); it++)
		{
			TextureAtlas::TextureId id = it->first;
			const AtlasTile &tile = it->second;
			std::stringstream stream;
			stream << "T" << tile.image.getCImage().spectrum() << "S" << tile.sampler.SerializeAsString();
			std::string key = stream.str();
			sortedAtlasTileMap[key][id] = tile;
		}

		// Build atlas from presorted tiles.
		TextureAtlas atlas;
		for (SortedAtlasTileMap::iterator it = sortedAtlasTileMap.begin(); it != sortedAtlasTileMap.end(); it++)
		{
			AtlasTileMap &tileMap = it->second;
			while (!tileMap.empty())
			{
				// Add textures to atlas
				if (addToAtlas(atlas, tileMap, maxAtlasTextureSize, squareAtlas))
					continue;

				// If no progress was made and in square mode, try non-square mode
				if (squareAtlas)
				{
					if (addToAtlas(atlas, tileMap, maxAtlasTextureSize, false))
						continue;
				}

				// If still no progress was made, reduce size of all remaining textures
				for (AtlasTileMap::iterator tileIt = tileMap.begin(); tileIt != tileMap.end(); tileIt++)
				{
					AtlasTile &tile = tileIt->second;
					tile.xSize = std::max(tile.xRepeat, tile.xSize / 2);
					tile.ySize = std::max(tile.yRepeat, tile.ySize / 2);
				}
			}
		}
		return atlas;
	}

	//--------------------------------------------------------------------
	void TextureAtlasGenerator::calculateSubmeshUVBounds(const Submesh &submesh, float &min_u, float &min_v, float &max_u, float &max_v)
	{
		static const float MAX_FLOAT = std::numeric_limits<float>::max();

		// Find minimum/maximum coordinates
		min_u =  MAX_FLOAT; min_v =  MAX_FLOAT;
		max_u = -MAX_FLOAT; max_v = -MAX_FLOAT;
		const Submesh::FloatVector &uvs = submesh.getUVs();
		for (size_t i = 0; i + 2 <= uvs.size(); i += 2)
		{
			float u = uvs[i + 0], v = uvs[i + 1];
			min_u = std::min(u, min_u);
			min_v = std::min(v, min_v);
			max_u = std::max(u, max_u);
			max_v = std::max(v, max_v);
		}
		if (min_u > max_u)
			min_u = max_u = 0;
		if (min_v > max_v)
			min_v = max_v = 0;
	}

	//--------------------------------------------------------------------
	double TextureAtlasGenerator::evaluateGridQuality(const AtlasGrid &grid)
	{
		// Prefer grid as large as possible, with as few empty cells as possible and as square as possible.
		// Also prefer tight packing in case of bigger tiles - loosely packed tiles mean more wasted space in that case.
		int emptyTiles = 0;
		for (size_t i = 0; i < grid.data.size(); i++)
		{
			if (!grid.data[i])
				emptyTiles++;
		}
		int fullTiles = (int) grid.data.size() - emptyTiles;
		if (fullTiles == 0)
			return -std::numeric_limits<double>::infinity();
		double emptyPenalty = (double) grid.tileSize / (double) PENALTY_TILE_SIZE;
		return 2 * grid.tileSize * grid.tileSize * (fullTiles - emptyTiles * emptyPenalty) - std::abs(grid.width - grid.height);
	}

	//--------------------------------------------------------------------
	bool TextureAtlasGenerator::addToAtlas(TextureAtlas &atlas, AtlasTileMap &tileMap, int maxAtlasTextureSize, bool squareAtlas) const
	{
		typedef std::vector<std::pair<TextureAtlas::TextureId, AtlasTile> > TileVector;

		// Generate sorted tile vector, keep bigger tiles in front
		TileVector sortedTiles;
		for (AtlasTileMap::const_iterator it = tileMap.begin(); it != tileMap.end(); it++)
		{
			TileVector::iterator it2;
			for (it2 = sortedTiles.begin(); it2 != sortedTiles.end(); it2++)
			{
				if (std::max(it->second.xSize, it->second.ySize) >= std::max(it2->second.xSize, it2->second.ySize))
					break;
			}
			sortedTiles.insert(it2, *it);
		}

		// Find tile size and maximum grid size
		int tileSize = maxAtlasTextureSize;
		int totalXSize = 0, totalYSize = 0;
		for (AtlasTileMap::const_iterator it = tileMap.begin(); it != tileMap.end(); it++)
		{
			const AtlasTile &tile = it->second;
			if (tile.xSize > 0)
				tileSize = std::min(tileSize, tile.xSize / tile.xRepeat);
			if (tile.ySize > 0)
				tileSize = std::min(tileSize, tile.ySize / tile.yRepeat);
			totalXSize += tile.xSize;
			totalYSize += tile.ySize;
		}
		int maxGridWidth  = Texture::roundToPOT(std::min(maxAtlasTextureSize, totalXSize));
		int maxGridHeight = Texture::roundToPOT(std::min(maxAtlasTextureSize, totalYSize));
		if (squareAtlas)
			maxGridWidth = maxGridHeight = std::max(maxGridWidth, maxGridHeight);

		// Find good grid layout by trying all possible layouts
		AtlasGrid bestGrid(maxGridWidth, maxGridHeight, tileSize);
		for (int gridHeight = maxGridHeight; gridHeight >= tileSize; gridHeight /= 2)
		{
			for (int gridWidth = maxGridWidth; gridWidth >= tileSize; gridWidth /= 2)
			{
				if (squareAtlas && gridWidth != gridHeight)
					continue;

				AtlasGrid grid(gridWidth, gridHeight, tileSize);
				for (TileVector::const_iterator it = sortedTiles.begin(); it != sortedTiles.end(); it++)
				{
					addToGrid(grid, it->first, it->second);
				}
				if (evaluateGridQuality(grid) > evaluateGridQuality(bestGrid))
					bestGrid = grid;
			}
		}

		// Generate atlas record for given layout
		Texture texture;
		for (int y = 0; y < bestGrid.height; y += tileSize)
		{
			for (int x = 0; x < bestGrid.width; x += tileSize)
			{
				TextureAtlas::TextureId id = bestGrid.at(x, y);
				if (tileMap.count(id) == 0)
					continue;
				AtlasTile tile = tileMap[id];
				tileMap.erase(id);

				// Generate transformation for this texture id
				TextureAtlas::TextureTransformation trans;
				trans.textureNumber = (int) atlas.mTextures.size();
				trans.scale[0] = (double) (tile.xSize / tile.xRepeat) / (double) bestGrid.width;
				trans.scale[1] = (double) (tile.ySize / tile.yRepeat) / (double) bestGrid.height;
				trans.offset[0] = (double) x / (double) bestGrid.width  - (double) tile.xOffset * trans.scale[0];
				trans.offset[1] = (double) y / (double) bestGrid.height - (double) tile.yOffset * trans.scale[1];
				atlas.mTextureTransformations[id] = trans;

				// Copy image to atlas texture
				Image image(tile.image);
				if (image.getCImage().size() == 0)
				{
					image.getCImage().assign(1, 1, 1, 3); // create one pixel RGB image that can be repeated
					image.getCImage().fill(255);
				}
				image.resize(tile.xSize / tile.xRepeat, tile.ySize / tile.yRepeat);
				image.replicate(tile.xRepeat, tile.yRepeat);
				if (texture.getImage().getCImage().size() == 0)
				{
					texture.getImage().getCImage().assign(bestGrid.width, bestGrid.height, 1, image.getCImage().spectrum());
					texture.getImage().getCImage().fill(255);
				}
				texture.getImage().getCImage().draw_image(x, bestGrid.height - y - tile.ySize, image.getCImage());
				texture.setSampler(tile.sampler);
			}
		}
		if (texture.getImage().getCImage().size() == 0)
			return false;
		atlas.mTextures.push_back(texture);
		return true;
	}

	//--------------------------------------------------------------------
	bool TextureAtlasGenerator::addToGrid(AtlasGrid &grid, TextureAtlas::TextureId id, const AtlasTile &tile) const
	{
		// First, check tile constraints against grid. This is required if texture needs repeating
		if (grid.width > tile.maxGridWidth || grid.height > tile.maxGridHeight)
			return false;

		// Find first empty cell for atlas texture. And fill it with texture id.
		for (int y = 0; y <= grid.height - tile.ySize; y += grid.tileSize)
		{
			for (int x = 0; x <= grid.width - tile.xSize; x += grid.tileSize)
			{
				bool found = true;
				for (int h = 0; h < tile.ySize && found; h += grid.tileSize)
				{
					for (int w = 0; w < tile.xSize && found; w += grid.tileSize)
					{
						if (grid.at(x + w, y + h))
							found = false;
					}
				}
				if (found)
				{
					for (int h = 0; h < tile.ySize; h += grid.tileSize)
					{
						for (int w = 0; w < tile.xSize; w += grid.tileSize)
						{
							grid.at(x + w, y + h) = id;
						}
					}
					return true;
				}
			}
		}
		return false;
	}

} // namespace NMLFramework
