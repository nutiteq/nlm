/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "Model.h"
#include "TextureAtlas.h"
#include "Utils.h"
#include "GeomUtils.h"

#include <set>
#include <iterator>


namespace NMLFramework
{

	//--------------------------------------------------------------------
	Texture *Model::getTexture(const std::string &id)
	{
		TextureMap::iterator it = mTextureMap.find(id);
		if (it != mTextureMap.end())
			return &it->second;
		return 0;
	}

	//--------------------------------------------------------------------
	const Texture *Model::getTexture(const std::string &id) const
	{
		TextureMap::const_iterator it = mTextureMap.find(id);
		if (it != mTextureMap.end())
			return &it->second;
		return 0;
	}

	//--------------------------------------------------------------------
	std::string Model::storeTexture(const Texture &tex)
	{
		std::string id = findUniqueId("T", mTextureMap);
		mTextureMap[id] = tex;
		return id;
	}

	//--------------------------------------------------------------------
	int Model::countTextureInstances(const std::string &id) const
	{
		int count = 0;
		for (MeshInstanceList::const_iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			const MeshInstance & meshInstance = *meshInstanceIt;
			for (MeshInstance::MaterialMap::const_iterator materialIt = meshInstance.getMaterialMap().begin(); materialIt != meshInstance.getMaterialMap().end(); materialIt++)
			{
				const Material & material = materialIt->second;
				for (Material::ChannelMap::const_iterator channelIt = material.getChannelMap().begin(); channelIt != material.getChannelMap().end(); channelIt++)
				{
					if (channelIt->second.type() == Material::ColorOrTexture::TEXTURE)
					{
						if (id == channelIt->second.texture_id())
							count++;
					}
				}
			}
		}
		return count;
	}

	//--------------------------------------------------------------------
	Mesh *Model::getMesh(const std::string &id)
	{
		MeshMap::iterator it = mMeshMap.find(id);
		if (it != mMeshMap.end())
			return &it->second;
		return 0;
	}

	//--------------------------------------------------------------------
	const Mesh *Model::getMesh(const std::string &id) const
	{
		MeshMap::const_iterator it = mMeshMap.find(id);
		if (it != mMeshMap.end())
			return &it->second;
		return 0;
	}

	//--------------------------------------------------------------------
	std::string Model::storeMesh(const Mesh &mesh)
	{
		std::string id = findUniqueId("M", mMeshMap);
		mMeshMap[id] = mesh;
		return id;
	}

	//--------------------------------------------------------------------
	int Model::countMeshInstances(const std::string &id) const
	{
		int count = 0;
		for (MeshInstanceList::const_iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			const MeshInstance & meshInstance = *meshInstanceIt;
			if (id == meshInstance.getMeshId())
				count++;
		}
		return count;
	}

	//--------------------------------------------------------------------
	int Model::getVertexCount() const
	{
		int count = 0;
		for (MeshInstanceList::const_iterator it = mMeshInstanceList.begin(); it != mMeshInstanceList.end(); it++)
		{
			const Mesh *mesh = getMesh(it->getMeshId());
			if (mesh != 0)
				count += mesh->getVertexCount();
		}
		return count;
	}

	//--------------------------------------------------------------------
	int Model::getMaterialCount() const
	{
		int count = 0;
		for (MeshInstanceList::const_iterator it = mMeshInstanceList.begin(); it != mMeshInstanceList.end(); it++)
		{
			const Mesh *mesh = getMesh(it->getMeshId());
			if (mesh != 0)
				count += mesh->getSubmeshList().size();
		}
		return count;
	}

	//--------------------------------------------------------------------
	void Model::setVertexIds(int id)
	{
		for (MeshMap::iterator it = mMeshMap.begin(); it != mMeshMap.end(); it++)
		{
			it->second.setVertexIds(id);
		}
	}
	
	//--------------------------------------------------------------------
	Bounds3 Model::calculateBounds(const Matrix4 &transform) const
	{
		Bounds3 bounds;
		for (MeshInstanceList::const_iterator it = mMeshInstanceList.begin(); it != mMeshInstanceList.end(); it++)
		{
			const Mesh *mesh = getMesh(it->getMeshId());
			if (mesh == 0)
				continue;
			bounds.add(mesh->calculateBounds(transform * it->getTransform()));
		}
		return bounds;
	}

	//--------------------------------------------------------------------
	std::vector<Vector3> Model::calculateFloorplan(float &minHeight, float &maxHeight) const
	{
		std::vector<Vector3> points;
		minHeight = std::numeric_limits<float>::max();
		maxHeight = -minHeight;
		for (MeshInstanceList::const_iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			const Mesh *mesh = getMesh(meshInstanceIt->getMeshId());
			if (mesh == 0)
				continue;
			for (Mesh::SubmeshList::const_iterator submeshIt = mesh->getSubmeshList().begin(); submeshIt != mesh->getSubmeshList().end(); submeshIt++)
			{
				const Submesh::FloatVector &posVector = submeshIt->getPositions();
				for (size_t i = 0; i + 3 <= posVector.size(); i += 3)
				{
					Vector3 point(posVector[i + 0], posVector[i + 1], 0);
					points.push_back(point);
					minHeight = std::min(minHeight, posVector[i + 2]);
					maxHeight = std::max(maxHeight, posVector[i + 2]);
				}
			}
		}
		return convexHull(points);
	}

	//--------------------------------------------------------------------
	void Model::clear()
	{
		mMeshInstanceList.clear();
		mMeshMap.clear();
		mTextureMap.clear();
	}

	//--------------------------------------------------------------------
	void Model::concat(const Model &model)
	{
		// Store textures, create remapping table
		std::map<std::string, std::string> textureIdMap;
		for (TextureMap::const_iterator it = model.mTextureMap.begin(); it != model.mTextureMap.end(); it++)
		{
			textureIdMap[it->first] = storeTexture(it->second);
		}

		// Store meshes, create remapping table
		std::map<std::string, std::string> meshIdMap;
		for (MeshMap::const_iterator it = model.mMeshMap.begin(); it != model.mMeshMap.end(); it++)
		{
			meshIdMap[it->first] = storeMesh(it->second);
		}

		// Remap texture and mesh references in mesh instances
		for (MeshInstanceList::const_iterator meshInstanceIt = model.getMeshInstanceList().begin(); meshInstanceIt != model.getMeshInstanceList().end(); meshInstanceIt++)
		{
			MeshInstance meshInstance = *meshInstanceIt;

			// Remap mesh id
			meshInstance.setMeshId(meshIdMap[meshInstance.getMeshId()]);

			// Remap texture ids in materials
			for (MeshInstance::MaterialMap::iterator materialIt = meshInstance.getMaterialMap().begin(); materialIt != meshInstance.getMaterialMap().end(); materialIt++)
			{
				Material & material = materialIt->second;
				for (Material::ChannelMap::iterator channelIt = material.getChannelMap().begin(); channelIt != material.getChannelMap().end(); channelIt++)
				{
					if (channelIt->second.type() == Material::ColorOrTexture::TEXTURE)
						channelIt->second.set_texture_id(textureIdMap[channelIt->second.texture_id()]);
				}
			}
			mMeshInstanceList.push_back(meshInstance);
		}
	}

	//--------------------------------------------------------------------
	void Model::resizeTextures(float scale)
	{
		for (Model::TextureMap::iterator it = mTextureMap.begin(); it != mTextureMap.end(); it++)
		{
			it->second.resize(scale);
		}
	}

	//--------------------------------------------------------------------
	void Model::resizeTexturesToPOT(int maxSize)
	{
		for (Model::TextureMap::iterator it = mTextureMap.begin(); it != mTextureMap.end(); it++)
		{
			it->second.resizeToPOT(1, maxSize);
		}
	}

	//--------------------------------------------------------------------
	void Model::stripPointsLines()
	{
		for (MeshMap::iterator meshIt = mMeshMap.begin(); meshIt != mMeshMap.end(); meshIt++)
		{
			Mesh::SubmeshList &submeshList = meshIt->second.getSubmeshList();
			for (Mesh::SubmeshList::iterator submeshIt = submeshList.begin(); submeshIt != submeshList.end(); )
			{
				switch (submeshIt->getType())
				{
				case NMLPackage::Submesh::TRIANGLES:
				case NMLPackage::Submesh::TRIANGLE_STRIPS:
				case NMLPackage::Submesh::TRIANGLE_FANS:
					submeshIt++;
					break;
				default:
					submeshIt = submeshList.erase(submeshIt);
					break;
				}
			}
		}
	}

	//--------------------------------------------------------------------
	void Model::calculateAvgColors()
	{
		for (MeshInstanceList::iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			for (MeshInstance::MaterialMap::iterator materialIt = meshInstanceIt->getMaterialMap().begin(); materialIt != meshInstanceIt->getMaterialMap().end(); materialIt++)
			{
				// Ideally we should average only texels that are used by the mesh instance (could be subset of the texture).
				// But as this is complicated, we just calculate average color of each texture.
				Material & material = materialIt->second;
				for (Material::ChannelMap::iterator channelIt = material.getChannelMap().begin(); channelIt != material.getChannelMap().end(); channelIt++)
				{
					if (channelIt->second.type() == Material::ColorOrTexture::TEXTURE)
					{
						const Texture *texture = getTexture(channelIt->second.texture_id());
						if (texture != 0)
							*channelIt->second.mutable_color() = texture->calculateAvgColor().getNMLColor();
					}
				}
			}
		}
	}

	//--------------------------------------------------------------------
	void Model::calculateMemoryFootprint(const TextureExporter &textureExporter)
	{
		// Estimate and store node footprint
		int meshFootprint = 0, textureFootprint = 0;
		for (Model::MeshMap::const_iterator meshIt = mMeshMap.begin(); meshIt != mMeshMap.end(); meshIt++)
		{
			for (Mesh::SubmeshList::const_iterator submeshIt =  meshIt->second.getSubmeshList().begin(); submeshIt !=  meshIt->second.getSubmeshList().end(); submeshIt++)
			{
				meshFootprint += submeshIt->estimateMemoryFootprint();
			}
		}
		for (Model::TextureMap::const_iterator textureIt = mTextureMap.begin(); textureIt != mTextureMap.end(); textureIt++)
		{
			textureFootprint += textureIt->second.estimateMemoryFootprint(textureExporter);
		}
		mMeshFootprint = meshFootprint;
		mTextureFootprint = textureFootprint;
	}

	//--------------------------------------------------------------------
	void Model::applyLightingModel(const LightingModel &lighting, const Matrix4 &transform)
	{
		Matrix4 transformIT = transform;
		transformIT.setTrans(Vector3::ZERO);
		transformIT = transformIT.inverse().transpose();

		// Process all mesh instances
		for (MeshInstanceList::iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			MeshInstance &meshInstance = *meshInstanceIt;
			const Mesh *oldMesh = getMesh(meshInstance.getMeshId());
			if (oldMesh == 0)
			{
				assert(false);
				continue;
			}

			// Clone mesh - when mesh is shared, we will make extra copies
			std::string newMeshId = storeMesh(*oldMesh);
			meshInstance.setMeshId(newMeshId);

			// Concatenate submeshes with constant diffuse colors
			Mesh &newMesh = *getMesh(newMeshId);
			for (Mesh::SubmeshList::iterator submeshIt = newMesh.getSubmeshList().begin(); submeshIt != newMesh.getSubmeshList().end(); submeshIt++)
			{
				Submesh &submesh = *submeshIt;
				const Material *material = meshInstance.getMaterial(submesh.getMaterialId());
				if (material == 0)
				{
					assert(false);
					continue;
				}

				// Check that mesh has normals, otherwise we can not continue
				if (submesh.getNormals().empty())
					continue;

				// Check that material has diffuse channel
				Material newMaterial(*material);
				Material::ColorOrTexture *colorOrTexture = newMaterial.getColorOrTexture(Material::DIFFUSE);
				if (colorOrTexture == 0)
					continue;

				// Calculate base color
				ColorRGBA baseColor(1, 1, 1, 1);
				if (colorOrTexture->type() == Material::ColorOrTexture::COLOR)
				{
					baseColor = ColorRGBA(colorOrTexture->color().r(), colorOrTexture->color().g(), colorOrTexture->color().b(), colorOrTexture->color().a());
					*colorOrTexture->mutable_color() = ColorRGBA(1, 1, 1, 1).getNMLColor();
				}

				// Store new material
				std::string newMaterialId = meshInstance.storeMaterial(newMaterial);
				submesh.setMaterialId(newMaterialId);

				// Create/update submesh colors array
				submesh.getColors().resize(submesh.getPositions().size() / 3 * 4, 1);
				for (size_t i = 0; i < submesh.getColors().size() / 4; i++)
				{
					Vector3 position(submesh.getPositions()[i * 3 + 0], submesh.getPositions()[i * 3 + 1], submesh.getPositions()[i * 3 + 2]);
					Vector3 normal(submesh.getNormals()[i * 3 + 0], submesh.getNormals()[i * 3 + 1], submesh.getNormals()[i * 3 + 2]);
					ColorRGBA lightColor = lighting.calculateColor(transform * position, transformIT * normal);
					float * c = &submesh.getColors()[i * 4];
					c[0] *= lightColor.r * baseColor.r;
					c[1] *= lightColor.g * baseColor.g;
					c[2] *= lightColor.b * baseColor.b;
					c[3] *= lightColor.a * baseColor.a;
				}

				// Remove normal info
				submesh.getNormals().clear();
			}
		}
	}

	//--------------------------------------------------------------------
	void Model::mergeMaterials()
	{
		typedef std::tr1::unordered_map<Material, std::string, boost::hash<Material> > MaterialHashMap;

		// Rename materials - keep only one instance for equal materials
		for (MeshInstanceList::iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			MeshInstance &meshInstance = *meshInstanceIt;
			const Mesh *oldMesh = getMesh(meshInstance.getMeshId());
			if (oldMesh == 0)
			{
				assert(false);
				continue;
			}

			std::map<std::string, std::string> materialIdMap;

			// Find unique id for each material - when materials are really equal, keep only one instance.
			MaterialHashMap materialMap;
			for (MeshInstance::MaterialMap::iterator materialIt = meshInstance.getMaterialMap().begin(); materialIt != meshInstance.getMaterialMap().end(); materialIt++)
			{
				if (materialMap.count(materialIt->second) == 0)
				{
					materialMap[materialIt->second] = materialIt->first;
				}
				materialIdMap[materialIt->first] = materialMap[materialIt->second];
			}

			// Check if anything was merged... Otherwise do nothing.
			if (materialMap.size() == materialIdMap.size())
			{
				continue;
			}

			// Clone mesh - when mesh is shared, we will make extra copies
			std::string newMeshId = storeMesh(*oldMesh);
			meshInstance.setMeshId(newMeshId);

			// Change submesh material ids
			Mesh &newMesh = *getMesh(newMeshId);
			for (Mesh::SubmeshList::iterator submeshIt = newMesh.getSubmeshList().begin(); submeshIt != newMesh.getSubmeshList().end(); submeshIt++)
			{
				submeshIt->setMaterialId(materialIdMap[submeshIt->getMaterialId()]);
			}

			// Create new material map, keep only referenced materials
			meshInstance.getMaterialMap().clear();
			for (MaterialHashMap::const_iterator it = materialMap.begin(); it != materialMap.end(); it++)
			{
				meshInstance.getMaterialMap()[it->second] = it->first;
			}
		}
	}

	//--------------------------------------------------------------------
	void Model::mergeSubmeshes()
	{
		// Collect referenced mesh ids
		std::set<std::string> referencedMeshIds;
		for (MeshInstanceList::iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			referencedMeshIds.insert(meshInstanceIt->getMeshId());
		}

		// Combine submeshes sharing the same material
		for (MeshMap::iterator meshIt = mMeshMap.begin(); meshIt != mMeshMap.end(); meshIt++)
		{
			if (referencedMeshIds.count(meshIt->first) == 0)
				continue;
			Mesh &mesh = meshIt->second;
			Mesh::SubmeshList submeshes;
			for (Mesh::SubmeshList::const_iterator submeshIt = mesh.getSubmeshList().begin(); submeshIt != mesh.getSubmeshList().end(); submeshIt++)
			{
				Mesh::SubmeshList::iterator it;
				for (it = submeshes.begin(); it != submeshes.end(); it++)
				{
					Submesh &submesh = *it;
					if (submesh.concat(*submeshIt))
						break;
				}
				if (it == submeshes.end())
					submeshes.push_back(*submeshIt);
			}
			mesh.getSubmeshList().assign(submeshes.begin(), submeshes.end());
		}
	}
	
	//--------------------------------------------------------------------
	void Model::splitSubmeshes(int maxVertices)
	{
		std::map<std::string, std::vector<std::string> > meshIdMap;

		// Mark all submeshes that contain at least maxVertices
		for (MeshMap::iterator meshIt = mMeshMap.begin(); meshIt != mMeshMap.end(); meshIt++)
		{
			std::string meshId = meshIt->first;
			Mesh &mesh = meshIt->second;
			if (mesh.getSubmeshList().size() < 2)
				continue;

			for (Mesh::SubmeshList::iterator submeshIt = mesh.getSubmeshList().begin(); submeshIt != mesh.getSubmeshList().end(); )
			{
				Submesh &submesh = *submeshIt;
				if (submesh.getVertexCount() < maxVertices)
				{
					submeshIt++;
					continue;
				}
				Mesh newMesh;
				newMesh.getSubmeshList().push_back(submesh);
				std::string newMeshId = storeMesh(newMesh);
				meshIdMap[meshId].push_back(newMeshId);
				submeshIt = mesh.getSubmeshList().erase(submeshIt);
			}
		}

		// Add all marked submeshes as separate meshes
		for (MeshInstanceList::iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			MeshInstance &meshInstance = *meshInstanceIt;
			std::vector<std::string> newMeshIds = meshIdMap[meshInstance.getMeshId()];
			for (size_t i = 0; i < newMeshIds.size(); i++)
			{
				MeshInstance newMeshInstance = meshInstance;
				newMeshInstance.setMeshId(newMeshIds[i]);
				meshInstanceIt = mMeshInstanceList.insert(++meshInstanceIt, newMeshInstance);
			}
		}
	}

	//--------------------------------------------------------------------
	void Model::generateVertexColors(int maxVertices)
	{
		// Process all mesh instances
		for (MeshInstanceList::iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			MeshInstance &meshInstance = *meshInstanceIt;
			const Mesh *oldMesh = getMesh(meshInstance.getMeshId());
			if (oldMesh == 0)
			{
				assert(false);
				continue;
			}

			// Clone mesh - when mesh is shared, we will make extra copies
			if (countMeshInstances(meshInstance.getMeshId()) > 1) // disable shared mesh cloning for now - it is possibly pessimizing transformation and can increase model size
			{
				continue;
			}
			std::string newMeshId = storeMesh(*oldMesh);
			meshInstance.setMeshId(newMeshId);

			// Concatenate submeshes with constant diffuse colors
			std::string newMaterialId;
			Mesh &newMesh = *getMesh(newMeshId);
			for (Mesh::SubmeshList::iterator submeshIt = newMesh.getSubmeshList().begin(); submeshIt != newMesh.getSubmeshList().end(); submeshIt++)
			{
				Submesh &submesh = *submeshIt;
				if (submesh.getVertexCount() > maxVertices) // ignore meshes with many vertices - otherwise we will make models simply bigger
					continue;

				const Material *material = meshInstance.getMaterial(submesh.getMaterialId());
				if (material == 0)
				{
					assert(false);
					continue;
				}

				// Only meshes with constant diffuse colors can be concatenated.
				if (material->getTextureCount() != 0)
					continue;
				const Material::ColorOrTexture *colorOrTexture = material->getColorOrTexture(Material::DIFFUSE);
				if (colorOrTexture == 0)
					continue;
				if (colorOrTexture->type() != Material::ColorOrTexture::COLOR)
					continue;

				// Create new material with constant white color (if not created yet)
				if (newMaterialId.empty())
				{
					Material newMaterial(*material);
					Material::ColorOrTexture color;
					color.set_type(Material::ColorOrTexture::COLOR);
					*color.mutable_color() = ColorRGBA(1, 1, 1, 1).getNMLColor();
					newMaterial.getChannelMap()[Material::DIFFUSE] = color;
					newMaterialId = meshInstance.storeMaterial(newMaterial);
				}

				// Update submesh colors array
				submesh.setMaterialId(newMaterialId);
				submesh.getColors().resize(submesh.getPositions().size() / 3 * 4, 1);
				for (size_t i = 0; i < submesh.getColors().size() / 4; i++)
				{
					float * c = &submesh.getColors()[i * 4];
					c[0] *= colorOrTexture->color().r();
					c[1] *= colorOrTexture->color().g();
					c[2] *= colorOrTexture->color().b();
					c[3] *= colorOrTexture->color().a();
				}
			}
		}
	}

	//--------------------------------------------------------------------
	namespace detail
	{
		struct AtlasRecord
		{
			MeshInstance *meshInstance;
			Submesh *submesh;
			const Material *material;
			const Texture *texture;
			TextureAtlas::TextureId textureId;
			
			AtlasRecord(MeshInstance *mi, Submesh *sm, const Material *mat, const Texture *tex) : meshInstance(mi), submesh(sm), material(mat), texture(tex), textureId(-1) { }
		};
	}

	//--------------------------------------------------------------------
	void Model::generateTextureAtlas(int maxTextureSize, int maxAtlasTextureSize, bool squareAtlas, int maxVertices, int maxRepeatedPixels)
	{
		typedef detail::AtlasRecord AtlasRecord;
		typedef std::list<AtlasRecord> AtlasRecordList;

		// Make copies of all submeshes, create list of atlas records, add textures to atlas
		AtlasRecordList atlasRecords;
		TextureAtlasGenerator atlasGenerator(maxTextureSize);
		for (MeshInstanceList::iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			MeshInstance &meshInstance = *meshInstanceIt;
			const Mesh *oldMesh = getMesh(meshInstance.getMeshId());
			if (oldMesh == 0)
			{
				assert(false);
				continue;
			}

			// Clone mesh - when mesh is shared, we will make extra copies
			if (countMeshInstances(meshInstance.getMeshId()) > 1) // disable shared mesh cloning for now - it is possibly pessimizing transformation and can increase model size
			{
				continue;
			}
			std::string newMeshId = storeMesh(*oldMesh);
			meshInstance.setMeshId(newMeshId);

			// Add submeshes to atlas
			Mesh &newMesh = *getMesh(newMeshId);
			for (Mesh::SubmeshList::iterator submeshIt = newMesh.getSubmeshList().begin(); submeshIt != newMesh.getSubmeshList().end(); submeshIt++)
			{
				Submesh &submesh = *submeshIt;
				if (submesh.getVertexCount() > maxVertices)
					continue;

				const Material *material = meshInstance.getMaterial(submesh.getMaterialId());
				if (material == 0)
				{
					assert(false);
					continue;
				}

				// Only meshes with diffuse textures can be added to atlas. Otherwise things will get too complicated as single uv-coordinate set is used.
				if (material->getTextureCount() != 1)
					continue;
				const Material::ColorOrTexture *colorOrTexture = material->getColorOrTexture(Material::DIFFUSE);
				if (colorOrTexture == 0)
					continue;
				if (colorOrTexture->type() != Material::ColorOrTexture::TEXTURE)
					continue;
				const Texture *texture = getTexture(colorOrTexture->texture_id());
				if (texture == 0)
				{
					assert(false);
					continue;
				}

				// Create atlas record
				AtlasRecord record(&meshInstance, &submesh, material, texture);
				record.textureId = atlasGenerator.addTexture(submesh, *texture, maxRepeatedPixels);
				atlasRecords.push_back(record);
			}
		}

		// Generate atlas textures
		TextureAtlas atlas = atlasGenerator.generate(maxAtlasTextureSize, squareAtlas);
		
		// Replace all atlas textures
		std::map<int, std::string> atlasTextureIds;
		std::map<std::string, std::string> atlasMaterialIds;
		for (AtlasRecordList::iterator recordIt = atlasRecords.begin(); recordIt != atlasRecords.end(); recordIt++)
		{
			AtlasRecord &record = *recordIt;

			// Lookup atlas transformation, apply this to submesh
			TextureAtlas::TextureTransformation trans = atlas.getTransformation(record.textureId);
			if (!trans.applyToSubmesh(*record.submesh))
				continue;

			// Store atlas texture
			std::string newTextureId = atlasTextureIds[trans.textureNumber];
			if (newTextureId.empty())
			{
				newTextureId = storeTexture(atlas.getTexture(trans.textureNumber));
				atlasTextureIds[trans.textureNumber] = newTextureId;
			}

			// Clone material, update material to use atlas texture
			Material newMaterial(*record.material);
			newMaterial.getColorOrTexture(Material::DIFFUSE)->set_texture_id(newTextureId);
			std::string newMaterialId = record.meshInstance->storeMaterial(newMaterial);
			record.submesh->setMaterialId(newMaterialId);
		}
	}

	//--------------------------------------------------------------------
	void Model::flatten(int maxExtraVertices, int maxMeshVertices)
	{
		if (mMeshInstanceList.empty())
			return;

		Mesh combinedMesh;
		MeshInstance combinedMeshInstance;

		// Apply instance transform to all meshes
		for (MeshInstanceList::iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); )
		{
			const MeshInstance &meshInstance = *meshInstanceIt;
			const Mesh *mesh = getMesh(meshInstance.getMeshId());
			if (mesh == 0)
			{
				assert(false);
				meshInstanceIt++;
				continue;
			}
			if (countMeshInstances(meshInstance.getMeshId()) * mesh->getVertexCount() - mesh->getVertexCount() > maxExtraVertices) // if mesh flattening causes many extra vertices, ignore
			{
				meshInstanceIt++;
				continue;
			}
			if (mesh->getVertexCount() > maxMeshVertices)
			{
				meshInstanceIt++;
				continue;
			}
			for (Mesh::SubmeshList::const_iterator submeshIt = mesh->getSubmeshList().begin(); submeshIt != mesh->getSubmeshList().end(); submeshIt++)
			{
				Submesh submesh = *submeshIt; // make copy
				const Material *material = meshInstance.getMaterial(submesh.getMaterialId());
				if (material == 0)
				{
					assert(false);
					continue;
				}
				std::string materialId = combinedMeshInstance.storeMaterial(*material);
				submesh.setMaterialId(materialId);
				submesh.applyTransformation(meshInstance.getTransform());
				combinedMesh.getSubmeshList().push_back(submesh);
			}
			meshInstanceIt = mMeshInstanceList.erase(meshInstanceIt);
			if (combinedMesh.getVertexCount() > maxMeshVertices)
			{
				std::string meshId = storeMesh(combinedMesh);
				combinedMeshInstance.setMeshId(meshId);
				mMeshInstanceList.push_back(combinedMeshInstance);
				combinedMesh = Mesh();
				combinedMeshInstance = MeshInstance();
			}
		}

		// Store combined mesh
		if (combinedMesh.getVertexCount() > 0)
		{
			std::string meshId = storeMesh(combinedMesh);
			combinedMeshInstance.setMeshId(meshId);
			mMeshInstanceList.push_back(combinedMeshInstance);
		}
	}

	//--------------------------------------------------------------------
	void Model::optimizeForBoundsSize(const Matrix4 &transform, const Vector3 &boundsSize, float sizeThreshold, int screenSize, int maxTextureSize)
	{
		static const double EPSILON = 1.0e-6;

		// Calculate pixel/texel ratios for each texture and bounds ratios for submeshes
		std::map<std::string, float> maxPixelTexelRatios;
		std::map<std::pair<std::string, int>, float> maxBoundsRatios;
		for (MeshInstanceList::const_iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			const MeshInstance &meshInstance = *meshInstanceIt;
			const Mesh *mesh = getMesh(meshInstance.getMeshId());
			if (mesh == 0)
			{
				assert(false);
				continue;
			}

			for (Mesh::SubmeshList::const_iterator submeshIt = mesh->getSubmeshList().begin(); submeshIt != mesh->getSubmeshList().end(); submeshIt++)
			{
				const Submesh &submesh = *submeshIt;

				// Calculate submesh size ratio from its projection on each principal plane
				Bounds3 submeshBounds = submesh.calculateBounds(transform * meshInstance.getTransform());
				double maxBoundsRatio = 0;
				for (int i = 0; i < 2; i++)
				{
					for (int j = i + 1; j < 3; j++)
					{
						double ratio = std::max(submeshBounds.size()[i], submeshBounds.size()[j]) / (std::max(boundsSize[i], boundsSize[j]) + EPSILON);
						maxBoundsRatio = std::max(maxBoundsRatio, ratio);
					}
				}
				int submeshIdx = (int) std::distance(mesh->getSubmeshList().begin(), submeshIt);
				maxBoundsRatios[std::make_pair(meshInstance.getMeshId(), submeshIdx)] = std::max(maxBoundsRatios[std::make_pair(meshInstance.getMeshId(), submeshIdx)], (float) maxBoundsRatio);

				// Calculate pixel-texel ratio for all textures used by this submesh
				const Material *material = meshInstance.getMaterial(submesh.getMaterialId());
				if (material == 0)
				{
					assert(false);
					continue;
				}
				for (Material::ChannelMap::const_iterator channelIt = material->getChannelMap().begin(); channelIt != material->getChannelMap().end(); channelIt++)
				{
					if (channelIt->second.type() == Material::ColorOrTexture::TEXTURE)
					{
						const Texture *texture = getTexture(channelIt->second.texture_id());
						if (texture == 0)
						{
							assert(false);
							continue;
						}
						double pixelSize = (double) screenSize / (std::max(boundsSize.x, std::max(boundsSize.y, boundsSize.z)) + EPSILON);
						Vector3 textureSize = Vector3(texture->getImage().getCImage().width(), texture->getImage().getCImage().height(), 0);
						float pixelTexelRatio = submesh.calculatePixelTexelRatio(transform * meshInstance.getTransform(), pixelSize, textureSize);
						maxPixelTexelRatios[channelIt->second.texture_id()] = std::max(maxPixelTexelRatios[channelIt->second.texture_id()], pixelTexelRatio);
					}
				}
			}
		}

		// Drop too small submeshes
		for (std::map<std::pair<std::string, int>, float>::iterator ratioIt = maxBoundsRatios.begin(); ratioIt != maxBoundsRatios.end(); ratioIt++)
		{
			if (ratioIt->second < sizeThreshold)
			{
				Mesh *mesh = getMesh(ratioIt->first.first);
				Mesh::SubmeshList::iterator submeshIt = mesh->getSubmeshList().begin();
				std::advance(submeshIt, ratioIt->first.second);
				mesh->getSubmeshList().erase(submeshIt);
			}
		}

		// Resize textures according to calculated pixel-texel ratios
		for (TextureMap::iterator textureIt = mTextureMap.begin(); textureIt != mTextureMap.end(); textureIt++)
		{
			float scale = 1;
			if (maxPixelTexelRatios.count(textureIt->first) > 0)
				scale = std::min(scale, maxPixelTexelRatios[textureIt->first]);
			textureIt->second.resizeToPOT(scale, maxTextureSize);
		}
	}

	//--------------------------------------------------------------------
	void Model::canonizeNames()
	{
		typedef std::map<std::string, std::string> IdMap;

		// Create material id maps for all meshes and remap submesh material ids
		std::map<std::string, IdMap> meshMaterialIdMap;
		for (MeshMap::iterator meshIt = mMeshMap.begin(); meshIt != mMeshMap.end(); meshIt++)
		{
			Mesh *mesh = &meshIt->second;
			IdMap &materialIdMap = meshMaterialIdMap[meshIt->first];
			for (Mesh::SubmeshList::iterator submeshIt = mesh->getSubmeshList().begin(); submeshIt != mesh->getSubmeshList().end(); submeshIt++)
			{
				std::string materialId = materialIdMap[submeshIt->getMaterialId()];
				if (materialId.empty())
				{
					std::stringstream stream;
					stream << "M" << materialIdMap.size();
					materialId = materialIdMap[submeshIt->getMaterialId()] = stream.str();
				}
				submeshIt->setMaterialId(materialId);
			}
		}

		// Create mesh name map based on hash of mesh contents
		MeshMap newMeshMap;
		IdMap meshIdMap;
		for (MeshMap::iterator meshIt = mMeshMap.begin(); meshIt != mMeshMap.end(); meshIt++)
		{
			size_t hash = boost::hash<Mesh>()(meshIt->second);
			std::string base_id = hexString(hash);
			std::string id = base_id;
			for (int n = 1; newMeshMap.count(id) > 0; n++)
			{
				if (newMeshMap[id] == meshIt->second)
					break; // can reuse existing mesh
				std::stringstream stream;
				stream << base_id << "#" << n;
				id = stream.str();
			}
			meshIdMap[meshIt->first] = id;
			newMeshMap[id] = meshIt->second;
		}

		// Create texture name map based on hash of texture contents
		TextureMap newTextureMap;
		IdMap textureIdMap;
		for (TextureMap::iterator textureIt = mTextureMap.begin(); textureIt != mTextureMap.end(); textureIt++)
		{
			size_t hash = boost::hash<Texture>()(textureIt->second);
			std::string base_id = hexString(hash);
			std::string id = base_id;
			for (int n = 1; newTextureMap.count(id) > 0; n++)
			{
				if (newTextureMap[id] == textureIt->second)
					break; // can reuse existing texture
				std::stringstream stream;
				stream << base_id << "#" << n;
				id = stream.str();
			}
			textureIdMap[textureIt->first] = id;
			newTextureMap[id] = textureIt->second;
		}

		// Remap all mesh ids and texture ids in mesh instances
		for (MeshInstanceList::iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); meshInstanceIt++)
		{
			MeshInstance &meshInstance = *meshInstanceIt;
			IdMap materialIdMap = meshMaterialIdMap[meshInstance.getMeshId()];
			std::string newMeshId = meshIdMap[meshInstance.getMeshId()];
			assert(!newMeshId.empty());
			meshInstance.setMeshId(newMeshId);

			MeshInstance::MaterialMap newMaterialMap;
			for (MeshInstance::MaterialMap::iterator materialIt = meshInstance.getMaterialMap().begin(); materialIt != meshInstance.getMaterialMap().end(); materialIt++)
			{
				std::string newMaterialId = materialIdMap[materialIt->first];
				if (newMaterialId.empty())
					continue; // unused material, skip this one
				newMaterialMap[newMaterialId] = materialIt->second;
				Material &material = newMaterialMap[newMaterialId];
				for (Material::ChannelMap::iterator channelIt = material.getChannelMap().begin(); channelIt != material.getChannelMap().end(); channelIt++)
				{
					Material::ColorOrTexture &colorOrTex = channelIt->second;
					if (colorOrTex.type() == Material::ColorOrTexture::TEXTURE)
					{
						std::string newTextureId = textureIdMap[colorOrTex.texture_id()];
						assert(!newTextureId.empty());
						colorOrTex.set_texture_id(newTextureId);
					}
				}
			}
			std::swap(meshInstance.getMaterialMap(), newMaterialMap);
		}

		// Update mesh and texture maps
		std::swap(mMeshMap, newMeshMap);
		std::swap(mTextureMap, newTextureMap);
	}

	//--------------------------------------------------------------------
	void Model::cleanup()
	{
		// Record all used mesh instances and textures
		std::set<std::string> referencedMeshIds;
		std::set<std::string> referencedTextureIds;
		for (MeshInstanceList::iterator meshInstanceIt = mMeshInstanceList.begin(); meshInstanceIt != mMeshInstanceList.end(); )
		{
			MeshInstance &meshInstance = *meshInstanceIt;
			const Mesh *mesh = getMesh(meshInstance.getMeshId());
			if (mesh == 0)
			{
				assert(false);
				meshInstanceIt++;
				continue;
			}

			// Check if mesh is not null
			if (mesh->getSubmeshList().empty())
			{
				meshInstanceIt = mMeshInstanceList.erase(meshInstanceIt);
				continue;
			}

			// Move to next mesh instance
			meshInstanceIt++;

			// Mark used mesh
			referencedMeshIds.insert(meshInstance.getMeshId());

			// Remove unused materials from mesh instances
			std::set<std::string> referencedMaterialIds;
			for (Mesh::SubmeshList::const_iterator submeshIt = mesh->getSubmeshList().begin(); submeshIt != mesh->getSubmeshList().end(); submeshIt++)
			{
				referencedMaterialIds.insert(submeshIt->getMaterialId());
			}
			for (MeshInstance::MaterialMap::iterator materialIt = meshInstance.getMaterialMap().begin(); materialIt != meshInstance.getMaterialMap().end(); )
			{
				if (referencedMaterialIds.count(materialIt->first) > 0)
					materialIt++;
				else
					meshInstance.getMaterialMap().erase(materialIt++);
			}

			// Mark used textures
			for (MeshInstance::MaterialMap::const_iterator materialIt = meshInstance.getMaterialMap().begin(); materialIt != meshInstance.getMaterialMap().end(); materialIt++)
			{
				const Material & material = materialIt->second;
				for (Material::ChannelMap::const_iterator channelIt = material.getChannelMap().begin(); channelIt != material.getChannelMap().end(); channelIt++)
				{
					if (channelIt->second.type() == Material::ColorOrTexture::TEXTURE)
						referencedTextureIds.insert(channelIt->second.texture_id());
				}
			}
		}
		
		// Remove unused meshes
		for (MeshMap::iterator it = mMeshMap.begin(); it != mMeshMap.end(); )
		{
			if (referencedMeshIds.count(it->first) > 0)
				it++;
			else
				mMeshMap.erase(it++);
		}

		// Remove unused textures
		for (TextureMap::iterator it = mTextureMap.begin(); it != mTextureMap.end(); )
		{
			if (referencedTextureIds.count(it->first) > 0)
				it++;
			else
				mTextureMap.erase(it++);
		}
	}

	//--------------------------------------------------------------------
	bool Model::saveNMLModel(const std::string &id, NMLPackage::Model &nmlModel, const TextureExporter &textureExporter) const
	{
		// Basic attributes
		nmlModel.set_id(id);

		// Store mesh instances
		for (MeshInstanceList::const_iterator it = mMeshInstanceList.begin(); it != mMeshInstanceList.end(); it++)
		{
			it->saveNMLMeshInstance(*nmlModel.add_mesh_instances());
		}

		// Store geometry
		for (MeshMap::const_iterator it = mMeshMap.begin(); it != mMeshMap.end(); it++)
		{
			it->second.saveNMLMesh(it->first, *nmlModel.add_meshes());
		}

		// Store textures
		for (TextureMap::const_iterator it = mTextureMap.begin(); it != mTextureMap.end(); it++)
		{
			it->second.saveNMLTexture(it->first, *nmlModel.add_textures(), textureExporter);
		}

		// Calculate and set model bounds
		*nmlModel.mutable_bounds() = calculateBounds(Matrix4::IDENTITY).getNMLBounds();

		// Set footprint info
		nmlModel.set_mesh_footprint(mMeshFootprint);
		nmlModel.set_texture_footprint(mTextureFootprint);
		return true;
	}

	//--------------------------------------------------------------------
	bool Model::loadNMLModel(const NMLPackage::Model &nmlModel)
	{
		// Read mesh instances
		mMeshInstanceList.clear();
		for (int i = 0; i < nmlModel.mesh_instances_size(); i++)
		{
			MeshInstance meshInstance;
			meshInstance.loadNMLMeshInstance(nmlModel.mesh_instances(i));
			mMeshInstanceList.push_back(meshInstance);
		}

		// Read geometry
		mMeshMap.clear();
		for (int i = 0; i < nmlModel.meshes_size(); i++)
		{
			const NMLPackage::Mesh &nmlMesh = nmlModel.meshes(i);
			Mesh mesh;
			mesh.loadNMLMesh(nmlMesh);
			mMeshMap[nmlMesh.id()] = mesh;
		}

		// Read textures
		mTextureMap.clear();
		for (int i = 0; i < nmlModel.textures_size(); i++)
		{
			const NMLPackage::Texture &nmlTexture = nmlModel.textures(i);
			Texture texture;
			texture.loadNMLTexture(nmlTexture);
			mTextureMap[nmlTexture.id()] = texture;
		}

		return true;
	}

} // namespace NMLFramework
