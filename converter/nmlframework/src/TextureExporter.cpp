/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#define ETC1_SUPPORT
#ifdef _WIN32
#define PVRTC1_SUPPORT
#endif
#define DXTC_SUPPORT

#include "TextureExporter.h"

#ifdef ETC1_SUPPORT
#include "rg_etc1.h"
#endif

#ifdef PVRTC1_SUPPORT
#include "PVRTextureUtilities.h"
#endif

#ifdef DXTC_SUPPORT
#include "squish.h"
#endif

#include <omp.h>
#include <boost/filesystem.hpp>


namespace
{
	const static struct ETC1State
	{
		ETC1State() { rg_etc1::pack_etc1_block_init(); }
	} g_ETC1State;

	static void writePKMInt16(std::vector<unsigned char> &buf, int val)
	{
		buf.push_back((unsigned char) ((val >> 8) & 255));
		buf.push_back((unsigned char) ((val >> 0) & 255));
	}

	static void writePKMInt32(std::vector<unsigned char> &buf, int val)
	{
		buf.push_back((unsigned char) ((val >> 24) & 255));
		buf.push_back((unsigned char) ((val >> 16) & 255));
		buf.push_back((unsigned char) ((val >> 8 ) & 255));
		buf.push_back((unsigned char) ((val >> 0 ) & 255));
	}

	static void writePKMETC1Image(std::vector<unsigned char> &buf, const NMLFramework::Image::CImage &cimage, const NMLFramework::ColorRGBA &avgColor)
	{
#ifdef ETC1_SUPPORT
		static const unsigned char header[] = { 'P', 'K', 'M', ' ', '1', '0', 0, 0 };

		// Make image dimensions divisible by 4, ETC1 requires that even for 1x1 mip-map levels
		int etc1Width = (cimage.width() + 3) / 4 * 4, etc1Height = (cimage.height() + 3) / 4 * 4;
		std::vector<unsigned char> etc1Image(etc1Width * etc1Height * 4);

		// Fill expanded image with average color
		for (size_t i = 0; i + 4 <= etc1Image.size(); i += 4)
		{
			etc1Image[i + 0] = (unsigned char) (avgColor.r * 255);
			etc1Image[i + 1] = (unsigned char) (avgColor.g * 255);
			etc1Image[i + 2] = (unsigned char) (avgColor.b * 255);
			etc1Image[i + 3] = 255;
		}

		// Copy pixels to expanded image
		cimg_forXYC(cimage, x, y, c)
		{
			int idx = ((cimage.height() - y - 1) * etc1Width + x) * 4 + c;
			if (c != 3)
				etc1Image[idx] = cimage(x, y, 0, c);
		}

		// Write header
		for (size_t i = 0; i < sizeof(header); i++)
		{
			buf.push_back(header[i]);
		}
		writePKMInt16(buf, etc1Width);
		writePKMInt16(buf, etc1Height);
		writePKMInt16(buf, cimage.width());
		writePKMInt16(buf, cimage.height());

		// Allocate buffer
		size_t offset = buf.size();
		buf.resize(offset + etc1Width * etc1Height / 2);

		// Compress image with ETC1
		rg_etc1::etc1_pack_params pack_params;
		pack_params.m_quality = rg_etc1::cMediumQuality;
		#pragma omp parallel for
		for (int y = 0; y < etc1Height; y += 4)
		{
			for (int x = 0; x < etc1Width; x += 4)
			{
				unsigned int block[4 * 4];
				for (int yb = 0; yb < 4; yb++)
				{
					for (int xb = 0; xb < 4; xb++)
					{
						block[yb * 4 + xb] = *reinterpret_cast<const unsigned int *>(&etc1Image[((y + yb) * etc1Width + x + xb) * 4]);
					}
				}
				size_t pos = (y * etc1Width + x * 4) / 2;
				rg_etc1::pack_etc1_block(&buf[offset + pos], block, pack_params);
			}
		}
#else
		throw std::runtime_error("ETC1 support not available in this build");
#endif
	}

	static void writePVRTC1Image(std::vector<unsigned char> &buf, const NMLFramework::Image::CImage &cimage, bool rgba, bool use2bpp, const NMLFramework::ColorRGBA &avgColor)
	{
#ifdef PVRTC1_SUPPORT
		std::vector<unsigned char> image(cimage.width() * cimage.height() * 4, 255);
		cimg_forXYC(cimage, x, y, c)
		{
			int idx = ((cimage.height() - y - 1) * cimage.width() + x) * 4 + c;
			image[idx] = cimage(x, y, 0, c);
		}

		pvrtexture::CPVRTextureHeader *header = new pvrtexture::CPVRTextureHeader(pvrtexture::PVRStandard8PixelType.PixelTypeID, cimage.height(), cimage.width()); // leak memory, ugly hack - otherwise we will have heap corruption, probably due to different compiler versions in the lib
		pvrtexture::CPVRTexture texture(*header, &image[0]);

		pvrtexture::PixelType pixelType = ePVRTPF_PVRTCI_2bpp_RGBA;
		if (rgba)
		{
			pixelType = use2bpp ? ePVRTPF_PVRTCI_2bpp_RGBA : ePVRTPF_PVRTCI_4bpp_RGBA;
		}
		else
		{
			pixelType = use2bpp ? ePVRTPF_PVRTCI_2bpp_RGB : ePVRTPF_PVRTCI_4bpp_RGB;
		}
		pvrtexture::Transcode(texture, pixelType, ePVRTVarTypeUnsignedByteNorm, ePVRTCSpacelRGB);
		PVRTextureHeaderV3 pvrHeader = texture.getHeader().getFileHeader();

		const unsigned char *pvrHeaderPtr = reinterpret_cast<const unsigned char *>(&pvrHeader);
		buf.insert(buf.end(), pvrHeaderPtr, pvrHeaderPtr + sizeof(PVRTextureHeaderV3));
		const unsigned char *pvrDataPtr = static_cast<const unsigned char *>(texture.getDataPtr());
		buf.insert(buf.end(), pvrDataPtr, pvrDataPtr + texture.getDataSize());
#else
		throw std::runtime_error("PVRTC support not available in this build");
#endif
	}

	static void writeDXTCImage(std::vector<unsigned char> &buf, const NMLFramework::Image::CImage &cimage, int dxtcVersion, const NMLFramework::ColorRGBA &avgColor)
	{
#ifdef DXTC_SUPPORT
		std::vector<unsigned char> image(cimage.width() * cimage.height() * 4, 255);
		cimg_forXYC(cimage, x, y, c)
		{
			int idx = ((cimage.height() - y - 1) * cimage.width() + x) * 4 + c;
			image[idx] = cimage(x, y, 0, c);
		}

		std::vector<unsigned char> dxtcImage(((cimage.width() + 3) / 4) * ((cimage.height() + 3) / 4) * (dxtcVersion == 1 ? 8 : 16));
		squish::CompressImage(&image[0], cimage.width(), cimage.height(), &dxtcImage[0], dxtcVersion == 1 ? squish::kDxt1 : (dxtcVersion == 3 ? squish::kDxt3 : squish::kDxt5));

		// Write header
		writePKMInt32(buf, dxtcVersion);
		writePKMInt32(buf, cimage.width());
		writePKMInt32(buf, cimage.height());

		// Write converted data
		buf.insert(buf.end(), dxtcImage.begin(), dxtcImage.end());
#else
		throw std::runtime_error("DXTC support not available in this build");
#endif
	}

	static void readFile(std::vector<unsigned char> &buf, const std::string &fileName)
	{
		FILE *fp = fopen(fileName.c_str(), "rb");
		if (fp == NULL)
			throw NMLFramework::TextureExporter::ExportException("Could not open file " + fileName);
		try
		{
			while (!feof(fp))
			{
				unsigned char block[16384];
				size_t count = fread(block, 1, sizeof(block), fp);
				buf.insert(buf.end(), block, block + count);
			}
			fclose(fp);
		}
		catch (...)
		{
			fclose(fp);
			throw;
		}
	}

	static void writeFile(const std::vector<unsigned char> &buf, const std::string &fileName)
	{
		FILE *fp = fopen(fileName.c_str(), "wb");
		if (fp == NULL)
			throw NMLFramework::TextureExporter::ExportException("Could not create file " + fileName);
		try
		{
			if (!buf.empty())
			{
				if (fwrite(&buf[0], 1, buf.size(), fp) != buf.size())
					throw NMLFramework::TextureExporter::ExportException("Could not write file " + fileName);
			}
			fclose(fp);
		}
		catch (...)
		{
			fclose(fp);
			throw;
		}
	}

	static void readPNGImage(const std::vector<unsigned char> &buf, NMLFramework::Image::CImage &cimage)
	{
#ifdef cimg_inmemory_png
		cimage.load_png(buf);
#else
		std::string tempFileName;
		try
		{
			boost::filesystem::path tempFilePath = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path("KMZ-%%%%-%%%%-%%%%-%%%%.png");
			tempFileName = tempFilePath.string();
			writeFile(buf, tempFileName);
			cimage.load_png(tempFileName.c_str());
			boost::filesystem::remove(tempFileName);
		}
		catch (...)
		{
			if (!tempFileName.empty())
				boost::filesystem::remove(tempFileName);
			throw;
		}
#endif
	}

	static void writePNGImage(std::vector<unsigned char> &buf, const NMLFramework::Image::CImage &cimage, int compression)
	{
#ifdef cimg_inmemory_png
		cimage.save_png(buf, compression);
#else
		std::string tempFileName;
		try
		{
			boost::filesystem::path tempFilePath = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path("KMZ-%%%%-%%%%-%%%%-%%%%.png");
			tempFileName = tempFilePath.string();
			cimage.save_png(tempFileName.c_str());
			readFile(buf, tempFileName);
			boost::filesystem::remove(tempFileName);
		}
		catch (...)
		{
			if (!tempFileName.empty())
				boost::filesystem::remove(tempFileName);
			throw;
		}
#endif
	}

	static void readJPEGImage(const std::vector<unsigned char> &buf, NMLFramework::Image::CImage &cimage)
	{
		std::string tempFileName;
		try
		{
			boost::filesystem::path tempFilePath = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path("KMZ-%%%%-%%%%-%%%%-%%%%.jpg");
			tempFileName = tempFilePath.string();
			writeFile(buf, tempFileName);
			cimage.load_jpeg(tempFileName.c_str());
			boost::filesystem::remove(tempFileName);
		}
		catch (...)
		{
			if (!tempFileName.empty())
				boost::filesystem::remove(tempFileName);
			throw;
		}
	}

	static void writeJPEGImage(std::vector<unsigned char> &buf, const NMLFramework::Image::CImage &cimage, int quality)
	{
		std::string tempFileName;
		try
		{
			boost::filesystem::path tempFilePath = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path("KMZ-%%%%-%%%%-%%%%-%%%%.jpg");
			tempFileName = tempFilePath.string();
			cimage.save_jpeg(tempFileName.c_str(), quality);
			readFile(buf, tempFileName);
			boost::filesystem::remove(tempFileName);
		}
		catch (...)
		{
			if (!tempFileName.empty())
				boost::filesystem::remove(tempFileName);
			throw;
		}
	}
}

namespace NMLFramework
{

	//--------------------------------------------------------------------
	TextureExporter::Options TextureExporter::sDefaultOptions;

	//--------------------------------------------------------------------
	int TextureExporter::estimateMemoryFootprint(const Texture &texture) const
	{
		const Image::CImage &cimage = texture.getImage().getCImage();
		size_t size = cimage.width() * cimage.height();
		Options::Format format = (cimage.spectrum() == 3 ? mOptions.RGBFormat : mOptions.RGBAFormat);
		switch (cimage.spectrum())
		{
		case 3:
		case 4:
			if (format == Options::ETC1)
			{
				size /= 2;
			}
			else if (format == Options::PVRTC1_2BPP || format == Options::PVRTC1_4BPP)
			{
				size /= (format == Options::PVRTC1_2BPP ? 4 : 2);
			}
			else if (format == Options::DXTC1 || format == Options::DXTC3 || format == Options::DXTC5)
			{
				size /= (format == Options::DXTC1 ? 2 : 1);
			}
			else
			{
				size *= cimage.spectrum();
			}
			break;
		default:
			size *= cimage.spectrum();
			break;
		}
		if (mOptions.generateMipMaps)
		{
			size = size * 4 / 3;
		}
		return (int) size;
	}

	//--------------------------------------------------------------------
	bool TextureExporter::exportNMLTexture(const Texture &texture, const std::string &id, NMLPackage::Texture &nmlTexture) const
	{
		Image::CImage cimage = texture.getImage().getCImage();

		// Set image attributes
		nmlTexture.set_id(id);
		nmlTexture.set_width(cimage.width());
		nmlTexture.set_height(cimage.height());
		*nmlTexture.mutable_sampler() = texture.getSampler();

		// Process image data
		while (true)
		{
			std::vector<unsigned char> data(cimage.size());
			cimg_forXYC(cimage, x, y, c)
			{
				int idx = (y * cimage.width() + x) * cimage.spectrum() + c;
				data[idx] = cimage(x, cimage.height() - y - 1, 0, c);
			}
			Options::Format format = (cimage.spectrum() == 3 ? mOptions.RGBFormat : mOptions.RGBAFormat);
			switch (cimage.spectrum())
			{
			case 1:
				nmlTexture.set_format(NMLPackage::Texture::LUMINANCE8);
				break;
			case 3:
			case 4:
				if (format == Options::ETC1 && !cimage.empty())
				{
					nmlTexture.set_format(NMLPackage::Texture::ETC1);
					data.clear();
					writePKMETC1Image(data, cimage, texture.calculateAvgColor());
				}
				else if ((format == Options::PVRTC1_2BPP || format == Options::PVRTC1_4BPP) && !cimage.empty())
				{
					nmlTexture.set_format(NMLPackage::Texture::PVRTC);
					data.clear();
					writePVRTC1Image(data, cimage, cimage.spectrum() == 4, format == Options::PVRTC1_2BPP, texture.calculateAvgColor());
				}
				else if ((format == Options::DXTC1 || format == Options::DXTC3 || format == Options::DXTC5) && !cimage.empty())
				{
					nmlTexture.set_format(NMLPackage::Texture::DXTC);
					data.clear();
					writeDXTCImage(data, cimage, (format == Options::DXTC1 ? 1 : (format == Options::DXTC3 ? 3 : 5)), texture.calculateAvgColor());
				}
				else if (format == Options::PNG && !cimage.empty())
				{
					nmlTexture.set_format(NMLPackage::Texture::PNG);
					data.clear();
					writePNGImage(data, cimage, DEFAULT_PNG_COMPRESSION);
				}
				else if (format == Options::JPEG && !cimage.empty())
				{
					nmlTexture.set_format(NMLPackage::Texture::JPEG);
					data.clear();
					writeJPEGImage(data, cimage, DEFAULT_JPEG_QUALITY);
				}
				else
				{
					nmlTexture.set_format(cimage.spectrum() == 3 ? NMLPackage::Texture::RGB8 : NMLPackage::Texture::RGBA8);
				}
				break;
			default:
				nmlTexture.set_width(0);
				nmlTexture.set_height(0);
				nmlTexture.set_format(NMLPackage::Texture::RGB8); // this case can happen when the texture is empty, so be graceful here
				data.clear();
				break;
			}
			if (!data.empty())
				nmlTexture.add_mipmaps(std::string(reinterpret_cast<const char *>(&data[0]), data.size()));
			else
				nmlTexture.add_mipmaps(std::string());

			// Done?
			if (!mOptions.generateMipMaps)
				break;
			if (cimage.width() < 2 && cimage.height() < 2)
				break;

			// Resize image
			cimage.resize(std::max(cimage.width() / 2, 1), std::max(cimage.height() / 2, 1), cimage.depth(), cimage.spectrum(), 2); // use 'moving average' filter. Better quality would be possible if using linear space, instead of sRGB
		}
		return true;
	}

	//--------------------------------------------------------------------
	bool TextureExporter::importNMLTexture(const NMLPackage::Texture &nmlTexture, Texture &texture)
	{
		texture.setSampler(nmlTexture.sampler());
		const unsigned char *data = reinterpret_cast<const unsigned char *>(nmlTexture.mipmaps(0).data());
		size_t dataSize = nmlTexture.mipmaps(0).size();
		Image::CImage &cimage = texture.getImage().getCImage();
		if (nmlTexture.format() == NMLPackage::Texture::PNG)
		{
			readPNGImage(std::vector<unsigned char>(data, data + dataSize), cimage);
			return true;
		}
		if (nmlTexture.format() == NMLPackage::Texture::JPEG)
		{
			readJPEGImage(std::vector<unsigned char>(data, data + dataSize), cimage);
			return true;
		}

		int components = 1;
		switch (nmlTexture.format())
		{
		case NMLPackage::Texture::LUMINANCE8:
			components = 1;
			break;
		case NMLPackage::Texture::RGB8:
			components = 3;
			break;
		case NMLPackage::Texture::RGBA8:
			components = 4;
			break;
		default:
			throw std::runtime_error("TextureExporter::importNMLTexture: unimplemented format (ETC1?)");
		}
		cimage.assign(nmlTexture.width(), nmlTexture.height(), 1, components);
		cimg_forXYC(cimage, x, y, c)
		{
			int idx = (y * cimage.width() + x) * cimage.spectrum() + c;
			cimage(x, cimage.height() - y - 1, 0, c) = data[idx];
		}
		return true;
	}

	//--------------------------------------------------------------------
	std::istream &operator >> (std::istream &is, TextureExporter::Options::Format &format)
	{
	    std::string input;
		is >> input;
 
		if (input == "raw")
		{
			format = TextureExporter::Options::RAW;
		}
		else if (input == "etc1")
		{
			format = TextureExporter::Options::ETC1;
		}
		else if (input == "pvrtc1_2bpp")
		{
			format = TextureExporter::Options::PVRTC1_2BPP;
		}
		else if (input == "pvrtc1_4bpp")
		{
			format = TextureExporter::Options::PVRTC1_4BPP;
		}
		else if (input == "dxtc1")
		{
			format = TextureExporter::Options::DXTC1;
		}
		else if (input == "dxtc3")
		{
			format = TextureExporter::Options::DXTC3;
		}
		else if (input == "dxtc5")
		{
			format = TextureExporter::Options::DXTC5;
		}
		else
		{
			throw boost::program_options::invalid_option_value("Illegal texture format");
		}
		return is;
 	}

	//--------------------------------------------------------------------
	std::ostream &operator << (std::ostream &os, TextureExporter::Options::Format format)
	{
		std::string output;
		switch (format)
		{
		case TextureExporter::Options::RAW:
			output = "raw";
			break;
		case TextureExporter::Options::ETC1:
			output = "etc1";
			break;
		case TextureExporter::Options::PVRTC1_2BPP:
			output = "pvrtc1_2bpp";
			break;
		case TextureExporter::Options::PVRTC1_4BPP:
			output = "pvrtc1_4bpp";
			break;
		case TextureExporter::Options::DXTC1:
			output = "dxtc1";
			break;
		case TextureExporter::Options::DXTC3:
			output = "dxtc3";
			break;
		case TextureExporter::Options::DXTC5:
			output = "dxtc5";
			break;
		}
		os << output;
		return os;
	}

} // namespace NMLFramework
