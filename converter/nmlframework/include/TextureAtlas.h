/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_TEXTUREATLAS_H__
#define __NMLFRAMEWORK_TEXTUREATLAS_H__

#include "Prerequisites.h"

#include "Texture.h"
#include "Submesh.h"

#include <list>
#include <map>


namespace NMLFramework
{

	class TextureAtlas
	{

	public:

		struct TextureTransformation
		{
			int textureNumber;
			float scale[2];
			float offset[2];

			TextureTransformation() { textureNumber = -1; scale[0] = scale[1] = 1; offset[0] = offset[1] = 0; }
			bool applyToSubmesh(Submesh &submesh) const;
		};

		typedef int TextureId;

		/** Get texture count in atlas */
		int getTextureCount() const { return (int) mTextures.size(); }

		/** Get specific atlas texture */
		Texture getTexture(int num) const;

		/** Get transformation for specified texture id */
		TextureTransformation getTransformation(TextureId id) const;

	private:

		TextureAtlas() { }

		std::list<Texture> mTextures;
		std::map<TextureId, TextureTransformation> mTextureTransformations;

		friend class TextureAtlasGenerator;
	};

	class TextureAtlasGenerator
	{

	public:

		/** Default constructor */
		TextureAtlasGenerator(int maxTextureSize) : mMaxTextureSize(maxTextureSize) { }

		/** Reset atlas */
		void reset();

		/** Add texture to atlas, returned id can be later used to retrieve texture information from generated atlas */
		TextureAtlas::TextureId addTexture(const Submesh &submesh, const Texture &tex, int maxRepeatedPixels);

		/** Finalize texture atlas and return generated atlas */
		TextureAtlas generate(int maxAtlasTextureSize, bool squareAtlas) const;

	protected:

		enum { PENALTY_TILE_SIZE = 32 }; // starting with this tile size, prefer tight packing more than big atlas

		struct AtlasTile
		{
			int xSize; // texture width in atlas (total, including repeat)
			int ySize; // texture height in atlas (total, including repeat)
			int xRepeat; // how many times texture should be repeating in x-direction
			int yRepeat ;// how many times texture should be repeating in y-direction
			int xOffset; // texture x-offset in pixels
			int yOffset; // texture y-offset in pixels
			int maxGridWidth; // maximum atlas width this tile can be added to
			int maxGridHeight; // maximum atlas height this tile can be added to
			Image image;
			Texture::Sampler sampler;

			AtlasTile() : xSize(0), ySize(0), xRepeat(1), yRepeat(1), xOffset(0), yOffset(0), maxGridWidth(16384), maxGridHeight(16384), image(), sampler() { }
		};

		struct AtlasGrid
		{
			int width;
			int height;
			int tileSize;
			std::vector<TextureAtlas::TextureId> data;

			AtlasGrid(int w, int h, int ts) : width(w), height(h), tileSize(ts), data(w / ts * h / ts, 0) { }
			TextureAtlas::TextureId &at(int x, int y) { return data[y / tileSize * (width / tileSize) + x / tileSize]; }
			const TextureAtlas::TextureId &at(int x, int y) const { return data[y / tileSize * (width / tileSize) + x / tileSize]; }
		};

		typedef std::map<TextureAtlas::TextureId, AtlasTile> AtlasTileMap;

		const int mMaxTextureSize;
		AtlasTileMap mAtlasTileMap;

		static void calculateSubmeshUVBounds(const Submesh &submesh, float &min_u, float &min_v, float &max_u, float &max_v);
		static double evaluateGridQuality(const AtlasGrid &grid);

		bool addToAtlas(TextureAtlas &atlas, AtlasTileMap &atlasTileMap, int maxAtlasTextureSize, bool squareAtlas) const;
		bool addToGrid(AtlasGrid &grid, TextureAtlas::TextureId id, const AtlasTile &atlasTile) const;
	};

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_TEXTUREATLAS_H__
