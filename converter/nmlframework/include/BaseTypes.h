/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_BASETYPES_H__
#define __NMLFRAMEWORK_BASETYPES_H__

#include "Prerequisites.h"

#pragma warning(push)
#pragma warning(disable: 4244)
#include "NMLPackage.pb.h"
#pragma warning(pop)

#include "COLLADAFWColor.h"
#include "Math/COLLADABUMathVector3.h"
#include "Math/COLLADABUMathMatrix3.h"
#include "Math/COLLADABUMathMatrix4.h"

#include <limits>
#include <boost/lexical_cast.hpp>


namespace NMLFramework
{

	class ColorRGBA
	{

	public:

		float r, g, b, a;

		ColorRGBA() { }
		ColorRGBA(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) { }
		ColorRGBA(const COLLADAFW::Color &c) : r((float) c.getRed()), g((float) c.getGreen()), b((float) c.getBlue()), a((float) c.getAlpha()) { }

		bool isTransparent() const
		{
			return r == 0 && g == 0 && b == 0 && a == 0;
		}

		NMLPackage::ColorRGBA getNMLColor() const
		{
			NMLPackage::ColorRGBA c;
			c.set_r(r); c.set_g(g); c.set_b(b); c.set_a(a);
			return c;
		}

		static ColorRGBA fromNMLColor(const NMLPackage::ColorRGBA &nmlColor)
		{
			ColorRGBA c(nmlColor.r(), nmlColor.g(), nmlColor.b(), nmlColor.a());
			return c;
		}
	};

	inline std::istream &operator >> (std::istream &is, ColorRGBA &c)
	{
		std::string r, g, b, a;
		std::getline(is, r, ','); std::getline(is, g, ','); std::getline(is, b, ','); std::getline(is, a, ',');
		c.r = boost::lexical_cast<float>(r); c.g = boost::lexical_cast<float>(g); c.b = boost::lexical_cast<float>(b); c.a = boost::lexical_cast<float>(a); 
		return is;
	}
	inline std::ostream &operator << (std::ostream &os, const ColorRGBA &c)
	{
		os << c.r << "," << c.g << "," << c.b << "," << c.a;
		return os;
	}

	class Vector3 : public COLLADABU::Math::Vector3
	{

	public:

		Vector3() { }
		Vector3(double x, double y, double z) : COLLADABU::Math::Vector3(x, y, z) { }
		Vector3(const COLLADABU::Math::Vector3 &v) : COLLADABU::Math::Vector3(v) { }

		NMLPackage::Vector3 getNMLVector() const
		{
			NMLPackage::Vector3 v;
			v.set_x((float) x); v.set_y((float) y); v.set_z((float) z);
			return v;
		}

		static Vector3 fromNMLVector(const NMLPackage::Vector3 &nmlVec)
		{
			Vector3 v(nmlVec.x(), nmlVec.y(), nmlVec.z());
			return v;
		}
	};

	inline std::istream &operator >> (std::istream &is, Vector3 &v)
	{
		std::string x, y, z;
		std::getline(is, x, ','); std::getline(is, y, ','); std::getline(is, z, ',');
		v.x = boost::lexical_cast<float>(x); v.y = boost::lexical_cast<float>(y); v.z = boost::lexical_cast<float>(z);
		return is;
	}
	inline std::ostream &operator << (std::ostream &os, const Vector3 &v)
	{
		os << v.x << "," << v.y << "," << v.z;
		return os;
	}

	class Bounds3
	{
	public:

		Vector3 min;
		Vector3 max;

		Bounds3() { clear(); }
		Bounds3(const COLLADABU::Math::Vector3 &min, const COLLADABU::Math::Vector3 &max) : min(min), max(max) { }

		void clear()
		{
			static const double MAX_DOUBLE = std::numeric_limits<double>::max();
			min = Vector3( MAX_DOUBLE,  MAX_DOUBLE,  MAX_DOUBLE);
			max = Vector3(-MAX_DOUBLE, -MAX_DOUBLE, -MAX_DOUBLE);
		}

		void add(const Vector3 &point)
		{
			min.x = std::min(min.x, point.x);
			min.y = std::min(min.y, point.y);
			min.z = std::min(min.z, point.z);
			max.x = std::max(max.x, point.x);
			max.y = std::max(max.y, point.y);
			max.z = std::max(max.z, point.z);
		}

		void add(const Bounds3 &bounds)
		{
			min.x = std::min(min.x, bounds.min.x);
			min.y = std::min(min.y, bounds.min.y);
			min.z = std::min(min.z, bounds.min.z);
			max.x = std::max(max.x, bounds.max.x);
			max.y = std::max(max.y, bounds.max.y);
			max.z = std::max(max.z, bounds.max.z);
		}

		double volume() const
		{
			if (empty())
				return 0;
			Vector3 s = size();
			return s.x * s.y * s.z;
		}

		Vector3 center() const
		{
			return Vector3((max.x + min.x) * 0.5f, (max.y + min.y) * 0.5f, (max.z + min.z) * 0.5f);
		}

		Vector3 size() const
		{
			return Vector3(max.x - min.x, max.y - min.y, max.z - min.z);
		}

		bool empty() const
		{
			return min.x >= max.x || min.y >= max.y || min.z >= max.z;
		}

		NMLPackage::Bounds3 getNMLBounds() const
		{
			NMLPackage::Bounds3 b;
			*b.mutable_min() = min.getNMLVector();
			*b.mutable_max() = max.getNMLVector();
			return b;
		}

		static Bounds3 fromNMLBounds(const NMLPackage::Bounds3 &nmlBounds)
		{
			Bounds3 b(Vector3::fromNMLVector(nmlBounds.min()), Vector3::fromNMLVector(nmlBounds.max()));
			return b;
		}

		static Bounds3 fromPoints(const Vector3 &p0, const Vector3 &p1)
		{
			Bounds3 b;
			b.add(p0);
			b.add(p1);
			return b;
		}
	};

	class Matrix4 : public COLLADABU::Math::Matrix4
	{

	public:

		Matrix4() : COLLADABU::Math::Matrix4(COLLADABU::Math::Matrix4::IDENTITY) { }
		Matrix4(const COLLADABU::Math::Matrix4 &m) : COLLADABU::Math::Matrix4(m) { }

		NMLPackage::Matrix4 getNMLMatrix() const
		{
			const Matrix4 &src = *this;
			NMLPackage::Matrix4 target;
			target.set_m00((float) src[0][0]);
			target.set_m01((float) src[0][1]);
			target.set_m02((float) src[0][2]);
			target.set_m03((float) src[0][3]);

			target.set_m10((float) src[1][0]);
			target.set_m11((float) src[1][1]);
			target.set_m12((float) src[1][2]);
			target.set_m13((float) src[1][3]);

			target.set_m20((float) src[2][0]);
			target.set_m21((float) src[2][1]);
			target.set_m22((float) src[2][2]);
			target.set_m23((float) src[2][3]);

			target.set_m30((float) src[3][0]);
			target.set_m31((float) src[3][1]);
			target.set_m32((float) src[3][2]);
			target.set_m33((float) src[3][3]);
			return target;
		}

		static Matrix4 fromNMLMatrix(const NMLPackage::Matrix4 &nmlMatrix)
		{
			Matrix4 m;
			m.setElement(0, 0, nmlMatrix.m00());
			m.setElement(0, 1, nmlMatrix.m01());
			m.setElement(0, 2, nmlMatrix.m02());
			m.setElement(0, 3, nmlMatrix.m03());

			m.setElement(1, 0, nmlMatrix.m10());
			m.setElement(1, 1, nmlMatrix.m11());
			m.setElement(1, 2, nmlMatrix.m12());
			m.setElement(1, 3, nmlMatrix.m13());

			m.setElement(2, 0, nmlMatrix.m20());
			m.setElement(2, 1, nmlMatrix.m21());
			m.setElement(2, 2, nmlMatrix.m22());
			m.setElement(2, 3, nmlMatrix.m23());

			m.setElement(3, 0, nmlMatrix.m30());
			m.setElement(3, 1, nmlMatrix.m31());
			m.setElement(3, 2, nmlMatrix.m32());
			m.setElement(3, 3, nmlMatrix.m33());
			return m;
		}
	};

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_BASETYPES_H__
