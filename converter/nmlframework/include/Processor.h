/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_PROCESSOR_H__
#define __NMLFRAMEWORK_PROCESSOR_H__

#include "Prerequisites.h"


namespace NMLFramework
{

	class Processor
	{

	public:

		virtual ~Processor() { }

	protected:

		enum Severity
		{
			SEVERITY_INFORMATION,
			SEVERITY_WARNING,
			_SEVERITY_ERROR
		};

		virtual void reportError(const std::string &method, const std::string &message, Severity severity) const;
		virtual void reportProgress(const std::string& method, const std::string& message) const;
	};

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_PROCESSOR_H__
