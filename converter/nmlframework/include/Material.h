/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_MATERIAL_H__
#define __NMLFRAMEWORK_MATERIAL_H__

#include "Prerequisites.h"
#include "BaseTypes.h"


namespace NMLFramework
{

	class Material
	{

	public:

		enum Channel { AMBIENT, EMISSION, DIFFUSE };

		typedef NMLPackage::Material::Type Type;
		typedef NMLPackage::Material::Culling Culling;
		typedef NMLPackage::ColorOrTexture ColorOrTexture;
		typedef std::map<Channel, ColorOrTexture> ChannelMap;

		/** Default constructor */
		Material() : mType(NMLPackage::Material::LAMBERT), mCulling(NMLPackage::Material::BACK), mTranslucent(false) {}

		/** Access to texture attributes */
		Type getType() const { return mType; }
		void setType(Type type) { mType = type; }
		Culling getCulling() const { return mCulling; }
		void setCulling(Culling culling) { mCulling = culling; }
		bool isTranslucent() const { return mTranslucent; }
		void setTranslucent(bool translucent) { mTranslucent = translucent; }
		int getTextureCount() const;
		ColorOrTexture *getColorOrTexture(Channel channel);
		const ColorOrTexture *getColorOrTexture(Channel channel) const;
		void setColorOrTexture(Channel channel, const ColorOrTexture &colorOrTex);
		ChannelMap &getChannelMap() { return mChannelMap; }
		const ChannelMap &getChannelMap() const { return mChannelMap; }

		/** Export/import material as NMLMaterial */
		bool saveNMLMaterial(const std::string &id, NMLPackage::Material &nmlMaterial) const;
		bool loadNMLMaterial(const NMLPackage::Material &nmlMaterial);

		/** Operators */
		size_t hash() const;
		bool operator == (const Material &material) const;
		bool operator != (const Material &material) const { return !(*this == material); }

	private:

		Type mType;
		Culling mCulling;
		bool mTranslucent;
		ChannelMap mChannelMap;
	};

	inline size_t hash_value(const Material &material)
	{
		return material.hash();
	}

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_MATERIAL_H__
