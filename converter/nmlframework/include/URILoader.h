/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_URILOADER_H__
#define __NMLFRAMEWORK_URILOADER_H__

#include "Prerequisites.h"

#include "COLLADABUURI.h"

namespace NMLFramework
{

	class URILoader
	{

	public:

		class URIException : public std::runtime_error
		{
		public:
			URIException(const std::string &msg) : std::runtime_error(msg) { }
		};

		explicit URILoader(const std::string &rootDir);
		virtual ~URILoader() {}

		/** Return local file name for given input file */
		virtual std::string resolve(const COLLADABU::URI& inputUri);

		/** Strip beginning and trailing spaces from URI string */
		static std::string stripUri(const std::string &uri);

	protected:

		std::string mRootDir;

	private:

		// Disable copy constructor/assignment
		URILoader(const URILoader& pre);
		const URILoader& operator= (const URILoader& pre);
	};

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_URILOADER_H__
