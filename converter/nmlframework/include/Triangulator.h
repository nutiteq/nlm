/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_TRIANGULATOR_H__
#define __NMLFRAMEWORK_TRIANGULATOR_H__

#include "BaseTypes.h"

#include <vector>

namespace NMLFramework
{

	class Triangulator
	{

	public:

		/** Construct triangulator given vertexinfo in XYZXYZ format. */
		Triangulator(const float *points);

		/** Try to triangulate polygon, given input indices. If successful, store triangle vertex info as T1aT1bT1cT2aT2bT2c. */
		bool triangulate(const std::vector<int> &inIndices, std::vector<int> &outIndices) const;

	protected:

		const float *mPoints;

		Vector3 getPoint(int idx) const { return Vector3(mPoints[idx*3 + 0], mPoints[idx*3 + 1], mPoints[idx*3 + 2]); }
		Vector3 calculateNormal(const std::vector<int> &V) const;
		bool insideTriangle(const Vector3& A, const Vector3& B, const Vector3& C,	const Vector3& P) const;
		bool sameSide(const Vector3& p0, const Vector3& p1, const Vector3& A, const Vector3& B) const;
		bool snip(int u, int v, int w, const std::vector<int> &V, const Vector3 &N) const;
		bool process(const std::vector<int> &inIndices, std::vector<int> &outIndices) const;
	};

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_TRIANGULATOR_H__
