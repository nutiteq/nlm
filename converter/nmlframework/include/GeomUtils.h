/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framrwork.
*/

#ifndef __NMLFRAMEWORK_GEOMUTILS_H__
#define __NMLFRAMEWORK_GEOMUTILS_H__

#include "Prerequisites.h"
#include "BaseTypes.h"

#include <cmath>
#include <algorithm>
#include <vector>


namespace NMLFramework
{
	//--------------------------------------------------------------------
	namespace detail
	{
		struct ConvexHullPointSorter
		{
			bool operator() (const Vector3 &a, const Vector3 &b) const
			{
				return a.x < b.x || (a.x == b.x && a.y < b.y);
			}
		};
	}

	//--------------------------------------------------------------------
	std::vector<Vector3> convexHull(std::vector<Vector3> P)
	{
		int n = P.size(), k = 0;
		std::vector<Vector3> H(2*n);

		// Sort points lexicographically
		std::sort(P.begin(), P.end(), detail::ConvexHullPointSorter());

		// Build lower hull
		for (int i = 0; i < n; i++)
		{
			while (k >= 2 && (H[k-1] - H[k-2]).crossProduct(P[i] - H[k-2]).z <= 0) k--;
			H[k++] = P[i];
		}

		// Build upper hull
		for (int i = n-2, t = k+1; i >= 0; i--) {
			while (k >= t && (H[k-1] - H[k-2]).crossProduct(P[i] - H[k-2]).z <= 0) k--;
			H[k++] = P[i];
		}

		H.resize(k);
		return H;
	}

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_GEOMUTILS_H__
