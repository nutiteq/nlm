/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_MODELLODTREE_H__
#define __NMLFRAMEWORK_MODELLODTREE_H__

#include "Prerequisites.h"
#include "BaseTypes.h"
#include "Model.h"

#include <vector>


namespace NMLFramework
{

	class ModelLODTree
	{

	public:

		class Node;

		typedef std::map<int, Node *> NodeMap;

		class Node
		{
		public:

			typedef std::map<std::string, long long> BindingMap;

			/** Default constructor */
			Node() : mParent(0) { }
			Node(const Node &node) : mParent(0) { *this = node; }
			~Node() { clearChildren(); }

			/** Access to node attributes */
			Bounds3 &getNodeBounds() { return mNodeBounds; }
			const Bounds3 &getNodeBounds() const { return mNodeBounds; }
			void setNodeBounds(const Bounds3 &bounds) { mNodeBounds = bounds; }

			Bounds3 &getModelBounds() { return mModelBounds; }
			const Bounds3 &getModelBounds() const { return mModelBounds; }
			void setModelBounds(const Bounds3 &bounds) { mModelBounds = bounds; }
			Model &getModel() { return mModel; }
			const Model &getModel() const { return mModel; }

			const std::string &getModelId() const { return mModelId; }
			void setModelId(const std::string &modelId) { mModelId = modelId; }
			const BindingMap & getMeshBindingMap() const { return mMeshBindingMap; }
			BindingMap & getMeshBindingMap() { return mMeshBindingMap; }
			const BindingMap & getTextureBindingMap() const { return mTextureBindingMap; }
			BindingMap & getTextureBindingMap() { return mTextureBindingMap; }

			Node *getParent() { return mParent; }
			const Node *getParent() const { return mParent; }
			int getChildCount() const { return (int) mChildren.size(); }
			Node &getChild(int idx) { return *mChildren[idx]; }
			const Node &getChild(int idx) const { return *mChildren[idx]; }

			/** Operations */
			void clearChildren() { for (size_t i = 0; i < mChildren.size(); i++) delete mChildren[i]; mChildren.clear(); }
			Node &addChild() { mChildren.push_back(new Node(this)); return *mChildren.back(); }

			/** Export/import LOD tree as NMLModelLODTree*/
			int saveNMLModelLODTreeNode(NMLPackage::ModelLODTree &nmlModelLODTree, NodeMap &nodeMap, const TextureExporter &textureExporter) const;
			bool loadNMLModelLODTreeNode(const NMLPackage::ModelLODTree &nmlModelLODTree, int id, NodeMap &nodeMap);

			/** Operators */
			size_t hash() const;
			Node &operator = (const Node &node);

		protected:

			Node(Node *parent) : mParent(parent) { }

			Model mModel;
			std::string mModelId;
			Bounds3 mModelBounds;
			Bounds3 mNodeBounds;
			std::vector<Node *> mChildren;
			Node *mParent;
			BindingMap mMeshBindingMap;
			BindingMap mTextureBindingMap;
		};

		/** Default constructor */
		ModelLODTree() : mRoot() { }

		/** Access to LOD tree attributes */
		Node &getRoot() { return mRoot; }
		const Node &getRoot() const { return mRoot; }

		/* Operations */
		void clear() { mRoot = Node(); }

		/** Export/import LOD tree as NMLModelLODTree*/
		bool saveNMLModelLODTree(NMLPackage::ModelLODTree &nmlModelLODTree, NodeMap &nodeMap, const TextureExporter &textureExporter) const;
		bool loadNMLModelLODTree(const NMLPackage::ModelLODTree &nmlModelLODTree, NodeMap &nodeMap);

		/** Operators */
		size_t hash() const { return mRoot.hash(); }

	private:

		Node mRoot;
	};

	inline size_t hash_value(const ModelLODTree::Node &modelLODTreeNode)
	{
		return modelLODTreeNode.hash();
	}

	inline size_t hash_value(const ModelLODTree &modelLODTree)
	{
		return modelLODTree.hash();
	}

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_MODELLODTREE_H__
