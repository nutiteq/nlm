/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_IMAGE_H__
#define __NMLFRAMEWORK_IMAGE_H__

#define XMD_H // hack for libjpeg on Win32

#define cimg_use_jpeg
#define cimg_use_png
#define cimg_verbosity 0

#include "Prerequisites.h"

#include "URILoader.h"

#pragma warning(push)
#pragma warning(disable: 4267 4244 4319)
#include "CImg.h"
#pragma warning(pop)

#include <boost/functional/hash.hpp>


namespace NMLFramework
{

	class Image
	{

	public:

		typedef cimg_library::CImg<unsigned char> CImage;

		/** Default constructor */
		Image() {}

		/** Load image from the given URI */
		bool load(const COLLADABU::URI& uri, URILoader& loader);
		bool reload(URILoader& loader) { return load(mURI, loader); }

		/** Resize image */
		void resize(int width, int height);

		/** Replicate image */
		void replicate(int xRepeat, int yRepeat);

		/** Access to image attributes */
		CImage &getCImage() { return mCImage; }
		const CImage &getCImage() const { return mCImage; }
		const COLLADABU::URI &getURI() const { return mURI; }

		/** Operators */
		size_t hash() const { return boost::hash_range(mCImage.data(), mCImage.data() + mCImage.size()); }
		bool operator == (const Image &image) const { return mURI == image.mURI && mCImage == image.mCImage; }
		bool operator != (const Image &image) const { return !(*this == image); }

	private:

		COLLADABU::URI mURI;
		CImage mCImage;
	};

	inline size_t hash_value(const Image &image)
	{
		return image.hash();
	}

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_IMAGE_H__
