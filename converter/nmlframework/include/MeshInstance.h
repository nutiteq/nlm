/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_MESHINSTANCE_H__
#define __NMLFRAMEWORK_MESHINSTANCE_H__

#include "Prerequisites.h"
#include "BaseTypes.h"
#include "Material.h"

#include <map>


namespace NMLFramework
{

	class MeshInstance
	{

	public:

		typedef std::map<std::string, Material> MaterialMap;

		/** Default constructor */
		MeshInstance() {}

		/** Access to mesh instance attributes */
		const std::string &getMeshId() const { return mMeshId; }
		void setMeshId(const std::string &id) { mMeshId = id; }
		const Matrix4 &getTransform() const { return mTransform; }
		void setTransform(const Matrix4 &trans) { mTransform = trans; }
		MaterialMap &getMaterialMap() { return mMaterialMap; }
		const MaterialMap &getMaterialMap() const { return mMaterialMap; }
		Material *getMaterial(const std::string &id);
		const Material *getMaterial(const std::string &id) const;
		std::string storeMaterial(const Material &material);

		/** Export/import mesh instance as NMLMeshInstance */
		bool saveNMLMeshInstance(NMLPackage::MeshInstance &nmlMeshInstance) const;
		bool loadNMLMeshInstance(const NMLPackage::MeshInstance &nmlMeshInstance);

	protected:

		std::string mMeshId;
		Matrix4 mTransform;
		MaterialMap mMaterialMap;
	};

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_MESHINSTANCE_H__
