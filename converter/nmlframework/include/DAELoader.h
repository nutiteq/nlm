/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_DAELOADER_H__
#define __NMLFRAMEWORK_DAELOADER_H__

#include "Prerequisites.h"
#include "Processor.h"

#include "COLLADAFWIWriter.h"
#include "COLLADAFWUniqueId.h"
#include "COLLADAFWVisualScene.h"
#include "COLLADAFWLibraryNodes.h"
#include "COLLADAFWMaterial.h"
#include "COLLADAFWEffect.h"
#include "COLLADAFWImage.h"
#include "COLLADAFWRoot.h"
#include "COLLADAFWGeometry.h"
#include "COLLADAFWNode.h"
#include "COLLADAFWVisualScene.h"
#include "COLLADAFWMesh.h"
#include "COLLADAFWPolygons.h"
#include "COLLADAFWInstanceGeometry.h"
#include "COLLADAFWMeshVertexData.h"
#include "COLLADASaxFWLLoader.h"
#include "COLLADASaxFWLIExtraDataCallbackHandler.h"

#include "COLLADABUURI.h"

#include "URILoader.h"
#include "Model.h"

#include <stack>
#include <list>
#include <map>


namespace NMLFramework
{

	class DAELoader : public COLLADAFW::IWriter, public Processor
	{

	public:

		DAELoader(const COLLADABU::URI& inputUri, URILoader& loader, const COLLADABU::Math::Matrix4 &rootTransform, bool loadTextures, Model &model);
		virtual ~DAELoader();

		/** Process DAE, store model */
		bool load();

	protected:

		/** Deletes the entire scene.
		@param errorMessage A message containing informations about the error that occurred.
		*/
		void cancel(const std::string &errorMessage);

		/** Prepare to receive data.*/
		void start();

		/** Remove all objects that don't have an object. Deletes unused visual scenes.*/
		void finish();

		/** When this method is called, the writer must write the global document asset.
		@return The writer should return true, if writing succeeded, false otherwise.*/
		virtual bool writeGlobalAsset (const COLLADAFW::FileInfo* asset);

		/** Writes the entire visual scene.
		@return True on succeeded, false otherwise.*/
		virtual bool writeVisualScene (const COLLADAFW::VisualScene* visualScene);

		/** Writes the scene.
		@return True on succeeded, false otherwise.*/
		virtual bool writeScene (const COLLADAFW::Scene* scene);

		/** Handles all nodes in the library nodes.
		@return True on succeeded, false otherwise.*/
		virtual bool writeLibraryNodes(const COLLADAFW::LibraryNodes* libraryNodes);

		/** Writes the geometry.
		@return True on succeeded, false otherwise.*/
		virtual bool writeGeometry (const COLLADAFW::Geometry* geometry);

		/** Writes the material.
		@return True on succeeded, false otherwise.*/
		virtual bool writeMaterial(const COLLADAFW::Material* material);

		/** Writes the effect.
		@return True on succeeded, false otherwise.*/
		virtual bool writeEffect(const COLLADAFW::Effect* effect);

		/** Writes the camera.
		@return True on succeeded, false otherwise.*/
		virtual bool writeCamera(const COLLADAFW::Camera* camera);

		/** Writes the image.
		@return True on succeeded, false otherwise.*/
		virtual bool writeImage(const COLLADAFW::Image* image);

		/** Writes the light.
		@return True on succeeded, false otherwise.*/
		virtual bool writeLight(const COLLADAFW::Light* light);

		/** Writes the animation.
		@return True on succeeded, false otherwise.*/
		virtual bool writeAnimation(const COLLADAFW::Animation* animation);

		/** Writes the animation.
		@return True on succeeded, false otherwise.*/
		virtual bool writeAnimationList(const COLLADAFW::AnimationList* animationList);

		/** Writes the skin controller data.
		@return True on succeeded, false otherwise.*/
		virtual bool writeSkinControllerData(const COLLADAFW::SkinControllerData* skinControllerData);

		/** Writes the controller.
		@return True on succeeded, false otherwise.*/
		virtual bool writeController(const COLLADAFW::Controller* Controller);

		/** When this method is called, the writer must write the formulas. All the formulas of the entire
		COLLADA file are contained in @a formulas.
		@return The writer should return true, if writing succeeded, false otherwise.*/
		virtual bool writeFormulas(const COLLADAFW::Formulas* formulas) { return true; }

		/** When this method is called, the writer must write the kinematics scene. 
		@return The writer should return true, if writing succeeded, false otherwise.*/
		virtual bool writeKinematicsScene(const COLLADAFW::KinematicsScene* kinematicsScene) { return true; }

	protected:

		class ExtraCallbackHandler : public COLLADASaxFWL::IExtraDataCallbackHandler
		{
		public:
			ExtraCallbackHandler(DAELoader *writer) : mUniqueId(COLLADAFW::UniqueId::INVALID), mWriter(writer) { }

			virtual bool elementBegin(const COLLADASaxFWL::ParserChar* elementName, const GeneratedSaxParser::xmlChar** attributes);
			virtual bool elementEnd(const COLLADASaxFWL::ParserChar* elementName);
			virtual bool textData(const COLLADASaxFWL::ParserChar* text, size_t textLength);
			virtual bool parseElement(const COLLADASaxFWL::ParserChar* profileName, const COLLADASaxFWL::StringHash& elementHash, const COLLADAFW::UniqueId& uniqueId);

		protected:
			COLLADAFW::UniqueId mUniqueId;
			DAELoader *mWriter;
		};

		friend class ExtraCallbackHandler;

	private:

		typedef std::set<COLLADAFW::UniqueId> UniqueIdSet;
		typedef std::list<COLLADAFW::LibraryNodes> LibraryNodesList;
		typedef std::list<COLLADAFW::VisualScene> VisualSceneList;
		typedef std::map<COLLADAFW::UniqueId, COLLADAFW::Node *> UniqueIdNodeMap;
		typedef std::map<COLLADAFW::UniqueId, COLLADAFW::Material> UniqueIdFWMaterialMap;
		typedef std::map<COLLADAFW::UniqueId, COLLADAFW::Effect> UniqueIdFWEffectMap;
		typedef std::map<COLLADAFW::UniqueId, Image> UniqueIdImageMap;
		typedef std::map<COLLADAFW::UniqueId, Mesh> UniqueIdMeshMap;

		enum Pass
		{
			PASS_COLLECT,
			PASS_CONVERT
		};

		URILoader &mURILoader;
		COLLADABU::URI mInputUri;
		Matrix4 mRootTransform;
		Pass mPass;
		bool mLoadTextures;
		Model &mModel;

		double mUnitInMeters;
		Matrix4 mAxisTransform;
		VisualSceneList mVisualSceneList;
		LibraryNodesList mLibraryNodesList;
		UniqueIdNodeMap mUniqueIdNodeMap;
		UniqueIdFWMaterialMap mUniqueIdFWMaterialMap;
		UniqueIdFWEffectMap mUniqueIdFWEffectMap;
		UniqueIdImageMap mUniqueIdImageMap;
		UniqueIdMeshMap mUniqueIdMeshMap;
		std::map<COLLADAFW::UniqueId, bool> mDoubleSided;
	
		// Disable copy constructor/assignment
		DAELoader(const DAELoader& pre);
		const DAELoader& operator= (const DAELoader& pre);

		std::vector<float> convertToFloatVector(const COLLADAFW::MeshVertexData &data) const;

		std::string getStringId(const COLLADAFW::UniqueId &id) const;
		std::string getMaterialId(const COLLADAFW::MaterialId &id) const;
		std::string getTextureId(const COLLADAFW::Texture *colladaTexture, const COLLADAFW::SamplerPointerArray &colladaSamplers);
		std::string getMeshId(const COLLADAFW::InstanceGeometry &instanceGeom);

		NMLPackage::ColorRGBA convertColor(const COLLADAFW::Color *colladaColor) const;
		NMLPackage::Vector3 convertVector3(const COLLADABU::Math::Vector3 &colladaVector) const;
		NMLPackage::Matrix4 convertMatrix4(const COLLADABU::Math::Matrix4 &colladaMatrix) const;
		NMLPackage::Sampler convertSampler(const COLLADAFW::Sampler *colladaSampler) const;
		NMLPackage::ColorOrTexture convertColorOrTexture(const COLLADAFW::ColorOrTexture *colladaColorOrTexture, const COLLADAFW::SamplerPointerArray &colladaSamplers, float translucency);
		
		bool createMaterial(const COLLADAFW::MaterialBinding *colladaMaterialBinding, const COLLADAFW::Material *colladaMaterial, Material &material);
		bool createSubmesh(const COLLADAFW::Mesh *colladaMesh, const COLLADAFW::MeshPrimitive *colladaMeshPrimitive, Submesh &submesh);
		void createMeshInstances(const COLLADAFW::Node *colladaNode, const Matrix4 &parentMatrix);

		void createUniqueIdNodeMap(COLLADAFW::Node* node);
		void createUniqueIdNodeMap(const COLLADAFW::NodePointerArray& nodes);
		void createUniqueIdNodeMap();
	};

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_DAELOADER_H__
