/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framrwork.
*/

#ifndef __NMLFRAMEWORK_WGS84UTILS_H__
#define __NMLFRAMEWORK_WGS84UTILS_H__

#include "Prerequisites.h"
#include "BaseTypes.h"

#include <cmath>


namespace NMLFramework
{

	//--------------------------------------------------------------------
	inline Vector3 webMercatorToWgs84(const Vector3 &mercatorPoint)
	{
		double x = mercatorPoint.x;
		double y = mercatorPoint.y;
		double num3 = x / 6378137.0;
		double num4 = num3 * 57.295779513082323;
		double num5 = std::floor((double)((num4 + 180.0) / 360.0));
		double num6 = num4 - (num5 * 360.0);
		double num7 = 1.5707963267948966 - (2.0 * std::atan(std::exp((-1.0 * y) / 6378137.0)));
		return Vector3(num6, num7 * 57.295779513082323, mercatorPoint.z);
	}

	//--------------------------------------------------------------------
	inline Vector3 wgs84ToWebMercator(const Vector3 &wgsPoint)
	{
		double num = wgsPoint.x * 0.017453292519943295;
		double x = 6378137.0 * num;
		double a = wgsPoint.y * 0.017453292519943295;
		double y = 3189068.5 * std::log((1.0 + std::sin(a)) / (1.0 - std::sin(a)));
		return Vector3(x, y, wgsPoint.z);
	}

	//--------------------------------------------------------------------
	inline Matrix4 internalWebMercatorTransformation(const Vector3 &mercatorPoint)
	{
		double k = 1.0 / std::cos(webMercatorToWgs84(mercatorPoint).y * COLLADABU::Math::Utils::DEG_TO_RAD);
		Matrix4 transform = Matrix4::IDENTITY;
		transform.setElement(0, 0, k);
		transform.setElement(1, 1, k);
		transform.setElement(2, 2, k);
		transform.setElement(0, 3, mercatorPoint.x);
		transform.setElement(1, 3, mercatorPoint.y);
		transform.setElement(2, 3, mercatorPoint.z);
		return transform;
	}

	//--------------------------------------------------------------------
	inline Matrix4 internalWgs84Transformation(const Vector3 &wgsPoint)
	{
		Vector3 mercatorPoint = wgs84ToWebMercator(wgsPoint);
		return internalWebMercatorTransformation(mercatorPoint);
	}

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_WGS84UTILS_H__
