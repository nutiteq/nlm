/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_LIGHTINGMODEL_H__
#define __NMLFRAMEWORK_LIGHTINGMODEL_H__

#include "Prerequisites.h"
#include "BaseTypes.h"


namespace NMLFramework
{

	class LightingModel
	{

	public:

		virtual ~LightingModel() { }

		/** Calculate lighting color based on world-space position and normal */
		virtual ColorRGBA calculateColor(const Vector3 &position, const Vector3 &normal) const = 0;
	};

	class BasicLightingModel : public LightingModel
	{

	public:

		BasicLightingModel(int model) : mModel(model) { }

		virtual ColorRGBA calculateColor(const Vector3 &position, const Vector3 &normal) const
		{
			Vector3 lightDir(0.35, 0.35, -0.87);
			float ambientLight = (mModel == 1 ? 0.45f : 0.0f);
			float LdotN = (float) std::abs(normal.normalisedCopy().dotProduct(lightDir.normalisedCopy()));
			float intensity = (1 - ambientLight) * LdotN + ambientLight;
			return ColorRGBA(intensity, intensity, intensity, 1);
		}

	private:
		int mModel;
	};

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_LIGHTINGMODEL_H__
