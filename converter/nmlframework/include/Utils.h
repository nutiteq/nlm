/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_UTILS_H__
#define __NMLFRAMEWORK_UTILS_H__

#include <string>
#include <map>
#include <sstream>
#include <iomanip>

#include <boost/uuid/sha1.hpp>


namespace NMLFramework
{

	template <typename T>
	inline std::string findUniqueId(const std::string &prefix, std::map<std::string, T> &map)
	{
		std::string id;
		for (size_t i = 0; true; i++)
		{
			std::stringstream stream;
			stream << prefix << i;
			id = stream.str();
			if (map.count(id) == 0)
				break;
			std::stringstream stream2;
			stream2 << prefix << (map.size() - i);
			id = stream2.str();
			if (map.count(id) == 0)
				break;
		}
		return id;
	}

	inline std::string calculateSHA1(const void *data, size_t size)
	{
		boost::uuids::detail::sha1 hasher;
		std::string hash(20, 0);
		hasher.process_bytes(data, size);
		unsigned int digest[5];
		hasher.get_digest(digest);
		for(int i = 0; i < 5; ++i)
		{
			const char *tmp = reinterpret_cast<char *>(digest);
			hash[i * 4 + 0] = tmp[i * 4 + 3];
			hash[i * 4 + 1] = tmp[i * 4 + 2];
			hash[i * 4 + 2] = tmp[i * 4 + 1];
			hash[i * 4 + 3] = tmp[i * 4 + 0];
		}
		return hash;
	}

	inline std::string calculateSHA1(const std::string &str)
	{
		return calculateSHA1(str.data(), str.size());
	}

	inline std::string hexString(size_t size)
	{
		std::stringstream stream;
		for (size_t i = 0; i < sizeof(size_t); i++)
		{
			stream << std::hex << std::setfill('0') << std::setw(2) << (int) (unsigned char) (size >> (i * 8));
		}
		return stream.str();
	}

	inline std::string hexString(const std::string &str)
	{
		std::stringstream stream;
		for (size_t i = 0; i < str.size(); i++)
		{
			stream << std::hex << std::setfill('0') << std::setw(2) << (int) (unsigned char) str[i];
		}
		return stream.str();
	}

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_UTILS_H__
