/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_SUBMESH_H__
#define __NMLFRAMEWORK_SUBMESH_H__

#include "Prerequisites.h"
#include "BaseTypes.h"

#include <vector>


namespace NMLFramework
{

	class Submesh
	{

	public:

		typedef NMLPackage::Submesh::Type Type;
		typedef std::vector<int> IntVector;
		typedef std::vector<float> FloatVector;

		/** Default constructor */
		Submesh() : mType(NMLPackage::Submesh::TRIANGLES) { }

		/** Access to submesh attributes */
		Type getType() const { return mType; }
		void setType(Type type) { mType = type; }
		const std::string &getMaterialId() const { return mMaterialId; }
		void setMaterialId(const std::string &id) { mMaterialId = id; }
		IntVector &getVertexCounts() { return mVertexCounts; }
		const IntVector &getVertexCounts() const { return mVertexCounts; }
		FloatVector &getPositions() { return mPositions; }
		const FloatVector &getPositions() const { return mPositions; }
		FloatVector &getNormals() { return mNormals; }
		const FloatVector &getNormals() const { return mNormals; }
		FloatVector &getUVs() { return mUVs; }
		const FloatVector &getUVs() const { return mUVs; }
		FloatVector &getColors() { return mColors; }
		const FloatVector &getColors() const { return mColors; }
		IntVector &getVertexIds() { return mVertexIds; }
		const IntVector &getVertexIds() const { return mVertexIds; }
		std::vector<IntVector> getPrimitiveIndices() const;
		int getVertexCount() const { return (int) mPositions.size() / 3; }
		bool isEmpty() const { return mPositions.empty(); }

		/** Set all vertex ids of this submesh */
		void setVertexIds(int id);

		/** Calculate bounds with given transform */
		Bounds3 calculateBounds(const Matrix4 &transform) const;

		/** Estimate submesh footprint in memory */
		int estimateMemoryFootprint() const;

		/** Calculate approximate texel-pixel ratio */
		float calculatePixelTexelRatio(const Matrix4 &transform, double pixelSize, const Vector3 &textureSize) const;

		/** Apply transformation to submesh */
		void applyTransformation(const Matrix4 &transform);

		/** Try to concatenate another submesh. Returns true is concatenation was successful. */
		bool concat(const Submesh &submesh);

		/** Export/import submesh as NMLSubmesh */
		bool saveNMLSubmesh(NMLPackage::Submesh &nmlSubmesh) const;
		bool loadNMLSubmesh(const NMLPackage::Submesh &nmlSubmesh);

		/** Operators */
		size_t hash() const;
		bool operator == (const Submesh &submesh) const;
		bool operator != (const Submesh &submesh) const { return !(*this == submesh); }

	private:

		Type mType;
		std::string mMaterialId;
		IntVector mVertexCounts;
		FloatVector mPositions;
		FloatVector mNormals;
		FloatVector mUVs;
		FloatVector mColors;
		IntVector mVertexIds;

		static std::string convertToNetworkByteVector(const std::vector<float> &data);
		static std::string convertToNetworkFloatVector(const std::vector<float> &data);
		static std::vector<float> convertFromNetworkByteVector(const std::string &netData);
		static std::vector<float> convertFromNetworkFloatVector(const std::string &netData);
	};

	inline size_t hash_value(const Submesh &submesh)
	{
		return submesh.hash();
	}

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_SUBMESH_H__
