/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_SPATIALTREE_H__
#define __NMLFRAMEWORK_SPATIALTREE_H__

#include "Prerequisites.h"
#include "BaseTypes.h"

#include <vector>

namespace NMLFramework
{

	template <typename Object, int Dim>
		class SpatialTree
	{

	public:

		enum { DIMENSIONS = Dim };

		typedef std::pair<Object *, Bounds3> ObjectBoundsPair;
		typedef std::list<Object *> ObjectList;
		typedef std::list<ObjectBoundsPair> ObjectBoundsPairList;

		class Node
		{

		public:

			enum { CHILDREN = 1 << Dim };

			explicit Node(Node * parent, const Bounds3 & nodeBounds) { mParent = parent; mNodeBounds = nodeBounds; for (int i = 0; i < CHILDREN; i++) mChildren[i] = 0; } 
			~Node() { for (int i = 0; i < CHILDREN; i++) delete mChildren[i]; }

			Node * getRoot() { if (mParent == 0) return this; else return mParent->getRoot(); }
			const Node * getRoot() const { if (mParent == 0) return this; else return mParent->getRoot(); }
			Node * getParent() { return mParent; }
			const Node * getParent() const { return mParent; }
			Node * getChild(int idx) { assert(idx >= 0 && idx < CHILDREN); return mChildren[idx]; }
			const Node * getChild(int idx) const { assert(idx >= 0 && idx < CHILDREN); return mChildren[idx]; }
			ObjectList & getObjects() { return mObjects; }
			const ObjectList & getObjects() const { return mObjects; }
			const Bounds3 & getObjectBounds() const { return mGlobalObjectBounds; }
			const Bounds3 & getNodeBounds() const { return mNodeBounds; }

			void setChild(int idx, Node * node)
			{
				assert(idx >= 0 && idx < CHILDREN);
				delete mChildren[idx];
				mChildren[idx] = node;
				updateObjectBounds();
			}

			void setObjects(const ObjectBoundsPairList & objs)
			{
				mObjects.clear();
				mLocalObjectBounds.clear();
				for (typename ObjectBoundsPairList::const_iterator it = objs.begin(); it != objs.end(); it++)
				{
					mObjects.push_back(it->first);
					mLocalObjectBounds.add(it->second);
				}
				updateObjectBounds();
			}

			void addObjects(const Node & node)
			{
				for (typename ObjectList::const_iterator it = node.getObjects().begin(); it != node.getObjects().end(); it++)
				{
					mObjects.push_back(*it);
				}
				mLocalObjectBounds.add(node.getObjectBounds());
				updateObjectBounds();
			}

		protected:

			void updateObjectBounds()
			{
				mGlobalObjectBounds = mLocalObjectBounds;
				for (int i = 0; i < CHILDREN; i++)
				{
					if (mChildren[i] != 0)
						mGlobalObjectBounds.add(mChildren[i]->getObjectBounds());
				}
				if (mParent != 0)
					mParent->updateObjectBounds();
			}

			Bounds3 mNodeBounds;
			Bounds3 mLocalObjectBounds;
			Bounds3 mGlobalObjectBounds;
			ObjectList mObjects;
			Node * mParent;
			Node * mChildren[CHILDREN];
		};

		SpatialTree() : mRoot(0)
		{
		}

		virtual ~SpatialTree()
		{
			delete mRoot;
		}

		void create(const ObjectBoundsPairList & objs, double minNodeSize, double maxNodeSize)
		{
			clear();

			// Calculate bounds for all objects
			Bounds3 bounds;
			for (typename ObjectBoundsPairList::const_iterator it = objs.begin(); it != objs.end(); it++)
			{
				const ObjectBoundsPair & obj = *it;
				bounds.add(obj.second);
			}

			// Change bounds to be at least maxNodeSize along each axis (except Z)
			Vector3 halfMaxSize(maxNodeSize / 2, maxNodeSize / 2, 0);
			bounds.add(Bounds3(bounds.center() - halfMaxSize, bounds.center() + halfMaxSize));

			// Create all nodes recursively starting from the root
			mRoot = createNode(0, bounds, objs, minNodeSize, MAX_TREE_DEPTH);
		}

		void clear()
		{
			delete mRoot;
			mRoot = 0;
		}

		Node * getRoot()
		{
			return mRoot;
		}

		const Node * getRoot() const
		{
			return mRoot;
		}

	protected:

		enum { MAX_TREE_DEPTH = 20 };

		static Node * createNode(Node * parent, const Bounds3 & bounds, const ObjectBoundsPairList & objs, double minNodeSize, int depthLimit)
		{
			if (objs.empty())
				return 0;

			double nodeSize = 0;
			for (int i = 0; i < Dim; i++)
			{
				nodeSize = std::max(nodeSize, bounds.size()[i]);
			}
			Node * node = new Node(parent, bounds);
			if (depthLimit < 0 || nodeSize < minNodeSize)
			{
				node->setObjects(objs);
				return node;
			}

			// Select axes along which to split
			bool splitDim[DIMENSIONS];
			for (int i = 0; i < Dim; i++)
			{
				splitDim[i] = true;
				for (int j = 0; j < Dim; j++)
				{
					if (bounds.size()[i] * 2 < bounds.size()[j])
						splitDim[i] = false;
				}
			}

			// Do actual splitting based on bounds center
			ObjectBoundsPairList childObjs[Node::CHILDREN];
			for (typename ObjectBoundsPairList::const_iterator it = objs.begin(); it != objs.end(); it++)
			{
				const ObjectBoundsPair & obj = *it;
				int idx = 0;
				for (int j = 0; j < DIMENSIONS; j++)
				{
					if (!splitDim[j])
						continue;
					idx += (obj.second.center()[j] > bounds.center()[j] ? 1 << j : 0);
				}
				childObjs[idx].push_back(obj);
			}

			// Create child nodes recursively
			for (int i = 0; i < Node::CHILDREN; i++)
			{
				Bounds3 childBounds = bounds;
				for (int j = 0; j < DIMENSIONS; j++)
				{
					if (!splitDim[j])
						continue;
					if (i & (1 << j))
						childBounds.min[j] = bounds.center()[j];
					else
						childBounds.max[j] = bounds.center()[j];
				}
				node->setChild(i, createNode(node, childBounds, childObjs[i], minNodeSize, depthLimit - 1));
			}
			return node;
		}

		Node * mRoot;
	};

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_SPATIALTREE_H__
