#ifndef __NUTITEQ_LICENSEMANAGER_H__
#define __NUTITEQ_LICENSEMANAGER_H__

#include <string>

#include <boost/property_tree/ptree.hpp>
#include <boost/program_options.hpp>
#include <boost/date_time.hpp>


namespace Nutiteq
{

	class LicenseManager
	{

	private:

		boost::property_tree::ptree mPTree;

		static LicenseManager gInstance;

		static std::string unbase64(const std::string &b64data);
		static std::string base64(const std::string &data);

	public:

		struct Options
		{
			Options() { }

			static void verifyLicense(const std::string &product, const std::string &encPublicKey, const boost::program_options::variables_map &vm)
			{
				if (vm.count("license-file") == 0)
					throw std::runtime_error("License must be specified via --license-file option");
				std::string licenseFile = vm["license-file"].as<std::string>();
				std::ifstream stream;
				stream.open(licenseFile.c_str());
				if (!stream.good())
					throw std::runtime_error("Could not open license file");
				std::string encLicense;
				std::getline(stream, encLicense);
				if (encLicense.size() < 1)
					throw std::runtime_error("Invalid license");
				std::string license = LicenseManager::unbase64(encLicense.substr(1));
				std::string publicKey = LicenseManager::unbase64(encPublicKey);
				LicenseManager &licenseManager = LicenseManager::getInstance();
				if (!licenseManager.load(license, publicKey))
					throw std::runtime_error("Invalid license");
				std::string validUntil = licenseManager.getProperty<std::string>("validUntil");
				if (!validUntil.empty())
				{
					boost::gregorian::date validDate(boost::gregorian::from_simple_string(validUntil));
					boost::gregorian::date currentDate(boost::gregorian::day_clock::local_day());
					if (currentDate > validDate)
						throw std::runtime_error("License expired");
				}
				std::string products = licenseManager.getProperty<std::string>("products");
				if (("," + products + ",").find("," + product + ",") == std::string::npos)
					throw std::runtime_error("License not valid for current product");

				std::string clientName = licenseManager.getProperty<std::string>("clientName");
				if (!clientName.empty())
				{
					std::cout << "Found valid license, product licensed to: " << clientName << std::endl;
				}
			}

			static boost::program_options::options_description getDescription()
			{
				namespace po = boost::program_options;
				po::options_description desc("License options");
				desc.add_options()
					("license-file", po::value<std::string>(), "license file")
				;
				return desc;
			}
		};

		void clear() { mPTree.clear(); }
		bool load(const std::string &license, const std::string &publicKey);

		template <typename T> T getProperty(const std::string &id) const
		{
			if (mPTree.count(id) == 0)
				return T();
			return mPTree.get<T>(id);
		}

		template <typename T> T getProperty(const std::string &id, T defaultValue) const
		{
			if (mPTree.count(id) == 0)
				return defaultValue;
			return mPTree.get<T>(id);
		}

		static LicenseManager &getInstance()
		{
			return gInstance;
		}
	};

} // namespace Nutiteq

#endif // __NUTITEQ_LICENSEMANAGER_H__
