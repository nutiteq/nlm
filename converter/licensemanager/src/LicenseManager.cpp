#include "LicenseManager.h"

#include <openssl/x509.h>
#include <openssl/evp.h>
#include <openssl/bio.h>

#include <string>
#include <sstream>

#include <boost/property_tree/ini_parser.hpp>


class OpenSSLInitializer {

public:
	OpenSSLInitializer() {
		OpenSSL_add_all_digests();
	}
} gOpenSSLInitializer;

namespace Nutiteq
{
	//--------------------------------------------------------------------
	LicenseManager LicenseManager::gInstance;

	//--------------------------------------------------------------------
	std::string LicenseManager::unbase64(const std::string &b64data)
	{
		BIO *b64 = BIO_new(BIO_f_base64());
		BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
		BIO *bmem = BIO_new_mem_buf(const_cast<char *>(b64data.data()), b64data.size());
		bmem = BIO_push(b64, bmem);

		char c;
		std::string data;
		while (BIO_read(bmem, &c, 1) == 1)
		{
			data += c;
		}

		BIO_free_all(bmem);
		return data;
	}

	//--------------------------------------------------------------------
	std::string LicenseManager::base64(const std::string &data)
	{
		BIO *b64 = BIO_new(BIO_f_base64());
		BIO *bmem = BIO_new(BIO_s_mem());
		b64 = BIO_push(b64, bmem);
		BIO_write(b64, data.data(), data.size());
		BIO_flush(b64);

		char *b64dataBuf = 0;
		long b64dataLen = BIO_get_mem_data(bmem, &b64dataBuf);
		std::string b64data(b64dataBuf, b64dataLen);

		BIO_free_all(b64);
		return b64data;
	}

	//--------------------------------------------------------------------
	bool LicenseManager::load(const std::string &license, const std::string &publicKey)
	{
		BIO *bio = BIO_new(BIO_s_mem());
		BIO_write(bio, publicKey.data(), publicKey.size());

		EVP_PKEY *evp_pkey = EVP_PKEY_new();
		EVP_PKEY_assign_DSA(evp_pkey, d2i_DSA_PUBKEY_bio(bio, NULL));

		EVP_MD_CTX md_ctx;
		EVP_VerifyInit(&md_ctx, EVP_dss1());

		std::istringstream stream(license);
		std::string signatureString;
		while (!stream.eof())
		{
			std::string line;
			std::getline(stream, line);
			if (line.empty())
				break;
			signatureString += line;
		}
		std::string propertiesString;
		std::string data;
		while (!stream.eof())
		{
			std::string line;
			std::getline(stream, line);
			if (line.empty())
				break;
			EVP_VerifyUpdate(&md_ctx, line.data(), line.size());
			propertiesString += line + "\n";
		}

		std::string sign = unbase64(signatureString);
		int result = EVP_VerifyFinal(&md_ctx, reinterpret_cast<const unsigned char *>(sign.data()), sign.size(), evp_pkey);

		EVP_PKEY_free(evp_pkey);

		BIO_free_all(bio);

		if (result != 1)
			return false;

		std::istringstream propertiesStream(propertiesString);
		boost::property_tree::ini_parser::read_ini(propertiesStream, mPTree);
		return true;
	}

}
