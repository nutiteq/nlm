#ifndef __BOOST_FSUTILS_H__
#define __BOOST_FSUTILS_H__

#include <vector>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>


namespace boost_fsutils
{
	/**
	* https://svn.boost.org/trac/boost/ticket/1976#comment:2
	* 
	* "The idea: uncomplete(/foo/new, /foo/bar) => ../new
	*  The use case for this is any time you get a full path (from an open dialog, perhaps)
	*  and want to store a relative path so that the group of files can be moved to a different
	*  directory without breaking the paths. An IDE would be a simple example, so that the
	*  project file could be safely checked out of subversion."
	* 
	* ALGORITHM:
	* iterate path and base
	* compare all elements so far of path and base
	* whilst they are the same, no write to output
	* when they change, or one runs out:
	*   write to output, ../ times the number of remaining elements in base
	*   write to output, the remaining elements in path
	*/
	inline boost::filesystem::path naive_uncomplete(boost::filesystem::path p, boost::filesystem::path base)
	{
		using boost::filesystem::path;

		p = boost::filesystem::canonical(p);
		base = boost::filesystem::canonical(base);
		if (p == base)
			return "./";
		/*!! this breaks stuff if path is a filename rather than a directory,
		which it most likely is... but then base shouldn't be a filename so... */

		boost::filesystem::path from_path, from_base, output;

		boost::filesystem::path::iterator path_it = p.begin(),    path_end = p.end();
		boost::filesystem::path::iterator base_it = base.begin(), base_end = base.end();

		// check for emptiness
		if ((path_it == path_end) || (base_it == base_end))
			throw std::runtime_error("path or base was empty; couldn't generate relative path");

#ifdef WIN32
		// drive letters are different; don't generate a relative path
		if (*path_it != *base_it)
			return p;

		// now advance past drive letters; relative paths should only go up
		// to the root of the drive and not past it
		++path_it, ++base_it;
#endif

		// Cache system-dependent dot, double-dot and slash strings
		const std::string _dot  = ".";
		const std::string _dots = "..";
		const std::string _sep  = "/";

		// iterate over path and base
		while (true) {

			// compare all elements so far of path and base to find greatest common root;
			// when elements of path and base differ, or run out:
			if ((path_it == path_end) || (base_it == base_end) || (*path_it != *base_it)) {

				// write to output, ../ times the number of remaining elements in base;
				// this is how far we've had to come down the tree from base to get to the common root
				for (; base_it != base_end; ++base_it) {
					if (*base_it == _dot)
						continue;
					else if (*base_it == _sep)
						continue;

					output /= "..";
				}

				// write to output, the remaining elements in path;
				// this is the path relative from the common root
				boost::filesystem::path::iterator path_it_start = path_it;
				for (; path_it != path_end; ++path_it) {

					if (path_it != path_it_start)
						output /= "/";

					if (*path_it == _dot)
						continue;
					if (*path_it == _sep)
						continue;

					output /= *path_it;
				}

				break;
			}

			// add directory level to both paths and continue iteration
			from_path /= path(*path_it);
			from_base /= path(*base_it);

			++path_it, ++base_it;
		}

		return output;
	}

	/**
	* Resolve wildcards to actual paths. For example, "dir\*.txt" is resolved to actual files.
	*
	* ALGORITHM:
	*  filesystem wild cards are replaced with regex wildcards
	*  initial search path is found
	*  paths are scanned and matched starting from initial search path
	*/
	inline std::vector<std::string> find_files(const std::string & filesearch, const boost::filesystem::path &parentpath = boost::filesystem::path())
	{
		std::vector<std::string> fileset;

		// Initialize path p
		boost::filesystem::path p(parentpath.empty() ? filesearch : parentpath / filesearch);
		p = boost::filesystem::system_complete(p);
		if (boost::filesystem::is_directory(p))
		{
			fileset.push_back(boost::filesystem::absolute(p.string()).string());
			return fileset;
		}

		// Initialize regex filter - use *.* as default if nothing is given in filesearch
		std::string f(!boost::filesystem::is_directory(p) ? p.filename().string() : "*");

		// Make replacements: 1: Replace . with \. (i.e. escape it)  2: Replace any ? with .  3: Replace any * with .* 
		f = boost::regex_replace(f, boost::regex("(\\.)|(\\?)|(\\*)"), "(?1\\\\.)(?2.)(?3.*)", boost::match_default | boost::format_all);
		// Replace () []
		f = boost::regex_replace(f, boost::regex("(\\[)|(\\])|(\\()|(\\))"), "(?1\\\\\\[)(?2\\\\\\])(?3\\\\\\()(?4\\\\\\))", boost::match_default | boost::format_all);
		const boost::regex filter(f, boost::regex::icase); // TODO: icase is good for Windows-only

		boost::filesystem::path dir(boost::filesystem::is_directory(p) ? p : p.parent_path());
		if (boost::filesystem::is_directory(dir))
		{
			// Iterate through contents (files/directories) of directory...
			for (boost::filesystem::directory_iterator it(dir); it != boost::filesystem::directory_iterator(); ++it)
			{
				// Match
				boost::filesystem::path filepath = parentpath / it->path().filename();
				if (boost::regex_match(filepath.string(), filter))
					fileset.push_back(boost::filesystem::absolute(it->path()).string());
			}
		}

		return fileset;
	}
}

#endif
