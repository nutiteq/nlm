/*
Copyright (c) 2012 Nutiteq

This file is part of KMZ2Db.
*/

#include <pqxx/pqxx>

#include "KMZ2PgConverter.h"
#include "DAEPreprocessor.h"
#include "Utils.h"
#include "WGS84Utils.h"
#include "TextureExporter.h"

#include <boost/filesystem.hpp>


namespace KMZ2Db
{

	//--------------------------------------------------------------------
	bool KMZ2PgConverter::initializeDb(pqxx::connection &db)
	{
		// Check that this is a proper PostGIS db
		try
		{
			pqxx::work(db).exec("SELECT srid FROM spatial_ref_sys");
		}
		catch (const std::exception &ex)
		{
			throw pqxx::data_exception("Spatial reference system table is missing, database not properly configured?");
		}

		// Create tables
		try
		{
			pqxx::work txn(db);
			txn.exec("CREATE TABLE IF NOT EXISTS Textures(id INTEGER NOT NULL, level INTEGER NOT NULL, nmltexture BYTEA NOT NULL, nmltexture_sha1hash BYTEA NOT NULL, PRIMARY KEY (id, level))");
			txn.exec("CREATE TABLE IF NOT EXISTS Meshes(id INTEGER NOT NULL PRIMARY KEY, nmlmesh BYTEA NOT NULL, nmlmesh_sha1hash BYTEA NOT NULL)");
			txn.exec("CREATE TABLE IF NOT EXISTS Models(id INTEGER NOT NULL PRIMARY KEY, model_key TEXT NOT NULL, nmlmodel BYTEA NOT NULL, nmlmodel_sha1hash BYTEA NOT NULL)"); // UNIQUE model_key
			txn.exec("CREATE TABLE IF NOT EXISTS ModelLODTrees(id INTEGER NOT NULL PRIMARY KEY, nmlmodellodtree BYTEA NOT NULL, nmlmodellodtree_sha1hash BYTEA NOT NULL)");
			txn.exec("CREATE TABLE IF NOT EXISTS ModelInfo(id INTEGER NOT NULL PRIMARY KEY, modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, global_id TEXT NOT NULL, model_id INTEGER NOT NULL, groundheight REAL NOT NULL, maxheight REAL NOT NULL, folder TEXT, vertices INTEGER NOT NULL, materials INTEGER NOT NULL, textures INTEGER NOT NULL, UNIQUE (modellodtree_id, global_id), UNIQUE (modellodtree_id, model_id))");
			txn.exec("CREATE TABLE IF NOT EXISTS MapTiles(id INTEGER NOT NULL PRIMARY KEY, modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, tile_x INTEGER NOT NULL, tile_y INTEGER NOT NULL, tile_size REAL NOT NULL, groundheight REAL NOT NULL, UNIQUE (tile_x, tile_y, tile_size))");
			txn.exec("CREATE TABLE IF NOT EXISTS ModelLODTreeNodeTextures(modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, node_id INTEGER NOT NULL, local_id TEXT NOT NULL, texture_id INTEGER NOT NULL, level INTEGER NOT NULL, FOREIGN KEY(texture_id, level) REFERENCES Textures(id, level) ON DELETE CASCADE)");
			txn.exec("CREATE TABLE IF NOT EXISTS ModelLODTreeNodeMeshes(modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, node_id INTEGER NOT NULL, local_id TEXT NOT NULL, mesh_id INTEGER NOT NULL REFERENCES Meshes(id) ON DELETE CASCADE, nmlmeshop BYTEA NULL)");
			txn.exec("CREATE TABLE IF NOT EXISTS ModelLODTreeNodeModels(modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, node_id INTEGER NOT NULL, model_key TEXT NOT NULL)");
			txn.commit();
		}
		catch (const std::exception &ex)
		{
			return false;
		}

		// Try to create geometry columns - if we fail, then most likely these already exist. Thus ignore exceptions.
		try
		{
			pqxx::work txn(db);
			txn.exec("SELECT AddGeometryColumn(Lower('ModelInfo'), 'mappos', 3857, 'POINT', 2)");
			txn.exec("SELECT AddGeometryColumn(Lower('ModelInfo'), 'floorplan', 3857, 'POLYGON', 2)");
			txn.exec("SELECT AddGeometryColumn(Lower('MapTiles'), 'mappos', 3857, 'POINT', 2)");
			txn.exec("SELECT AddGeometryColumn(Lower('MapTiles'), 'mapbounds', 3857, 'POLYGON', 2)");
			txn.commit();
		}
		catch (const std::exception &ex)
		{
		}

		// Try to create sequences - if we fail, then most likely sequences already exist. Thus ignore exceptions.
		try
		{
			pqxx::work txn(db);
			txn.exec("CREATE SEQUENCE Textures_seq");
			txn.exec("CREATE SEQUENCE Meshes_seq");
			txn.exec("CREATE SEQUENCE Models_seq");
			txn.exec("CREATE SEQUENCE ModelLODTrees_seq");
			txn.exec("CREATE SEQUENCE ModelInfo_seq");
			txn.exec("CREATE SEQUENCE MapTiles_seq");
			txn.commit();
		}
		catch (const std::exception &ex)
		{
		}

		// Try to create indexes - if we fail, then most likely indexes already exist. Thus ignore exceptions.
		try
		{
			pqxx::work txn(db);
			txn.exec("CREATE INDEX Textures_id ON Textures(id)");
			txn.exec("CREATE INDEX Textures_nmltexture_sha1hash ON Textures(nmltexture_sha1hash)"); // index for fast searching
			txn.exec("CREATE INDEX Meshes_nmlmesh_sha1hash ON Meshes(nmlmesh_sha1hash)");
			txn.exec("CREATE INDEX Models_model_keyhash ON Models USING hash(model_key)");
			txn.exec("CREATE INDEX Models_nmlmodel_sha1hash ON Models(nmlmodel_sha1hash)");
			txn.exec("CREATE INDEX ModelLODTrees_nmlmodellodtree_sha1hash ON ModelLODTrees(nmlmodellodtree_sha1hash)");
			txn.exec("CREATE INDEX ModelInfo_modellodtree_id ON ModelInfo(modellodtree_id)");
			txn.exec("CREATE INDEX ModelInfo_modellodtree_id_global_id ON ModelInfo(modellodtree_id, global_id)");
			txn.exec("CREATE INDEX ModelInfo_modellodtree_id_model_id ON ModelInfo(modellodtree_id, model_id)");
			txn.exec("CREATE INDEX MapTiles_modellodtree_id ON MapTiles(modellodtree_id)");
			txn.exec("CREATE INDEX MapTiles_modellodtree_tile ON MapTiles(tile_x, tile_y, tile_size)");
			txn.exec("CREATE INDEX MapTiles_mapbounds_y0 ON MapTiles(mapbounds_y0)");
			txn.exec("CREATE INDEX MapTiles_mapbounds_y1 ON MapTiles(mapbounds_y1)");
			txn.exec("CREATE INDEX ModelLODTreeNodeTextures_modellodtree_id ON ModelLODTreeNodeTextures(modellodtree_id)");
			txn.exec("CREATE INDEX ModelLODTreeNodeMeshes_modellodtree_id ON ModelLODTreeNodeMeshes(modellodtree_id)");
			txn.exec("CREATE INDEX ModelLODTreeNodeModels_modellodtree_id ON ModelLODTreeNodeModels(modellodtree_id)");
			txn.commit();
		}
		catch (const std::exception &ex)
		{
		}

		// Done
		return true;
	}

	//--------------------------------------------------------------------
	bool KMZ2PgConverter::freezeDb(pqxx::connection &db)
	{
		pqxx::work txn(db);
		txn.exec("DROP TABLE Models");
		txn.exec("VACUUM");
		txn.commit();
		return true;
	}

	//--------------------------------------------------------------------
	bool KMZ2PgConverter::convert()
	{
		// Group models into tiles and build list of model references
		mModelLODTreeBuilderMap.clear();
		for (size_t i = 0; i < mCommands.size(); i++)
		{
			const Command &command = mCommands[i];

			if (command.type == Command::ADD || command.type == Command::UPDATE)
			{
				const COLLADABU::URI &inputUri = command.kmzFile;
				reportProgress("convert", "Processing " + inputUri.originalStr());

				KMZURILoader uriLoader(inputUri.toNativePath(), "");
				std::list<std::string> kmlFiles = uriLoader.getFiles(".kml");
				if (kmlFiles.empty())
					reportError("convert", "No KML files inside KMZ!", SEVERITY_WARNING);
				std::list<ModelInfo> modelInfos;
				for (std::list<std::string>::const_iterator it = kmlFiles.begin(); it != kmlFiles.end(); it++)
				{
					std::string kmlFile = uriLoader.resolve(*it);
					reportProgress("convert", "Parsing " + *it);

					pugi::xml_document doc;
					if (!doc.load_file(kmlFile.c_str()))
					{
						reportError("convert", "Could not parse KML file: " + *it, SEVERITY_WARNING);
						continue;
					}
					processKML(modelInfos, doc, uriLoader);
				}
				for (std::list<ModelInfo>::const_iterator it = modelInfos.begin(); it != modelInfos.end(); it++)
				{
					const ModelInfo &modelInfo = *it;
					if (!command.globalId.empty() && modelInfo.globalId != command.globalId)
						continue;

					TileId tileId = getTileId(modelInfo.mapPos);
					ModelLODTreeBuilderPtr builder = getModelLODTreeBuilder(tileId);
					if (command.type == Command::ADD)
					{
						// Do nothing if model has been already added
						if (findModelId(modelInfo.globalId) != -1)
						{
							reportError("convert", "Model already exists, not adding", SEVERITY_WARNING);
							continue;
						}
						builder->add(modelInfo);
					}
					else if (command.type == Command::UPDATE)
					{
						// Split update into remove and add parts - as updated model can move between tiles
						ModelId modelId = findModelId(modelInfo.globalId);
						if (modelId != -1)
						{
							TileId tileId = findTileId(modelInfo.globalId);
							ModelLODTreeBuilderPtr oldBuilder = getModelLODTreeBuilder(tileId);
							oldBuilder->remove(modelId);
						}
						builder->add(modelInfo);
					}
				}
			}
			else if (command.type == Command::REMOVE)
			{
				int modelId = findModelId(command.globalId);
				if (modelId == -1)
				{
					reportError("convert", "Model does not exist, can not remove", SEVERITY_WARNING);
					continue;
				}
				TileId tileId = findTileId(command.globalId);
				ModelLODTreeBuilderPtr builder = getModelLODTreeBuilder(tileId);
				builder->remove(modelId);
			}
		}

		// Create DB lodtree records for each tile
		for (ModelLODTreeBuilderMap::const_iterator it = mModelLODTreeBuilderMap.begin(); it != mModelLODTreeBuilderMap.end(); it++)
		{
			TileId tileId = it->first;
			ModelLODTreeBuilderPtr builder = it->second;

			// Build model tree
			reportProgress("convert", "Building new LOD tree for tile");
			ModelLODTree modelLODTree;
			builder->buildModelLODTree(modelLODTree);

			// Transaction for current tile
			mTileTxn.reset(new pqxx::work(mDB));

			// Save LOD tree
			reportProgress("convert", "Saving LOD tree");
			long long modelLODTreeId = saveModelLODTree(modelLODTree);

			// Compress LOD tree meshes
			if (mOptions.compressLODTreeMeshes)
			{
				// TODO: implement
				reportError("convert", "LOD tree mesh compression not yet implemented", SEVERITY_WARNING);
			}

			// Save model info
			reportProgress("convert", "Saving model info");
			saveModelInfoMap(modelLODTreeId, builder->getAll());

			// Save generated tile
			reportProgress("convert", "Saving map tile");
			Bounds3 localBounds = modelLODTree.getRoot().getModelBounds();
			Vector3 mapPos = getTilePos(tileId);
			saveMapTile(modelLODTreeId, tileId, mapPos, localBounds);

			// Commit changes
			mTileTxn->commit();
		}
		return true;
	}

	//--------------------------------------------------------------------
	KMZ2PgConverter::TileId KMZ2PgConverter::findTileId(const std::string &globalId) const
	{
		pqxx::work txn(mDB);
		pqxx::result r = txn.exec("SELECT mt.tile_x, mt.tile_y FROM MapTiles mt, ModelInfo mi WHERE mi.modellodtree_id=mt.modellodtree_id AND mi.global_id=" + txn.quote(globalId) + " AND mt.tile_size=" + txn.quote(mOptions.tileSize));
		if (!r.empty())
		{
			int x = r[0][0].as<int>();
			int y = r[0][1].as<int>();
			return std::make_pair(x, y);
		}
		return TileId();
	}

	//--------------------------------------------------------------------
	KMZ2PgConverter::ModelId KMZ2PgConverter::findModelId(const std::string &globalId) const
	{
		pqxx::work txn(mDB);
		pqxx::result r = txn.exec("SELECT mi.model_id FROM MapTiles mt, ModelInfo mi WHERE mi.modellodtree_id=mt.modellodtree_id AND mi.global_id=" + txn.quote(globalId) + " AND mt.tile_size=" + txn.quote(mOptions.tileSize));
		if (!r.empty())
		{
			return r[0][0].as<int>();
		}
		return -1;
	}

	//--------------------------------------------------------------------
	KMZ2PgConverter::ModelLODTreeBuilderPtr KMZ2PgConverter::getModelLODTreeBuilder(const TileId &tileId)
	{
		ModelLODTreeBuilderPtr builder = mModelLODTreeBuilderMap[tileId];
		if (!builder)
		{
			long long modelLODTreeId = -1;
			{
				pqxx::work txn(mDB);
				pqxx::result r = txn.exec("SELECT modellodtree_id FROM MapTiles WHERE tile_x=" + txn.quote(tileId.first) + " AND tile_y=" + txn.quote(tileId.second) + " AND tile_size=" + txn.quote(mOptions.tileSize));
				if (!r.empty())
				{
					modelLODTreeId = r[0][0].as<long long>();
				}
			}

			ModelLODTree modelLODTree;
			ModelInfoMap modelInfoMap;
			if (modelLODTreeId != -1)
			{
				loadModelLODTree(modelLODTreeId, modelLODTree);
				loadModelInfoMap(modelLODTreeId, modelInfoMap);
			}

			Vector3 mapPos = getTilePos(tileId);
			builder.reset(new KMZModelLODTreeBuilder(KMZModelLODTreeBuilder::getDefaultOptions(), *this, modelLODTree, modelInfoMap, mapPos));
			mModelLODTreeBuilderMap[tileId] = builder;
		}
		return builder;
	}

	//--------------------------------------------------------------------
	bool KMZ2PgConverter::loadModelLODTree(long long modelLODTreeId, ModelLODTree &modelLODTree)
	{
		pqxx::work txn(mDB);
		pqxx::result r = txn.exec("SELECT nmlmodellodtree, LENGTH(nmlmodellodtree) FROM ModelLODTrees WHERE id=" + txn.quote(modelLODTreeId));
		if (!r.empty())
		{
			pqxx::binarystring modelLODTreeData(r[0][0]);
			NMLPackage::ModelLODTree nmlModelLODTree;
			nmlModelLODTree.ParseFromArray(modelLODTreeData.get(), modelLODTreeData.size());
			ModelLODTree::NodeMap nodeMap;
			modelLODTree.loadNMLModelLODTree(nmlModelLODTree, nodeMap);

			// Load texture bindings
			r = txn.exec("SELECT local_id, node_id, texture_id FROM ModelLODTreeNodeTextures WHERE modellodtree_id=" + txn.quote(modelLODTreeId) + " AND level=0");
			for (pqxx::result::const_iterator it = r.begin(); it != r.end(); it++)
			{
				std::string localId = (*it)[0].as<std::string>();
				int nodeId = (*it)[1].as<int>();
				long long textureId = (*it)[2].as<long long>();
				ModelLODTree::Node *node = nodeMap[nodeId];
				if (node == 0)
				{
					reportError("loadModelLODTree", "Inconsistent node mapping", _SEVERITY_ERROR);
					return false;
				}
				node->getTextureBindingMap()[localId] = textureId;
			}

			// Load mesh bindings
			r = txn.exec("SELECT local_id, node_id, mesh_id FROM ModelLODTreeNodeMeshes WHERE modellodtree_id=" + txn.quote(modelLODTreeId));
			for (pqxx::result::const_iterator it = r.begin(); it != r.end(); it++)
			{
				std::string localId = (*it)[0].as<std::string>();
				int nodeId = (*it)[1].as<int>();
				long long meshId = (*it)[2].as<long long>();
				ModelLODTree::Node *node = nodeMap[nodeId];
				if (node == 0)
				{
					reportError("loadModelLODTree", "Inconsistent node mapping", _SEVERITY_ERROR);
					return false;
				}
				node->getMeshBindingMap()[localId] = meshId;
			}

			// Load model bindings
			r = txn.exec("SELECT node_id, model_key FROM ModelLODTreeNodeModels WHERE modellodtree_id=" + txn.quote(modelLODTreeId));
			for (pqxx::result::const_iterator it = r.begin(); it != r.end(); it++)
			{
				int nodeId = (*it)[0].as<int>();
				std::string modelKey = (*it)[1].as<std::string>();
				ModelLODTree::Node *node = nodeMap[nodeId];
				if (node == 0)
				{
					reportError("loadModelLODTree", "Inconsistent node mapping", _SEVERITY_ERROR);
					return false;
				}
				node->setModelId(modelKey);
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	long long KMZ2PgConverter::saveModelLODTree(const ModelLODTree &modelLODTree)
	{
		TextureExporter textureExporter(TextureExporter::getDefaultOptions());
		NMLPackage::ModelLODTree nmlModelLODTree;
		ModelLODTree::NodeMap nodeMap;
		modelLODTree.saveNMLModelLODTree(nmlModelLODTree, nodeMap, textureExporter);
		std::string modelLODTreeString;
		nmlModelLODTree.SerializeToString(&modelLODTreeString);
		std::string modelLODTreeHashString = calculateSHA1(modelLODTreeString);

		// Use tile transaction
		pqxx::work &txn = *mTileTxn;

		// Save model LOD tree
		pqxx::binarystring modelLODTreeData(modelLODTreeString.data(), modelLODTreeString.size());
		pqxx::binarystring modelLODTreeHashData(modelLODTreeHashString.data(), modelLODTreeHashString.size());
		pqxx::result r = txn.exec("SELECT NEXTVAL('ModelLODTrees_seq')");
		long long modelLODTreeId = r[0][0].as<long long>();
		txn.exec("INSERT INTO ModelLODTrees(id, nmlmodellodtree, nmlmodellodtree_sha1hash) VALUES(" + txn.quote(modelLODTreeId) + ", " + txn.quote(modelLODTreeData) + ", " + txn.quote(modelLODTreeHashData) + ")");

		// Save bindings
		for (ModelLODTree::NodeMap::const_iterator nit = nodeMap.begin(); nit != nodeMap.end(); nit++)
		{
			int nodeId = nit->first;
			const ModelLODTree::Node &node = *nit->second;

			// Save texture bindings
			for (ModelLODTree::Node::BindingMap::const_iterator bit = node.getTextureBindingMap().begin(); bit != node.getTextureBindingMap().end(); bit++)
			{
				const std::string &localId = bit->first;
				long long textureId = bit->second;
				txn.exec("INSERT INTO ModelLODTreeNodeTextures(modellodtree_id, node_id, local_id, texture_id, level) VALUES(" + txn.quote(modelLODTreeId) + ", " + txn.quote(nodeId) + ", " + txn.quote(localId) + ", " + txn.quote(textureId) + ", 0)");
			}

			// Save mesh bindings
			for (ModelLODTree::Node::BindingMap::const_iterator bit = node.getMeshBindingMap().begin(); bit != node.getMeshBindingMap().end(); bit++)
			{
				const std::string &localId = bit->first;
				long long meshId = bit->second;
				txn.exec("INSERT INTO ModelLODTreeNodeMeshes(modellodtree_id, node_id, local_id, mesh_id) VALUES(" + txn.quote(modelLODTreeId) + ", " + txn.quote(nodeId) + ", " + txn.quote(localId) + ", " + txn.quote(meshId) + ")");
			}

			// Save model binding
			const std::string &modelKey = nit->second->getModelId();
			txn.exec("INSERT INTO ModelLODTreeNodeModels(modellodtree_id, node_id, model_key) VALUES(" + txn.quote(modelLODTreeId) + ", " + txn.quote(nodeId) + ", " + txn.quote(modelKey) + ")");
		}

		// Done
		return modelLODTreeId;
	}

	//--------------------------------------------------------------------
	bool KMZ2PgConverter::loadModelInfoMap(long long modelLODTreeId, ModelInfoMap &modelInfoMap)
	{
		pqxx::work txn(mDB);
		pqxx::result r = txn.exec("SELECT id, global_id, model_id FROM ModelInfo WHERE modellodtree_id=" + txn.quote(modelLODTreeId));
		for (pqxx::result::const_iterator it = r.begin(); it != r.end(); it++)
		{
			long long id = it[0].as<long long>();
			std::string globalId = it[1].as<std::string>();
			int modelId = it[2].as<int>();
			ModelInfo modelInfo;
			modelInfo.sourceId = id;
			modelInfo.globalId = globalId;
			modelInfoMap[modelId] = modelInfo;
		}
		return true;
	}

	//--------------------------------------------------------------------
	void KMZ2PgConverter::saveModelInfoMap(long long modelLODTreeId, const ModelInfoMap &modelInfoMap)
	{
		// Use tile transaction
		pqxx::work &txn = *mTileTxn;

		// Delete existing duplicate model info
		txn.exec("DELETE FROM ModelInfo WHERE modellodtree_id=" + txn.quote(modelLODTreeId));

		// Save model info
		for (ModelInfoMap::const_iterator it = modelInfoMap.begin(); it != modelInfoMap.end(); it++)
		{
			int modelId = it->first;
			const ModelInfo &modelInfo = it->second;
			if (modelInfo.sourceId != 0)
			{
				txn.exec("INSERT INTO ModelInfo(id, modellodtree_id, global_id, model_id, mappos, groundheight, maxheight, floorplan, folder, vertices, materials, textures) SELECT NEXTVAL('ModelInfo_seq'), " + txn.quote(modelLODTreeId) + ", " + txn.quote(modelInfo.globalId) + ", " + txn.quote(modelId) + ", mappos, groundheight, maxheight, floorplan, folder, vertices, materials, textures FROM ModelInfo WHERE id=" + txn.quote(modelInfo.sourceId));
			}
			else
			{
				std::string folder = mOptions.kmzFilenameAsFolder ? boost::filesystem::path(modelInfo.kmzFile).stem().string() : modelInfo.folder;
				std::string mapPosStr = "POINT(" + boost::lexical_cast<std::string>(modelInfo.mapPos.x) + " " + boost::lexical_cast<std::string>(modelInfo.mapPos.y) + ")";
				std::string floorplanStr = "LINESTRING(";
				for (size_t i = 0; i <= modelInfo.floorplan.size(); i++)
				{
					Vector3 p = internalWebMercatorTransformation(modelInfo.mapPos) * modelInfo.floorplan[i % modelInfo.floorplan.size()];
					if (i > 0)
						floorplanStr += ",";
					floorplanStr += boost::lexical_cast<std::string>(p.x) + " " + boost::lexical_cast<std::string>(p.y);
				}
				floorplanStr += ")";
				txn.exec("INSERT INTO ModelInfo(id, modellodtree_id, global_id, model_id, mappos, groundheight, maxheight, floorplan, folder, vertices, materials, textures) VALUES(NEXTVAL('ModelInfo_seq'), " + txn.quote(modelLODTreeId) + ", " + txn.quote(modelInfo.globalId) + ", " + txn.quote(modelId) + ", ST_GeomFromText('" + mapPosStr + "', 3857), " + txn.quote(modelInfo.mapPos.z) + ", " + txn.quote(modelInfo.maxHeight) + ", ST_MakePolygon(ST_GeomFromText('" + floorplanStr + "', 3857)), " + txn.quote(folder) + ", " + txn.quote(modelInfo.vertices) + ", " + txn.quote(modelInfo.materials) + ", " + txn.quote(modelInfo.textures) + ")");
			}
		}
	}

	//--------------------------------------------------------------------
	long long KMZ2PgConverter::saveMapTile(long long modelLODTreeId, const TileId &tileId, const Vector3 &mapPos, const Bounds3 &localBounds)
	{
		// Use tile transaction
		pqxx::work &txn = *mTileTxn;

		// Delete existing duplicate tile (based on tile id)
		txn.exec("DELETE FROM MapTiles WHERE tile_x=" + txn.quote(tileId.first) + " AND tile_y=" + txn.quote(tileId.second) + " AND tile_size=" + txn.quote(mOptions.tileSize));
		if (localBounds.empty())
			return -1;

		// Insert new tile
		Matrix4 globalTransform = internalWebMercatorTransformation(mapPos);
		Bounds3 mapBounds(globalTransform * localBounds.min, globalTransform * localBounds.max);
		pqxx::result r = txn.exec("SELECT NEXTVAL('MapTiles_seq')");
		long long mapTileId = r[0][0].as<long long>();
		std::string mapPosStr = "POINT(" + boost::lexical_cast<std::string>(mapPos.x) + " " + boost::lexical_cast<std::string>(mapPos.y) + ")";
		std::string mapBoundsStr = "LINESTRING(";
		mapBoundsStr += boost::lexical_cast<std::string>(mapBounds.min.x) + " " + boost::lexical_cast<std::string>(mapBounds.min.y) + ",";
		mapBoundsStr += boost::lexical_cast<std::string>(mapBounds.max.x) + " " + boost::lexical_cast<std::string>(mapBounds.min.y) + ",";
		mapBoundsStr += boost::lexical_cast<std::string>(mapBounds.max.x) + " " + boost::lexical_cast<std::string>(mapBounds.max.y) + ",";
		mapBoundsStr += boost::lexical_cast<std::string>(mapBounds.min.x) + " " + boost::lexical_cast<std::string>(mapBounds.max.y) + ",";
		mapBoundsStr += boost::lexical_cast<std::string>(mapBounds.min.x) + " " + boost::lexical_cast<std::string>(mapBounds.min.y) + ")";
		txn.exec("INSERT INTO MapTiles(id, modellodtree_id, tile_x, tile_y, tile_size, mappos, groundheight, mapbounds) VALUES(" + txn.quote(mapTileId) + ", " + txn.quote(modelLODTreeId) + ", " + txn.quote(tileId.first) + ", " + txn.quote(tileId.second) + ", " + txn.quote(mOptions.tileSize) + ", ST_GeomFromText('" + mapPosStr + "', 3857), " + txn.quote(mapPos.z) + ", ST_MakePolygon(ST_GeomFromText('" + mapBoundsStr + "', 3857)))");

		// Done
		return mapTileId;
	}

	//--------------------------------------------------------------------
	bool KMZ2PgConverter::loadModel(const std::string &modelKey, Model &model)
	{
		pqxx::work txn(mDB);
		pqxx::result r = txn.exec("SELECT id, nmlmodel, LENGTH(nmlmodel) FROM Models WHERE model_key=" + txn.quote(modelKey));
		if (!r.empty())
		{
			pqxx::binarystring nmlModelData(r[0][1]);
			NMLPackage::Model nmlModel;
			nmlModel.ParseFromArray(nmlModelData.data(), nmlModelData.size());
			return model.loadNMLModel(nmlModel);
		}
		return false;
	}

	//--------------------------------------------------------------------
	long long KMZ2PgConverter::saveModel(const std::string &modelKey, const Model &model)
	{
		TextureExporter::Options options;
		options.generateMipMaps = false;
		options.RGBFormat = TextureExporter::Options::PNG;
		options.RGBAFormat = TextureExporter::Options::PNG;
		TextureExporter textureExporter(options);
		NMLPackage::Model nmlModel;
		model.saveNMLModel(modelKey, nmlModel, textureExporter);

		std::string modelString;
		nmlModel.SerializeToString(&modelString);
		std::string modelHashString = calculateSHA1(modelString);

		// Start new transaction
		pqxx::work txn(mDB);

		// Delete old LOD tree and insert new
		txn.exec("DELETE FROM Models WHERE model_key=" + txn.quote(modelKey));
		pqxx::binarystring modelData(modelString.data(), modelString.size());
		pqxx::binarystring modelHashData(modelHashString.data(), modelHashString.size());
		pqxx::result r = txn.exec("SELECT NEXTVAL('Models_seq')");
		long long id = r[0][0].as<long long>();
		txn.exec("INSERT INTO Models(id, model_key, nmlmodel, nmlmodel_sha1hash) VALUES(" + txn.quote(id) + ", " + txn.quote(modelKey) + ", " + txn.quote(modelData) + ", " + txn.quote(modelHashData) + ")");

		// Success, commit changes to db
		txn.commit();
		return id;
	}

	//--------------------------------------------------------------------
	long long KMZ2PgConverter::saveMesh(const std::string &meshId, const Mesh &mesh)
	{
		NMLPackage::Mesh nmlMesh;
		mesh.saveNMLMesh(meshId, nmlMesh);
		std::string meshString;
		nmlMesh.SerializeToString(&meshString);
		std::string meshHashString = calculateSHA1(meshString);

		// Start new transaction
		pqxx::work txn(mDB);

		// Save NML mesh if not saved already
		long long id = -1;
		pqxx::binarystring meshData(meshString.data(), meshString.size());
		pqxx::binarystring meshHashData(meshHashString.data(), meshHashString.size());
		pqxx::result r = txn.exec("SELECT id FROM Meshes WHERE nmlmesh_sha1hash=" + txn.quote(meshHashData) + " AND nmlmesh=" + txn.quote(meshData));
		if (!r.empty())
		{
			id = r[0][0].as<long long>();
		}
		else
		{
			pqxx::result r = txn.exec("SELECT NEXTVAL('Meshes_seq')");
			id = r[0][0].as<long long>();
			txn.exec("INSERT INTO Meshes(id, nmlmesh, nmlmesh_sha1hash) VALUES(" + txn.quote(id) + ", " + txn.quote(meshData) + ", " + txn.quote(meshHashData) + ")");
		}

		// Success, commit changes to db
		txn.commit();
		return id;
	}

	//--------------------------------------------------------------------
	long long KMZ2PgConverter::saveTexture(const std::string &textureId, const Texture &texture)
	{
		TextureExporter textureExporter(TextureExporter::getDefaultOptions());
		NMLPackage::Texture nmlTexture;
		texture.saveNMLTexture(textureId, nmlTexture, textureExporter);
		std::string textureString;
		nmlTexture.SerializeToString(&textureString);
		std::string textureHashString = calculateSHA1(textureString);

		// Start new transaction
		pqxx::work txn(mDB);

		// Save NML texture if not saved already
		long long id = -1;
		pqxx::binarystring textureData(textureString.data(), textureString.size());
		pqxx::binarystring textureHashData(textureHashString.data(), textureHashString.size());
		pqxx::result r = txn.exec("SELECT id FROM Textures WHERE nmltexture_sha1hash=" + txn.quote(textureHashData) + " AND nmltexture=" + txn.quote(textureData) + " AND level=0");
		if (!r.empty())
		{
			id = r[0][0].as<long long>();
		}
		else
		{
			pqxx::result r = txn.exec("SELECT NEXTVAL('Textures_seq')");
			id = r[0][0].as<long long>();
			txn.exec("INSERT INTO Textures(id, level, nmltexture, nmltexture_sha1hash) VALUES(" + txn.quote(id) + ", 0, " + txn.quote(textureData) + ", " + txn.quote(textureHashData) + ")");
		}

		// Success, commit changes to db
		txn.commit();
		return id;
	}

} // namespace KMZ2Db
