/*
Copyright (c) 2012 Nutiteq

This file is part of KMZ2Db.
*/

#include "KMZModelLODTreeBuilder.h"
#include "KMZURILoader.h"
#include "DAEPreprocessor.h"
#include "TextureExporter.h"
#include "DAELoader.h"
#include "WGS84Utils.h"
#include "Utils.h"

#include <boost/filesystem.hpp>

namespace KMZ2Db
{

	//--------------------------------------------------------------------
	KMZModelLODTreeBuilder::Options KMZModelLODTreeBuilder::sDefaultOptions;

	//--------------------------------------------------------------------
	void KMZModelLODTreeBuilder::buildModelLODTree(ModelLODTree &newModelLODTree)
	{
		reportProgress("buildModelLODTree", "Building model LOD tree index");

		// Create list from old unchanged models
		std::list<ModelId> modelList;
		SpatialModelTree::ObjectBoundsPairList spatialObjList;
		for (ModelInfoMap::const_iterator it = mModelInfoMap.begin(); it != mModelInfoMap.end(); it++)
		{
			ModelId modelId = it->first;
			if (mChangedModelIds.count(modelId) != 0)
				continue;
			std::string modelKey = it->second.globalId;
			Model model;
			if (!mWriter.loadModel(modelKey, model))
			{
				reportError("buildModelLODTree", "Could not load existing model from database, result will be corrupt", _SEVERITY_ERROR);
				continue;
			}

			modelList.push_back(modelId);
			spatialObjList.push_back(std::make_pair(&modelList.back(), model.calculateBounds(Matrix4::IDENTITY)));
		}

		// Add new models
		for (ModelInfoMap::iterator it = mModelInfoMap.begin(); it != mModelInfoMap.end(); it++)
		{
			ModelId modelId = it->first;
			ModelInfo &modelInfo = it->second;
			if (modelInfo.kmzFile.empty())
				continue;
			Matrix4 transform = internalWebMercatorTransformation(mMapPos).inverse() * internalWebMercatorTransformation(modelInfo.mapPos);
			Model model;
			if (!loadDAEModel(modelInfo, model, transform, false))
				continue;

			modelList.push_back(modelId);
			spatialObjList.push_back(std::make_pair(&modelList.back(), model.calculateBounds(Matrix4::IDENTITY)));
		}

		// Create model tree
		SpatialModelTree spatialTree;
		spatialTree.create(spatialObjList, mOptions.finalNodeSize, mOptions.initialNodeSize);
		SpatialModelTree::Node *spatialTreeRoot = spatialTree.getRoot();

		// Build ids for spatial tree nodes
		NodeIdMap nodeIdMap;
		buildNodeIdMap(spatialTreeRoot, nodeIdMap);

		// Build node cache from old tree
		NodeCache nodeCache;
		buildNodeCache(mOldModelLODTree.getRoot(), nodeCache);

		// Start processing from the root
		reportProgress("buildModelLODTree", "Building model LOD tree content");
		createNode(newModelLODTree.getRoot(), spatialTreeRoot, nodeCache, nodeIdMap);
	}

	//--------------------------------------------------------------------
	std::string KMZModelLODTreeBuilder::buildNodeIdMap(const SpatialModelTree::Node *spatialTreeNode, NodeIdMap &nodeIdMap) const
	{
		if (spatialTreeNode == 0)
			return "";

		// We assign unique id for each spatial tree node, based on its children, spatial coordinates and models it contains.
		// Global model ids are encoded between ";" characters, so that it is easy to detect whether a model with id n affects the node.
		std::stringstream stream;
		stream << "(;";
		for (SpatialModelTree::ObjectList::const_iterator it = spatialTreeNode->getObjects().begin(); it != spatialTreeNode->getObjects().end(); it++)
		{
			ModelInfoMap::const_iterator mit = mModelInfoMap.find(**it);
			if (mit != mModelInfoMap.end())
				stream << mit->second.globalId << ";";
		}
		Bounds3 nodeBounds = spatialTreeNode->getNodeBounds();
		stream << nodeBounds.size().x << "," << nodeBounds.size().y << "," << nodeBounds.size().z << "";
		for (int n = 0; n < SpatialModelTree::Node::CHILDREN; n++)
		{
			if (spatialTreeNode->getChild(n) != 0)
			{
				stream << buildNodeIdMap(spatialTreeNode->getChild(n), nodeIdMap);
			}
		}
		stream << ")";
		std::string id = stream.str();
		nodeIdMap[spatialTreeNode] = id;
		return id;
	}

	//--------------------------------------------------------------------
	bool KMZModelLODTreeBuilder::buildNodeCache(const ModelLODTree::Node &node, NodeCache &nodeCache) const
	{
		// What we do here is hacky - we assume that model ids inside LOD tree contain references to original models.
		// We try all ids of changed models against current node id.
		bool changed = false;
		for (ModelIdSet::const_iterator it = mChangedModelIds.begin(); it != mChangedModelIds.end(); it++)
		{
			std::stringstream stream;
			ModelInfoMap::const_iterator mit = mModelInfoMap.find(*it);
			if (mit != mModelInfoMap.end())
			{
				stream << ";" << mit->second.globalId << ";";
				if (node.getModelId().find(stream.str()) != std::string::npos)
					changed = true;
			}
		}
		for (int i = 0; i < node.getChildCount(); i++)
		{
			if (buildNodeCache(node.getChild(i), nodeCache))
				changed = true;
		}
		if (!changed)
			nodeCache[node.getModelId()] = &node;
		return changed;
	}

	//--------------------------------------------------------------------
	void KMZModelLODTreeBuilder::createNode(ModelLODTree::Node &node, SpatialModelTree::Node *spatialTreeNode, NodeCache &nodeCache, NodeIdMap &nodeIdMap)
	{
		if (spatialTreeNode == 0)
			return;

		// Check if this node needs to be rebuilt or not
		std::string nodeId = nodeIdMap[spatialTreeNode];
		if (nodeCache.find(nodeId) != nodeCache.end())
		{
			reportProgress("createNode", "Loading prebuilt models from cache");
			node = *nodeCache[nodeId];
		}
		else
		{
			node.setModelId(nodeId);
			Model &model = node.getModel();

			// Process children first
			for (int n = 0; n < SpatialModelTree::Node::CHILDREN; n++)
			{
				if (spatialTreeNode->getChild(n) == 0)
					continue;
				createNode(node.addChild(), spatialTreeNode->getChild(n), nodeCache, nodeIdMap);
			}

			// Load models under this node
			reportProgress("createNode", "Loading node models");
			loadNodeModels(node, spatialTreeNode, nodeIdMap);

			// Optimize and combine models. Do here only 'progressive' optimizations. Full optimizations are done after combining.
			reportProgress("createNode", "Optimizing model, pass 1");
			model.stripPointsLines();
			model.flatten(mOptions.maxExtraFlattenVertices, mOptions.maxSubmeshVertices);
			model.optimizeForBoundsSize(Matrix4::IDENTITY, spatialTreeNode->getNodeBounds().size(), mOptions.minSubmeshSize, mOptions.targetScreenSize, mOptions.maxSingleTextureSize);
			model.mergeMaterials();
			model.mergeSubmeshes();
			model.cleanup();

			// Cache model
			mWriter.saveModel(nodeId, model);

			// Apply lighting
			if (mOptions.lightingModel != 0)
			{
				reportProgress("createNode", "Generating lighting for model");
				Matrix4 globalTransform = internalWebMercatorTransformation(mMapPos);
				model.applyLightingModel(BasicLightingModel(mOptions.lightingModel), globalTransform);
			}

			// Optimize combined model
			reportProgress("createNode", "Optimizing model, pass 2");
			model.generateVertexColors(mOptions.maxMergedColorVertices);
			model.mergeMaterials();
			model.mergeSubmeshes();
			model.cleanup();
			model.resizeTexturesToPOT(mOptions.maxSingleTextureSize);
			if (mOptions.maxAtlasTextureSize > 0)
			{
				reportProgress("createNode", "Generating texture atlas for model");
				model.generateTextureAtlas(mOptions.maxSingleTextureSize, mOptions.maxAtlasTextureSize, mOptions.squareTextureAtlas, mOptions.maxSubmeshVertices, mOptions.maxRepeatedTexturePixels);
			}
			model.cleanup();
			model.mergeMaterials();
			model.mergeSubmeshes();
			model.cleanup();
			model.canonizeNames();

			// Store bounds. Offset node bounds a bit to be at the center of the model
			Bounds3 nodeBounds = spatialTreeNode->getNodeBounds();
			Bounds3 modelBounds = model.calculateBounds(Matrix4::IDENTITY);
			Vector3 delta = modelBounds.center() - nodeBounds.center();
			node.setModelBounds(modelBounds);
			node.setNodeBounds(Bounds3(nodeBounds.min + delta, nodeBounds.max + delta));

			// Detach and save meshes and textures
			reportProgress("createNode", "Saving NML assets");
			saveNodeAssets(node);
		}
	}

	//--------------------------------------------------------------------
	void KMZModelLODTreeBuilder::loadNodeModels(ModelLODTree::Node &node, SpatialModelTree::Node *spatialTreeNode, NodeIdMap &nodeIdMap)
	{
		// Load all models under this node. Also translate the objects to the center of tree so that objects can be concatenated later
		for (SpatialModelTree::ObjectList::iterator it = spatialTreeNode->getObjects().begin(); it != spatialTreeNode->getObjects().end(); it++)
		{
			ModelId modelId = **it;
			ModelInfo &modelInfo = mModelInfoMap[modelId];
			std::string modelKey = modelInfo.globalId;
			Model model;
			if (modelInfo.kmzFile.empty())
			{
				if (!mWriter.loadModel(modelKey, model))
				{
					reportError("loadNodeModels", "Could not load existing model from database, result will be corrupt", _SEVERITY_ERROR);
					continue;
				}
			}
			else
			{
				Matrix4 transform = internalWebMercatorTransformation(mMapPos).inverse() * internalWebMercatorTransformation(modelInfo.mapPos);
				if (!loadDAEModel(modelInfo, model, transform, true))
				{
					continue;
				}
				model.setVertexIds(modelId);
				model.resizeTexturesToPOT(mOptions.maxSingleTextureSize);
				mWriter.saveModel(modelKey, model);
			}
			node.getModel().concat(model);
		}

		// Add models from child nodes
		for (int n = 0; n < SpatialModelTree::Node::CHILDREN; n++)
		{
			if (spatialTreeNode->getChild(n) == 0)
				continue;

			std::string nodeId = nodeIdMap[spatialTreeNode->getChild(n)];
			Model model;
			if (!mWriter.loadModel(nodeId, model))
			{
				reportError("loadNodeModels", "Could not load existing model from database, result will be corrupt", _SEVERITY_ERROR);
				continue;
			}
			node.getModel().concat(model);
		}
	}

	//--------------------------------------------------------------------
	void KMZModelLODTreeBuilder::saveNodeAssets(ModelLODTree::Node &node)
	{
		Model &model = node.getModel();

		// Calculate memory footprint before detaching meshes/textures from the model
		TextureExporter textureExporter(TextureExporter::getDefaultOptions());
		model.calculateMemoryFootprint(textureExporter);

		// Save meshes
		for (Model::MeshMap::const_iterator it = model.getMeshMap().begin(); it != model.getMeshMap().end(); it++)
		{
			node.getMeshBindingMap()[it->first] = mWriter.saveMesh(it->first, it->second);
		}
		model.getMeshMap().clear();

		// Save textures
		for (Model::TextureMap::const_iterator it = model.getTextureMap().begin(); it != model.getTextureMap().end(); it++)
		{
			node.getTextureBindingMap()[it->first] = mWriter.saveTexture(it->first, it->second);
		}
		model.getTextureMap().clear();
	}

	//--------------------------------------------------------------------
	bool KMZModelLODTreeBuilder::loadDAEModel(ModelInfo &modelInfo, Model &model, const Matrix4 &transform, bool loadTextures)
	{
		reportProgress("loadDAEModel", (loadTextures ? "Loading DAE model " : "Preloading DAE model ") + modelInfo.kmzFile + "#" + modelInfo.modelUri.originalStr());

		// Generate name for preprocessed file
		boost::filesystem::path preprocessedFile = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path("KMZ-%%%%-%%%%-%%%%-%%%%.dae");

		// Preprocess DAE
		KMZURILoader uriLoader(modelInfo.kmzFile, modelInfo.modelUri.getPathDir());
		DAEPreprocessor preprocessor(modelInfo.modelUri.getPathFile(), uriLoader, preprocessedFile.string());
		if (!preprocessor.preprocess())
		{
			boost::filesystem::remove(preprocessedFile);
			reportError("loadDAEModel", "Preprocessing failed", _SEVERITY_ERROR);
			return false;
		}

		// Load model
		DAELoader loader(COLLADABU::URI::nativePathToUri(preprocessedFile.string()), uriLoader, transform * modelInfo.localTransform, loadTextures, model);
		if (!loader.load())
		{
			boost::filesystem::remove(preprocessedFile);
			reportError("loadDAEModel", "Could not load DAE model: " + modelInfo.modelUri.getURIString(), _SEVERITY_ERROR);
			return false;
		}

		// Remove preprocessed file
		boost::filesystem::remove(preprocessedFile);

		// Check model complexity
		float minHeight, maxHeight;
		modelInfo.floorplan = model.calculateFloorplan(minHeight, maxHeight);
		modelInfo.maxHeight = maxHeight;
		modelInfo.vertices = model.getVertexCount();
		modelInfo.materials = model.getMaterialCount();
		modelInfo.textures = model.getTextureMap().size();

		if (mOptions.maxModelVertices > 0)
		{
			if (model.getVertexCount() > mOptions.maxModelVertices)
			{
				reportError("loadDAEModel", "Model " + modelInfo.modelUri.getURIString() + " too complex, contains " + COLLADABU::Utils::toString(model.getVertexCount()) + " vertices", _SEVERITY_ERROR);
				return false;
			}
		}

		// All ok
		return true;
	}

} // namespace KMZ2Db
