/*
Copyright (c) 2012 Nutiteq

This file is part of KMZ2Db.
*/

#include "KMZ2DbConverterBase.h"
#include "Utils.h"
#include "WGS84Utils.h"

#include <boost/filesystem.hpp>


namespace KMZ2Db
{

	//--------------------------------------------------------------------
	KMZ2DbConverterBase::Options KMZ2DbConverterBase::sDefaultOptions;

	//--------------------------------------------------------------------
	Vector3 KMZ2DbConverterBase::getTilePos(const TileId &tileId) const
	{
		return Vector3((tileId.first + 0.5) * mOptions.tileSize, (tileId.second + 0.5) * mOptions.tileSize, 0);
	}

	//--------------------------------------------------------------------
	KMZ2DbConverterBase::TileId KMZ2DbConverterBase::getTileId(const Vector3 &mapPos) const
	{
		int x = (int) mapPos.x / mOptions.tileSize;
		int y = (int) mapPos.y / mOptions.tileSize;
		return std::make_pair(x, y);
	}

	//--------------------------------------------------------------------
	void KMZ2DbConverterBase::processKML(std::list<ModelInfo> &modelInfos, const pugi::xml_node &root_node, KMZURILoader &uriLoader)
	{
		pugi::xpath_node_set models = pugi::xpath_query("kml//Placemark/Model").evaluate_node_set(root_node);
		for (pugi::xpath_node_set::const_iterator models_it = models.begin(); models_it != models.end(); ++models_it)
		{
			pugi::xml_node model_node = (*models_it).node();

			std::string folder;
			pugi::xpath_node_set folders = pugi::xpath_query("../../name").evaluate_node_set(model_node);
			if (!folders.empty())
			{
				folder = folders.first().node().child_value();
			}

			pugi::xml_node altmode_node = model_node.child("altitudeMode");
			if (!altmode_node.empty())
			{
				std::string altmode = altmode_node.child_value();
				if (altmode != "clampToGround" && altmode != "relativeToGround")
					reportError("processKML", "Unsupported altituteMode: " + altmode, SEVERITY_WARNING);
			}
			pugi::xml_node gx_altmode_node = model_node.child("gx:altitudeMode");
			if (!gx_altmode_node.empty())
			{
				std::string gx_altmode = gx_altmode_node.child_value();
				reportError("processKML", "Unsupported gx:altituteMode: " + gx_altmode, SEVERITY_WARNING);
			}

			Vector3 wgs84Location(0, 0, 0);
			Matrix4 transform(Matrix4::IDENTITY);
			pugi::xml_node location_node = model_node.child("Location");
			if (!location_node.empty())
			{
				double lat = atof(location_node.child_value("latitude"));
				double lng = atof(location_node.child_value("longitude"));
				double alt = atof(location_node.child_value("altitude"));
				wgs84Location = Vector3(lng, lat, mOptions.ignoreHeight ? 0 : alt);
			}
			pugi::xml_node orientation_node = model_node.child("Orientation");
			if (!orientation_node.empty())
			{
				double head = atof(orientation_node.child_value("heading"));
				double tilt = atof(orientation_node.child_value("tilt"));
				double roll = atof(orientation_node.child_value("roll"));
				COLLADABU::Math::Matrix3 rotHead; rotHead.fromAxisAngle(Vector3(0, 0, 1), -COLLADABU::Math::Utils::DEG_TO_RAD * head);
				COLLADABU::Math::Matrix3 rotTilt; rotTilt.fromAxisAngle(Vector3(1, 0, 0), -COLLADABU::Math::Utils::DEG_TO_RAD * tilt);
				COLLADABU::Math::Matrix3 rotRoll; rotRoll.fromAxisAngle(Vector3(0, 1, 0), -COLLADABU::Math::Utils::DEG_TO_RAD * roll);
				transform = transform * rotHead * rotTilt * rotRoll;
			}
			pugi::xml_node scale_node = model_node.child("Scale");
			if (!scale_node.empty())
			{
				double x = atof(scale_node.child_value("x"));
				double y = atof(scale_node.child_value("y"));
				double z = atof(scale_node.child_value("z"));
				transform = transform * Matrix4::getScale(x, y, z);
			}

			pugi::xml_node link_node = model_node.child("Link");
			if (!link_node.empty())
			{
				COLLADABU::URI modelUri(URILoader::stripUri(link_node.child_value("href")));

				// Create model reference and store it
				std::string globalId = uriLoader.getKmzFile() + "#" + modelUri.getURIString(); // folder
				ModelInfo modelInfo;
				modelInfo.globalId = globalId;
				modelInfo.kmzFile = uriLoader.getKmzFile();
				modelInfo.modelUri = modelUri;
				modelInfo.folder = folder;
				modelInfo.mapPos = wgs84ToWebMercator(wgs84Location);
				modelInfo.localTransform = transform;
				modelInfos.push_back(modelInfo);
			}
			else
			{
				reportError("processKML", "No link for model, ignoring", SEVERITY_WARNING);
			}
		}
	}

} // namespace KMZ2Db
