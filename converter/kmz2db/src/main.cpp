/*
Copyright (c) 2012 Nutiteq

This file is part of KMZ2Db.
*/

#include <pqxx/pqxx>

#include "Prerequisites.h"
#include "DAELoader.h"
#include "TextureExporter.h"
#include "COLLADABUURI.h"
#include "KMZ2DbConverterBase.h"
#include "KMZ2SqliteConverter.h"
#include "KMZ2PgConverter.h"
#include "LicenseManager.h"
#include "../extra/boost_fsutils.h"

#include <iostream>

#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/bind.hpp>

#if defined(WIN32) && !defined(NDEBUG)
#	include <crtdbg.h> 
#endif

namespace po = boost::program_options;
namespace fs = boost::filesystem;

// static const std::string LICENSE_PUBLIC_KEY = "MIIBtzCCASwGByqGSM44BAEwggEfAoGBAOHD5Oa9gZ/b6mJKf5xyVUAtEGzTD+ynSSxp/jLwdBiCq1MDKCDFrSJjCCp2V84SGcPzS+YRMXkPp6zAH8/8hXyqQrvocp+FJ0HZ51MCSesKzLDwgHOfat6BKQD/bzxX5LDKj84+1gzMcMA55JioVVG/cFoltKEFtS16zoLbloexAhUA8yRmkSLicTd2RYHuCtpNLOHuyIsCgYEAm+ZQpfx9tVtNh1eYqaWbVO9xNNJbZA2MpLbt4BYmUWtLdNq4I814ES/aWRlWEf2lXmLIdlAXWaRdvJ3Co66o2ZDeHWPqyNqAUpnf+OQeQu3DzuWTm5wx0bcwwZAhas7MyXGY6rc88TY+//H5l/QBG0P0J7a3V1cwACqZ4nQarkgDgYQAAoGAHf2jI4AsnPnYOQBzcrbU15dk7ygreVf4+7NpS0S8e+n/oHwS1lpUdstIx9KyqCrGB7G6maZkhztBu2/5he6/H3GnzCyvFq2iQZ7WVe6fkk86bW2blQki6Adj1dUCiFei/zhDd4wqFldfJaMBDVpXoXmMmoqCCFJ4eacPwZhBHGM=";
static const std::string LICENSE_PUBLIC_KEY = "MIIBtjCCASsGByqGSM44BAEwggEeAoGBAIlFs9OqwdM4lcfhXQVCyYDzPc6rO6sbtlKuEYa/uSzeuk1tvYcGybBwB3mYLYvOh0Qd/65TUO6rYI9xTAHK0HtjGj+XLaNDiP4eEvaVfg2fhX26XDdaAGXfKWKCHKiZ0cWLnBtTap2woVvSt6TLxcxrDnwG9mrL3Lt06rflF4oJAhUAyWhMdWcGzgq37TUJcKauhX7OEHcCgYAsXe5Q9JRA3yQsAGijSifQS8Pth1FfF69dU7VUeYD55VZ5x4UVAQP+wg7K5e6RQgJpaL1R4duVkFRgr3RuTwevv1szi/3ENiIQW4vNhPxc/sN+Y2YdlNnguOhV2LEcYmneX+F5cb2UXQZBBDhVgEtU7c9bxyA6tSwKuC70EqfZSQOBhAACgYAzp7pUQ1XxR79N8NbaB1PUPE7n1bZdFLF1QsK9thjL4Q3dbg6fub3qZfSPL286nZjfD+15oya1ORFKwSplindHNx6vSL+AmNhI4GYdyIasPnLAqCV9rIMTgQ+RfmyWDvSLxSDVqWqA83M6m/FFuWMKWKqMOEueJZTwrr/HNNTk+w==";

std::vector<COLLADABU::URI> resolveUris(const std::vector<std::string> &inputFiles)
{
	std::vector<COLLADABU::URI> resolvedUris;
	for (size_t i = 0; i < inputFiles.size(); i++)
	{
		const std::string & inputFile = inputFiles[i];
		std::vector<std::string> resolvedFiles = boost_fsutils::find_files(inputFile);
		if (resolvedFiles.empty())
		{
			throw std::runtime_error("Could not find " + inputFile);
		}
		for (size_t j = 0; j < resolvedFiles.size(); j++)
		{
			boost::filesystem::path finalPath = boost_fsutils::naive_uncomplete(resolvedFiles[j], fs::current_path());
			resolvedUris.push_back(COLLADABU::URI::nativePathToUri(finalPath.string()));
		}
	}
	return resolvedUris;
}

int kmz2sqlite(const std::vector<KMZ2Db::KMZ2DbConverterBase::Command> &commands, const std::string &dbFile, bool freeze)
{
	boost::shared_ptr<sqlite3pp::database> db(new sqlite3pp::database(dbFile.c_str()));
	db->execute("PRAGMA encoding='UTF-8'");
	KMZ2Db::KMZ2SqliteConverter::initializeDb(*db);

	KMZ2Db::KMZ2SqliteConverter converter(KMZ2Db::KMZ2SqliteConverter::getDefaultOptions(), commands, *db);
	if (!converter.convert())
	{
		std::cerr << "Conversion failed!" << std::endl;
		return 1;
	}
	if (freeze)
	{
		std::cout << "Freezing database, updates are no longer possible" << std::endl;
		KMZ2Db::KMZ2SqliteConverter::freezeDb(*db);
	}
	return 0;
}

int kmz2pg(const std::vector<KMZ2Db::KMZ2DbConverterBase::Command> &commands, const std::string &connectionInfo, bool freeze)
{
	boost::shared_ptr<pqxx::connection> db(new pqxx::connection(connectionInfo));
	KMZ2Db::KMZ2PgConverter::initializeDb(*db);

	KMZ2Db::KMZ2PgConverter converter(KMZ2Db::KMZ2PgConverter::getDefaultOptions(), commands, *db);
	if (!converter.convert())
	{
		std::cerr << "Conversion failed!" << std::endl;
		return 1;
	}
	if (freeze)
	{
		std::cout << "Freezing database, updates are no longer possible" << std::endl;
		KMZ2Db::KMZ2PgConverter::freezeDb(*db);
	}
	return 0;
}

int main(int argc, char* argv[]) 
{
	po::options_description cmd_options("Commands");
	cmd_options.add_options()
		("add-all", "add all models from input files to database")
		("update-all", "update all models from input files in database")
		("add", po::value<std::vector<std::string> >()->composing(), "add model with given ID from input file to database")
		("update", po::value<std::vector<std::string> >()->composing(), "update model with given ID from input file in database")
		("remove", po::value<std::vector<std::string> >()->composing(), "remove model with given ID from database")
		;

	po::options_description db_options("Database options");
	db_options.add_options()
		("kmz2pg", po::value<std::string>(), "use Postgres database with given connection info for output")
		("kmz2sqlite", po::value<std::string>(), "use given Sqlite database file for output")
		("freeze", "freeze database")
		;

	po::options_description options;
	options.add(cmd_options).add(db_options);
	options.add(KMZ2Db::KMZ2DbConverterBase::Options::getDescription());
	options.add(KMZ2Db::KMZModelLODTreeBuilder::Options::getDescription());
	options.add(KMZ2Db::TextureExporter::Options::getDescription());

#ifdef REQUIRE_LICENSE
	options.add(Nutiteq::LicenseManager::Options::getDescription());
#endif

	po::options_description hidden("Hidden options");
	hidden.add_options()
		("input-files", po::value<std::vector<std::string> >(), "input files")
		;

	po::options_description cmdline_options;
	cmdline_options.add(options).add(hidden);

	po::positional_options_description file_options;
	file_options.add("input-files", -1);

	try
	{
		po::variables_map vm;
		store(po::command_line_parser(argc, argv).
			options(cmdline_options).positional(file_options).run(), vm);
		notify(vm);

		if (!vm.count("kmz2pg") && !vm.count("kmz2sqlite"))
		{
			std::cerr << "Usage: kmz2db [options] [command] [kmzfile1] [kmzfile2] ..." << std::endl;
			std::cerr << std::endl << options << std::endl;
			std::cerr << "Either --kmz2pg or --kmz2sqlite option must be specified!" << std::endl;
			return -1;
		}

#ifdef REQUIRE_LICENSE
		Nutiteq::LicenseManager::Options::verifyLicense("kmz2db", LICENSE_PUBLIC_KEY, vm);
#endif

		std::vector<std::string> inputFiles;
		if (vm.count("input-files") > 0)
			inputFiles = vm["input-files"].as<std::vector<std::string> >();

		int count = (vm.count("add-all") ? 1 : 0) + (vm.count("update-all") ? 1 : 0) + (vm.count("add") ? 1 : 0) + (vm.count("update") ? 1 : 0) + (vm.count("remove") ? 1 : 0);
		if (count > 1)
		{
			std::cerr << "Only one of --all-all, --update-all, --add, --update or --remove can be specified." << std::endl;
			return -1;
		}

		std::vector<KMZ2Db::KMZ2DbConverterBase::Command> commands;
		if (vm.count("add-all") || vm.count("update-all"))
		{
			std::vector<COLLADABU::URI> resolvedUris = resolveUris(inputFiles);
			for (size_t i = 0; i < resolvedUris.size(); i++)
			{
				KMZ2Db::KMZ2DbConverterBase::Command command(vm.count("add-all") ? KMZ2Db::KMZ2DbConverterBase::Command::ADD : KMZ2Db::KMZ2DbConverterBase::Command::UPDATE);
				command.kmzFile = resolvedUris[i];
				commands.push_back(command);
			}
		}
		if (vm.count("add") || vm.count("update"))
		{
			std::vector<COLLADABU::URI> resolvedUris = resolveUris(inputFiles);
			for (size_t i = 0; i < resolvedUris.size(); i++)
			{
				std::vector<std::string> globalIds = (vm.count("add") ? vm["add"] : vm["update"]).as<std::vector<std::string> >(); 
				for (size_t j = 0; j < globalIds.size(); j++)
				{
					KMZ2Db::KMZ2DbConverterBase::Command command(vm.count("add") ? KMZ2Db::KMZ2DbConverterBase::Command::ADD : KMZ2Db::KMZ2DbConverterBase::Command::UPDATE);
					command.globalId = globalIds[j];
					command.kmzFile = resolvedUris[i];
					commands.push_back(command);
				}
			}
		}
		if (vm.count("remove"))
		{
			std::vector<std::string> globalIds = vm["remove"].as<std::vector<std::string> >(); 
			for (size_t j = 0; j < globalIds.size(); j++)
			{
				KMZ2Db::KMZ2DbConverterBase::Command command(KMZ2Db::KMZ2DbConverterBase::Command::REMOVE);
				command.globalId = globalIds[j];
				commands.push_back(command);
			}
		}

		bool freeze = vm.count("freeze");
		if (commands.empty() && !freeze)
		{
			std::cerr << "Nothing to do." << std::endl;
			return -1;
		}

		KMZ2Db::KMZ2DbConverterBase::getDefaultOptions().setVariables(vm);
		KMZ2Db::KMZModelLODTreeBuilder::getDefaultOptions().setVariables(vm);
		KMZ2Db::TextureExporter::getDefaultOptions().setVariables(vm);

		if (vm.count("kmz2pg"))
		{
			std::string connectionInfo = vm["kmz2pg"].as<std::string>();
			return kmz2pg(commands, connectionInfo, freeze);
		}
		if (vm.count("kmz2sqlite"))
		{
			std::string dbFileName = vm["kmz2sqlite"].as<std::string>();
			return kmz2sqlite(commands, dbFileName, freeze);
		}
	}
	catch (const std::exception &ex)
	{
		std::cerr << ex.what() << std::endl;
	}
	return -1;
}
