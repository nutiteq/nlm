/*
Copyright (c) 2012 Nutiteq

This file is part of KMZ2Db.
*/

#ifndef __KMZ2DB_KMZ2PGCONVERTER_H__
#define __KMZ2DB_KMZ2PGCONVERTER_H__

#include "Prerequisites.h"
#include "Processor.h"
#include "KMZURILoader.h"
#include "DAELoader.h"
#include "KMZModelLODTreeBuilder.h"
#include "KMZ2DbConverterBase.h"

#include "COLLADABUURI.h"

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>

#include <pugixml.hpp>

#include <pqxx/pqxx>


namespace KMZ2Db
{
	using namespace NMLFramework;

	class KMZ2PgConverter : public KMZModelLODTreeBuilder::IWriter, public KMZ2DbConverterBase
	{

	public:

		/** Default constructor */
		KMZ2PgConverter(const Options &options, const std::vector<Command>& commands, pqxx::connection &db) : KMZ2DbConverterBase(options, commands), mDB(db) { }

		/** Convert KMZ file given by inputUri to multiple NMLPackage classes and store the result in spatialite database. */
		bool convert();

		/** Initialize db */
		static bool initializeDb(pqxx::connection &db);

		/** Freeze db */
		static bool freezeDb(pqxx::connection &db);

	private:

		pqxx::connection &mDB;
		boost::shared_ptr<pqxx::work> mTileTxn;
		ModelLODTreeBuilderMap mModelLODTreeBuilderMap;

		TileId findTileId(const std::string &globalId) const;
		ModelId findModelId(const std::string &globalId) const;
		ModelLODTreeBuilderPtr getModelLODTreeBuilder(const TileId &tileId);

		// Local database interface
		bool loadModelLODTree(long long modelLODTreeId, ModelLODTree &modelLODTree);
		long long saveModelLODTree(const ModelLODTree &modelLODTree);
		bool loadModelInfoMap(long long modelLODTreeId, ModelInfoMap &modelInfoMap);
		void saveModelInfoMap(long long modelLODTreeId, const ModelInfoMap &modelInfoMap);
		long long saveMapTile(long long modelLODTreeId, const TileId &tileId, const Vector3 &mapPos, const Bounds3 &localBounds);

		// KMZModelLODTreeBuilder::IWriter interface
		virtual bool loadModel(const std::string &modelKey, Model &model);
		virtual long long saveModel(const std::string &modelKey, const Model &model);
		virtual long long saveMesh(const std::string &meshId, const Mesh &mesh);
		virtual long long saveTexture(const std::string &textureId, const Texture &texture);
	};

} // namespace KMZ2Db

#endif // __KMZ2DB_KMZ2PGCONVERTER_H__
