/*
Copyright (c) 2012 Nutiteq

This file is part of KMZ2Db.
*/

#ifndef __KMZ2DB_KMZMODELLODTREEBUILDER_H__
#define __KMZ2DB_KMZMODELLODTREEBUILDER_H__

#include "Prerequisites.h"
#include "Processor.h"
#include "SpatialTree.h"
#include "Model.h"
#include "ModelLODTree.h"

#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>


namespace KMZ2Db
{
	using namespace NMLFramework;

	class KMZModelLODTreeBuilder : public Processor
	{

	public:

		typedef int ModelId;

		struct ModelInfo
		{
			long sourceId; // model info source id
			std::string globalId; // unique id among all models
			Matrix4 localTransform; // a local transform that will be baked into model when model is loaded
			std::string kmzFile;
			COLLADABU::URI modelUri;
			std::string folder;
			Vector3 mapPos; // model location in Web Mercator coordinate system. Z-coordinate denotes height in meters above sealevel
			double maxHeight; // maximum height of the model
			std::vector<Vector3> floorplan; // model floorplan points
			int materials;
			int vertices;
			int textures;

			ModelInfo() : sourceId(0), localTransform(Matrix4::IDENTITY), maxHeight(0), materials(0), vertices(0), textures(0) { }
		};

		typedef std::map<ModelId, ModelInfo> ModelInfoMap;

		class IWriter
		{
		public:
			virtual ~IWriter() { }
			virtual bool loadModel(const std::string &modelKey, Model &model) = 0;
			virtual long long saveModel(const std::string &modelKey, const Model &model) = 0;
			virtual long long saveMesh(const std::string &meshId, const Mesh &mesh) = 0;
			virtual long long saveTexture(const std::string &textureId, const Texture &texture) = 0;
		};

		struct Options
		{
			int lightingModel;
			int targetScreenSize;
			int maxModelVertices;
			int maxSubmeshVertices;
			int maxExtraFlattenVertices;
			int maxMergedColorVertices;
			int maxSingleTextureSize;
			int maxAtlasTextureSize;
			int maxRepeatedTexturePixels;
			bool squareTextureAtlas;
			double minSubmeshSize;
			double initialNodeSize;
			double finalNodeSize;

			Options() : lightingModel(1), targetScreenSize(256), maxModelVertices(-1), maxSubmeshVertices(65536), maxExtraFlattenVertices(256), maxMergedColorVertices(4096), maxSingleTextureSize(256), maxAtlasTextureSize(1024), maxRepeatedTexturePixels(4096), squareTextureAtlas(false), minSubmeshSize(0), initialNodeSize(2000.0), finalNodeSize(20.0) { }

			void setVariables(const boost::program_options::variables_map &vm)
			{
				lightingModel = vm["lighting-model"].as<int>();
				targetScreenSize = vm["target-screen-size"].as<int>();
				maxModelVertices = vm["max-model-vertices"].as<int>();
				maxSubmeshVertices = vm["max-submesh-vertices"].as<int>();
				maxExtraFlattenVertices = vm["max-extra-flatten-vertices"].as<int>();
				maxMergedColorVertices = vm["max-merged-color-vertices"].as<int>();
				maxSingleTextureSize = Texture::roundToPOT(vm["max-single-texture-size"].as<int>());
				maxAtlasTextureSize = vm["max-atlas-texture-size"].as<int>() > 0 ? Texture::roundToPOT(vm["max-atlas-texture-size"].as<int>()) : 0;
				maxRepeatedTexturePixels = vm["max-repeated-texture-pixels"].as<int>();
				squareTextureAtlas = vm["square-texture-atlas"].as<bool>();
				minSubmeshSize = vm["min-submesh-size"].as<double>();
				initialNodeSize = vm["initial-node-size"].as<double>();
				finalNodeSize = vm["final-node-size"].as<double>();
			}

			static boost::program_options::options_description getDescription()
			{
				namespace po = boost::program_options;
				static const Options opt;
				po::options_description desc("LOD tree options");
				desc.add_options()
					("lighting-model", po::value<int>()->default_value(opt.lightingModel), "lighting model (0=none, 1=directional sun)")
					("target-screen-size", po::value<int>()->default_value(opt.targetScreenSize), "target screen size")
					("max-model-vertices", po::value<int>()->default_value(opt.maxModelVertices), "maximum model vertices (drop model if more vertices)")
					("max-submesh-vertices", po::value<int>()->default_value(opt.maxSubmeshVertices), "maximum submesh vertices")
					("max-extra-flatten-vertices", po::value<int>()->default_value(opt.maxExtraFlattenVertices), "maximum extra vertices flattening may generate per mesh")
					("max-merged-color-vertices", po::value<int>()->default_value(opt.maxMergedColorVertices), "maximum merged color vertices")
					("max-single-texture-size", po::value<int>()->default_value(opt.maxSingleTextureSize), "maximum texture size")
					("max-atlas-texture-size", po::value<int>()->default_value(opt.maxAtlasTextureSize), "maximum texture atlas size (0=disable atlas generation)")
					("max-repeated-texture-pixels", po::value<int>()->default_value(opt.maxRepeatedTexturePixels), "maximum number of pixels in texture that can be repeated")
					("square-texture-atlas", po::value<bool>()->default_value(opt.squareTextureAtlas), "generate square texture atlases")
					("min-submesh-size", po::value<double>()->default_value(opt.minSubmeshSize), "minimum relative submesh size")
					("initial-node-size", po::value<double>()->default_value(opt.initialNodeSize), "initial LOD tree node size (meters)")
					("final-node-size", po::value<double>()->default_value(opt.finalNodeSize), "final LOD tree node size (meters)")
				;
				return desc;
			}
		};

		KMZModelLODTreeBuilder(const Options &options, IWriter &writer, ModelLODTree &oldModelLODTree, ModelInfoMap oldModelInfoMap, const Vector3 &mapPos) : mOptions(options), mWriter(writer), mOldModelLODTree(oldModelLODTree), mModelInfoMap(oldModelInfoMap), mMapPos(mapPos) { }

		/** Clear model references */
		void clear() { mModelInfoMap.clear(); mChangedModelIds.clear(); }

		/** Add model reference to model list, return model id */
		ModelId add(const ModelInfo &modelInfo) { ModelId modelId = (ModelId) mModelInfoMap.size() + 1; while (mModelInfoMap.count(modelId) > 0) modelId--; mModelInfoMap[modelId] = modelInfo; mChangedModelIds.insert(modelId); return modelId; }

		/** Update model with existing id */
		void update(ModelId modelId, const ModelInfo &modelInfo) { mModelInfoMap[modelId] = modelInfo; mChangedModelIds.insert(modelId); }

		/** Remove existing model with given id */
		void remove(ModelId modelId) { mModelInfoMap.erase(modelId); mChangedModelIds.insert(modelId); }

		/** Get all models */
		const ModelInfoMap &getAll() const { return mModelInfoMap; }

		/** Generate new model LOD tree */
		void buildModelLODTree(ModelLODTree &newModelLODTree);

		/** Options */
		static Options &getDefaultOptions() { return sDefaultOptions; }

	private:

		typedef std::set<ModelId> ModelIdSet;
		typedef SpatialTree<ModelId, 3> SpatialModelTree;
		typedef std::map<std::string, const ModelLODTree::Node *> NodeCache;
		typedef std::map<const SpatialModelTree::Node *, std::string> NodeIdMap;

		IWriter &mWriter;
		ModelLODTree mOldModelLODTree;
		Vector3 mMapPos;
		ModelInfoMap mModelInfoMap;
		ModelIdSet mChangedModelIds;

		const Options mOptions;
		static Options sDefaultOptions;

		std::string buildNodeIdMap(const SpatialModelTree::Node *spatialTreeNode, NodeIdMap &nodeIdMap) const;
		bool buildNodeCache(const ModelLODTree::Node &node, NodeCache &cache) const;
		void createNode(ModelLODTree::Node &node, SpatialModelTree::Node *spatialTreeNode, NodeCache &nodeCache, NodeIdMap &nodeIdMap);
		void loadNodeModels(ModelLODTree::Node &node, SpatialModelTree::Node *spatialTreeNode, NodeIdMap &nodeIdMap);
		void saveNodeAssets(ModelLODTree::Node &node);
		bool loadDAEModel(ModelInfo &modelInfo, Model &model, const Matrix4 &transform, bool loadTextures);
	};

} // namespace KMZ2Db

#endif // __KMZ2DB_KMZMODELLODTREEBUILDER_H__
