/*
Copyright (c) 2012 Nutiteq

This file is part of geojson2db.
*/

#ifndef __POI2SQLITE_GEOJSON2SQLITECONVERTER_H__
#define __POI2SQLITE_GEOJSON2SQLITECONVERTER_H__

#include "Prerequisites.h"
#include "Processor.h"
#include "GeoJsonModelLODTreeBuilder.h"

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

#include "rapidjson/document.h"

#include <sqlite3pp.h>


namespace GeoJson2Db
{
	using namespace NMLFramework;

	class GeoJson2SqliteConverter : public GeoJsonModelLODTreeBuilder::IWriter, public Processor
	{

	public:

		struct Options
		{
			double tileSize;
			double polyHeight;
			std::string folderProperty;
			std::vector<std::string> customProperties;

			Options() : tileSize(1000.0), polyHeight(12.0), folderProperty(), customProperties() { }

			void setVariables(const boost::program_options::variables_map &vm)
			{
				tileSize = vm["tile-size"].as<double>();
				polyHeight = vm["polygon-height"].as<double>();
				folderProperty = vm["folder-property"].as<std::string>();
				if (vm.count("custom-property"))
					customProperties = vm["custom-property"].as<std::vector<std::string> >();
				else
					customProperties.clear();
			}

			static boost::program_options::options_description getDescription()
			{
				namespace po = boost::program_options;
				static const Options opt;
				po::options_description desc("Converter options");
				desc.add_options()
					("tile-size", po::value<double>()->default_value(opt.tileSize), "tile size (meters)")
					("polygon-height", po::value<double>()->default_value(opt.polyHeight), "polygon height (meters)")
					("folder-property", po::value<std::string>()->default_value(opt.folderProperty), "name of the the property to store in ModelInfo 'folder' field")
					("custom-property", po::value<std::vector<std::string> >()->composing(), "name of the the custom property to store in ModelInfo field")
				;
				return desc;
			}
		};

		GeoJson2SqliteConverter(const std::vector<std::string>& inputFiles, sqlite3pp::database &db);
		virtual ~GeoJson2SqliteConverter();

		/** Convert KMZ file given by inputUri to multiple NMLPackage classes and store the result in spatialite database. */
		bool convert();

		/** Initialize db */
		static bool initializeDb(sqlite3pp::database &db);

		/** Options */
		static Options &getOptions() { return sOptions; }

	private:

		typedef GeoJsonModelLODTreeBuilder::ModelId ModelId;
		typedef GeoJsonModelLODTreeBuilder::ModelInfo ModelInfo;
		typedef std::pair<int, int> TileId;
		typedef std::map<ModelId, ModelInfo> ModelInfoMap;
		typedef std::map<TileId, ModelInfoMap> ModelTileMap;
		typedef std::map<TileId, boost::shared_ptr<GeoJsonModelLODTreeBuilder> > ModelLODTreeBuilderMap;

		const std::vector<std::string> &mInputFiles;
		boost::mutex mDBMutex;
		sqlite3pp::database &mDB;
		sqlite_int64 mModelLODTreeId;
		ModelTileMap mModelTileMap;
		ModelLODTreeBuilderMap mLODTreeBuilderMap;

		static Options sOptions;

		TileId getTileId(const Vector3 &mapPos) const;
		Vector3 calculateContourOrigin(const std::vector<Vector3> &contour) const;
		void processGeoJson(const rapidjson::Document &doc);
		void saveNMLModelLODTree(const NMLPackage::ModelLODTree &nmlLODTree);
		void saveModelInfoMap(sqlite_int64 modelLODTreeId, const ModelInfoMap &modelMap);
		void saveMapTile(sqlite_int64 modelLODTreeId, const Vector3 &mapPos, const Bounds3 &localBounds);

		virtual void saveNMLModelLODTreeNode(NMLPackage::ModelLODTreeNode &nmlLODTreeNode);

		static std::string getCustomPropertyField(const std::string &prop);
	};

} // namespace GeoJson2Db

#endif // __POI2SQLITE_GEOJSON2SQLITECONVERTER_H__
