/*
Copyright (c) 2012 Nutiteq

This file is part of geojson2db.
*/

#include "Prerequisites.h"
#include "GeoJson2SqliteConverter.h"
#include "TextureExporter.h"

#include "COLLADABUURI.h"

#include <iostream>

#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/bind.hpp>

#if defined(WIN32) && !defined(NDEBUG)
#	include <crtdbg.h> 
#endif

namespace po = boost::program_options;
namespace fs = boost::filesystem;

std::vector<std::string> find_files(const std::string & filesearch, const fs::path &parentpath = fs::path())
{
	std::vector<std::string> fileset;

	// Initialize path p (if necessary prepend with ./ so current dir is used) 
	fs::path p(parentpath / filesearch);
	if (!p.has_parent_path())
	{
		p = fs::path("./") / p;
	}
	p = fs::system_complete(p);

	// Initialize regex filter - use *.* as default if nothing is given in filesearch
	std::string f(!fs::is_directory(p) ? p.filename().string() : "*.*");

	// Make replacements: 1: Replace . with \. (i.e. escape it)  2: Replace any ? with .  3: Replace any * with .* 
	f = boost::regex_replace(f, boost::regex("(\\.)|(\\?)|(\\*)"), "(?1\\\\.)(?2.)(?3.*)", boost::match_default | boost::format_all);
	// Replace () []
	f = boost::regex_replace(f, boost::regex("(\\[)|(\\])|(\\()|(\\))"), "(?1\\\\\\[)(?2\\\\\\])(?3\\\\\\()(?4\\\\\\))", boost::match_default | boost::format_all);
	const boost::regex filter(f, boost::regex::icase); // TODO: icase is good for Windows-only

	fs::path dir(p.parent_path());
	if (fs::is_directory(dir))
	{
		// Iterate through contents (files/directories) of directory...
		for (fs::directory_iterator it(dir); it != fs::directory_iterator(); ++it)
		{
			// If directory, recurse
			if (fs::is_directory(*it))
			{
				if (it->path().filename().string().substr(0, 1) != ".")
				{
					std::vector<std::string> sub_fileset = find_files(filesearch, parentpath / it->path().filename());
					fileset.insert(fileset.end(), sub_fileset.begin(), sub_fileset.end());
				}
				continue;
			}

			// Match
			fs::path filepath = parentpath / it->path().filename();
			if (boost::regex_match(filepath.string(), filter))
				fileset.push_back(it->path().string());
		}
	}

	return fileset;
}

int geojson2sqlite(const std::vector<std::string> &inputFiles, const std::string &dbFile)
{
	// Prepare database
	boost::shared_ptr<sqlite3pp::database> db(new sqlite3pp::database(dbFile.c_str()));
	db->execute("PRAGMA encoding='UTF-8'");
	GeoJson2Db::GeoJson2SqliteConverter::initializeDb(*db);

	// Find files to convert
	std::vector<std::string> allResolvedFiles;
	for (size_t i = 0; i < inputFiles.size(); i++)
	{
		const std::string & inputFile = inputFiles[i];
		std::vector<std::string> resolvedFiles = find_files(inputFile);
		if (resolvedFiles.empty())
		{
			std::cerr << "Could not find " << inputFile << "!" << std::endl;
			return 1;
		}
		for (size_t j = 0; j < resolvedFiles.size(); j++)
		{
			allResolvedFiles.push_back(resolvedFiles[j]);
		}
	}

	// Convert
	GeoJson2Db::GeoJson2SqliteConverter converter(allResolvedFiles, *db);
	if (!converter.convert())
	{
		std::cerr << "Conversion failed!" << std::endl;
		return 1;
	}
	return 0;
}

int main(int argc, char* argv[]) 
{
	po::options_description options("General options");
	options.add_options()
		("append", "append models to existing database")
		("overwrite", "overwrite existing database")
		;

	options.add(GeoJson2Db::GeoJson2SqliteConverter::Options::getDescription());
	options.add(GeoJson2Db::GeoJsonModelLODTreeBuilder::Options::getDescription());

	po::options_description hidden("Hidden options");
	hidden.add_options()
		("dae2nml", "convert dae to nml")
		("output-file", po::value<std::string>(), "output file")
		("input-file", po::value<std::vector<std::string> >(), "input file")
		;

	po::options_description cmdline_options;
	cmdline_options.add(options).add(hidden);

	po::positional_options_description file_options;
	file_options.add("output-file", 1);
	file_options.add("input-file", -1);

	try
	{
		po::variables_map vm;
		store(po::command_line_parser(argc, argv).
			options(cmdline_options).positional(file_options).run(), vm);
		notify(vm);

		if (!(vm.count("output-file") && vm.count("input-file")))
		{
			std::cerr << "Usage: poi2sqlite [options] dbfile jsonpoifile1 [jsonpoifile2] ..." << std::endl;
			std::cerr << std::endl << options << std::endl;
			return -1;
		}

		std::string outputFile = vm["output-file"].as<std::string>();
		std::vector<std::string> inputFiles = vm["input-file"].as<std::vector<std::string> >();

		bool outputFileExists = fs::exists(outputFile);
		if (outputFileExists && !vm.count("append") && !vm.count("overwrite"))
		{
			std::cerr << "Database file already exists, please specify --append or --overwrite option." << std::endl;
			return -1;
		}
		if (vm.count("overwrite"))
			fs::remove(outputFile);

		GeoJson2Db::GeoJson2SqliteConverter::getOptions().setVariables(vm);
		GeoJson2Db::GeoJsonModelLODTreeBuilder::getOptions().setVariables(vm);

		return geojson2sqlite(inputFiles, outputFile);
	}
	catch (const std::exception &ex)
	{
		std::cerr << ex.what() << std::endl;
	}
	return -1;
}
