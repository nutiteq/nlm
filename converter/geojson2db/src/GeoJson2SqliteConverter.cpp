/*
Copyright (c) 2012 Nutiteq

This file is part of geojson2db.
*/

#include "GeoJson2SqliteConverter.h"
#include "Utils.h"
#include "WGS84Utils.h"

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>


namespace GeoJson2Db
{

	//--------------------------------------------------------------------
	GeoJson2SqliteConverter::Options GeoJson2SqliteConverter::sOptions;

	//--------------------------------------------------------------------
	GeoJson2SqliteConverter::GeoJson2SqliteConverter(const std::vector<std::string> &inputFiles, sqlite3pp::database &db)
		: mInputFiles(inputFiles), mDB(db), mModelLODTreeId(-1)
	{
	}
	
	//--------------------------------------------------------------------
	GeoJson2SqliteConverter::~GeoJson2SqliteConverter()
	{
	}

	//--------------------------------------------------------------------
	bool GeoJson2SqliteConverter::initializeDb(sqlite3pp::database &db)
	{
		std::string customFields;
		for (size_t i = 0; i < getOptions().customProperties.size(); i++)
		{
			std::string customProperty = getOptions().customProperties[i];
			customFields.append(getCustomPropertyField(customProperty)).append(" TEXT, ");
		}

		db.execute("PRAGMA page_size=4096"); // this should increase BLOB performance by 2x

		db.execute("CREATE TABLE IF NOT EXISTS Textures(id INTEGER NOT NULL, level INTEGER NOT NULL, nmltexture BLOB NOT NULL, nmltexture_sha1hash BLOB NOT NULL, PRIMARY KEY (id, level))");
		db.execute("CREATE TABLE IF NOT EXISTS Meshes(id INTEGER NOT NULL PRIMARY KEY, nmlmesh BLOB NOT NULL, nmlmesh_sha1hash BLOB NOT NULL)");
		db.execute("CREATE TABLE IF NOT EXISTS ModelLODTrees(id INTEGER NOT NULL PRIMARY KEY ASC, nmlmodellodtree BLOB NOT NULL, nmlmodellodtree_sha1hash BLOB NOT NULL)");
		db.execute(("CREATE TABLE IF NOT EXISTS ModelInfo(id INTEGER NOT NULL PRIMARY KEY ASC, modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, model_id INTEGER NOT NULL, mappos_x REAL NOT NULL, mappos_y REAL NOT NULL, groundheight REAL NOT NULL, folder TEXT, " + customFields + " UNIQUE (modellodtree_id, model_id))").c_str());
		db.execute("CREATE TABLE IF NOT EXISTS MapTiles(id INTEGER NOT NULL PRIMARY KEY ASC, modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, mappos_x REAL NOT NULL, mappos_y REAL NOT NULL, groundheight REAL NOT NULL, mapbounds_x0 REAL NOT NULL, mapbounds_y0 REAL NOT NULL, mapbounds_x1 REAL NOT NULL, mapbounds_y1 REAL NOT NULL)");
		db.execute("CREATE TABLE IF NOT EXISTS ModelLODTreeNodeTextures(modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, node_id INTEGER NOT NULL, local_id TEXT NOT NULL, texture_id INTEGER NOT NULL, level INTEGER NOT NULL, FOREIGN KEY(texture_id, level) REFERENCES Textures(id, level) ON DELETE CASCADE)");
		db.execute("CREATE TABLE IF NOT EXISTS ModelLODTreeNodeMeshes(modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, node_id INTEGER NOT NULL, local_id TEXT NOT NULL, mesh_id INTEGER NOT NULL REFERENCES Meshes(id) ON DELETE CASCADE, nmlmeshop BLOB NULL)");
		
		db.execute("CREATE INDEX IF NOT EXISTS Textures_id ON Textures(id)");
		db.execute("CREATE INDEX IF NOT EXISTS Textures_nmltexture_sha1hash ON Textures(nmltexture_sha1hash)"); // index for fast searching
		db.execute("CREATE INDEX IF NOT EXISTS Meshes_nmlmesh_sha1hash ON Meshes(nmlmesh_sha1hash)");
		db.execute("CREATE INDEX IF NOT EXISTS ModelLODTrees_nmlmodellodtree_sha1hash ON ModelLODTrees(nmlmodellodtree_sha1hash)");
		db.execute("CREATE INDEX IF NOT EXISTS ModelInfo_modellodtree_id ON ModelInfo(modellodtree_id)");
		db.execute("CREATE INDEX IF NOT EXISTS MapTiles_modellodtree_id ON MapTiles(modellodtree_id)");
		db.execute("CREATE INDEX IF NOT EXISTS ModelLODTreeNodeTextures_modellodtree_id ON ModelLODTreeNodeTextures(modellodtree_id)");
		db.execute("CREATE INDEX IF NOT EXISTS ModelLODTreeNodeMeshes_modellodtree_id ON ModelLODTreeNodeMeshes(modellodtree_id)");
		return true;
	}

	//--------------------------------------------------------------------
	bool GeoJson2SqliteConverter::convert()
	{
		// Group models into tiles and build list of model references
		mModelTileMap.clear();
		mLODTreeBuilderMap.clear();
		for (size_t i = 0; i < mInputFiles.size(); i++)
		{
			FILE *fp = fopen(mInputFiles[i].c_str(), "rb");
			if (fp == NULL)
			{
				reportError("convert", "Could not open input file!", _SEVERITY_ERROR);
				continue;
			}
			std::string data;
			while (!feof(fp))
			{
				char c = '\n';
				fread(&c, sizeof(c), 1, fp);
				data.append(1, c);
			}
			rapidjson::Document doc;
			doc.Parse<0>(data.c_str());
			if (doc.HasParseError())
			{
				reportError("convert", "Could not parse input file!", _SEVERITY_ERROR);
				continue;
			}
			processGeoJson(doc);
		}

		// Create DB lodtree records for each tile
		for (ModelLODTreeBuilderMap::const_iterator it = mLODTreeBuilderMap.begin(); it != mLODTreeBuilderMap.end(); it++)
		{
			TileId tileId = it->first;
			boost::shared_ptr<GeoJsonModelLODTreeBuilder> builder = it->second;

			reportProgress("convert", "Processing new tile");
			sqlite3pp::command insertStmt(mDB, "INSERT INTO ModelLODTrees(nmlmodellodtree, nmlmodellodtree_sha1hash) VALUES(X'', X'')");
			if (insertStmt.execute() != 0)
			{
				reportError("convert", "Unable to insert tree info into database", _SEVERITY_ERROR);
				return false;
			}
			mModelLODTreeId = mDB.last_insert_rowid();

			// Build model tree
			Vector3 mapPos;
			Bounds3 localBounds;
			NMLPackage::ModelLODTree nmlLODTree;
			builder->buildNMLModelLODTree(nmlLODTree, mapPos, localBounds);
			saveNMLModelLODTree(nmlLODTree);
			if (!getOptions().folderProperty.empty() || !getOptions().customProperties.empty())
			{
				saveModelInfoMap(mModelLODTreeId, mModelTileMap[tileId]);
			}
			saveMapTile(mModelLODTreeId, mapPos, localBounds);
		}
		return true;
	}

	//--------------------------------------------------------------------
	GeoJson2SqliteConverter::TileId GeoJson2SqliteConverter::getTileId(const Vector3 &mapPos) const
	{
		int x = (int) mapPos.x / getOptions().tileSize;
		int y = (int) mapPos.y / getOptions().tileSize;
		return std::make_pair(x, y);
	}

	//--------------------------------------------------------------------
	Vector3 GeoJson2SqliteConverter::calculateContourOrigin(const std::vector<Vector3> &contour) const
	{
		double x = 0, y = 0, z = std::numeric_limits<double>::infinity();
		for (size_t i = 0; i < contour.size(); i++)
		{
			x += contour[i].x / contour.size();
			y += contour[i].y / contour.size();
			z = std::min(z, contour[i].z);
		}
		return Vector3(x, y, z);
	}

	//--------------------------------------------------------------------
	void GeoJson2SqliteConverter::processGeoJson(const rapidjson::Document &doc)
	{
		for (size_t i = 0; i < doc["features"].Size(); i++)
		{
			const rapidjson::Value &feature = doc["features"][i];
			if (!feature.HasMember("geometry"))
				continue;
			const rapidjson::Value &geometry = feature["geometry"];
			std::string type = geometry["type"].GetString();
			if (!(type == "Polygon" || type == "MultiPolygon"))
				continue;
			const rapidjson::Value &coordinatesList = geometry["coordinates"];
			if (coordinatesList.Size() < 1)
				continue;
			const rapidjson::Value *coordinatesPtr;
			if (type == "MultiPolygon")
				coordinatesPtr = &coordinatesList[(rapidjson::SizeType) 0][(rapidjson::SizeType) 0];
			else
				coordinatesPtr = &coordinatesList[(rapidjson::SizeType) 0];
			const rapidjson::Value &coordinates = *coordinatesPtr;
			
			GeoJsonModelLODTreeBuilder::Contour contour;
			Vector3 center(0, 0, 0);
			for (size_t j = 0; j < coordinates.Size() - 1; j++) // polygons are closed, so drop last point
			{
				double lon = coordinates[j][(rapidjson::SizeType) 0].GetDouble();
				double lat = coordinates[j][(rapidjson::SizeType) 1].GetDouble();
				Vector3 mapPos = wgs84ToWebMercator(Vector3(lon, lat, 0));
				contour.push_back(mapPos);
				center += mapPos * (1.0 / coordinates.Size());
			}

			ModelInfo modelInfo;
			modelInfo.height = getOptions().polyHeight;
			modelInfo.contour = contour;
			if (!getOptions().folderProperty.empty())
			{
				const rapidjson::Value &property = feature["properties"][getOptions().folderProperty.c_str()];
				if (property.GetType() == rapidjson::kNumberType)
					modelInfo.folder = boost::lexical_cast<std::string>(property.GetInt());
				else if (property.GetType() == rapidjson::kStringType)
					modelInfo.folder = property.GetString();
			}
			for (size_t j = 0; j < getOptions().customProperties.size(); j++)
			{
				std::string customProperty = getOptions().customProperties[j];
				std::string field = getCustomPropertyField(customProperty);
				std::string value;
				const rapidjson::Value &property = feature["properties"][customProperty.c_str()];
				if (property.GetType() == rapidjson::kNumberType)
					value = boost::lexical_cast<std::string>(property.GetInt());
				else if (property.GetType() == rapidjson::kStringType)
					value = property.GetString();
				modelInfo.customPropertyMap[field] = value;
			}
			TileId tileId = getTileId(center);
			boost::shared_ptr<GeoJsonModelLODTreeBuilder> builder = mLODTreeBuilderMap[tileId];
			if (!builder)
			{
				builder.reset(new GeoJsonModelLODTreeBuilder(*this));
				mLODTreeBuilderMap[tileId] = builder;
			}
			ModelId modelId = builder->add(modelInfo);
			mModelTileMap[tileId][modelId] = modelInfo;
		}
	}

	//--------------------------------------------------------------------
	void GeoJson2SqliteConverter::saveNMLModelLODTreeNode(NMLPackage::ModelLODTreeNode &nmlLODTreeNode)
	{
		reportProgress("saveNMLModelLODTreeNode", "Saving NML LOD tree node");

		// Start new transaction
		sqlite3pp::transaction xct(mDB);

		// Save meshes
		for (int i = 0; i < nmlLODTreeNode.model().meshes_size(); i++)
		{
			const NMLPackage::Mesh &nmlMesh = nmlLODTreeNode.model().meshes(i);
			sqlite_int64 source_id = -1;
			std::string meshString;
			nmlMesh.SerializeToString(&meshString);
			std::string meshHash = calculateSHA1(meshString);

			// Save NML mesh if not saved already
			sqlite3pp::query meshQry(mDB, "SELECT id FROM Meshes WHERE nmlmesh_sha1hash=:nmlmesh_sha1hash AND nmlmesh=:nmlmesh");
			meshQry.bind(":nmlmesh", meshString.data(), meshString.size(), false);
			meshQry.bind(":nmlmesh_sha1hash", meshHash.data(), meshHash.size(), false);
			sqlite3pp::query::iterator qit = meshQry.begin();
			if (qit != meshQry.end())
			{
				source_id = qit->get<sqlite_int64>(0);
			}
			else
			{
				sqlite3pp::command meshStmt(mDB, "INSERT INTO Meshes(nmlmesh, nmlmesh_sha1hash) VALUES(:nmlmesh, :nmlmesh_sha1hash)");
				meshStmt.bind(":nmlmesh", meshString.data(), meshString.size(), false);
				meshStmt.bind(":nmlmesh_sha1hash", meshHash.data(), meshHash.size(), false);
				if (meshStmt.execute() != 0)
				{
					reportError("saveNMLModelLODTreeNode", "Unable to insert mesh info into database", _SEVERITY_ERROR);
					return;
				}
				source_id = mDB.last_insert_rowid();
			}

			// Create mesh binding
			sqlite3pp::command bindingStmt(mDB, "INSERT INTO ModelLODTreeNodeMeshes(modellodtree_id, node_id, local_id, mesh_id) VALUES(:modellodtree_id, :node_id, :local_id, :mesh_id)");
			bindingStmt.bind(":modellodtree_id", mModelLODTreeId);
			bindingStmt.bind(":node_id", nmlLODTreeNode.id());
			bindingStmt.bind(":local_id", nmlMesh.id().c_str(), false);
			bindingStmt.bind(":mesh_id", source_id);
			if (bindingStmt.execute() != 0)
			{
				reportError("saveNMLModelLODTreeNode", "Unable to insert mesh binding into database", _SEVERITY_ERROR);
				return;
			}
		}

		// Save texture sets in textures table
		for (int i = 0; i < nmlLODTreeNode.model().textures_size(); i++)
		{
			const NMLPackage::Texture &nmlTexture = nmlLODTreeNode.model().textures(i);
			sqlite_int64 source_id = -1;
			int level = 0;
			std::string textureString;
			nmlTexture.SerializeToString(&textureString);
			std::string textureHash = calculateSHA1(textureString);

			// Save NML texture if not saved already
			sqlite3pp::query textureQry(mDB, "SELECT id, level FROM Textures WHERE nmltexture_sha1hash=:nmltexture_sha1hash AND nmltexture=:nmltexture");
			textureQry.bind(":nmltexture", textureString.data(), textureString.size(), false);
			textureQry.bind(":nmltexture_sha1hash", textureHash.data(), textureHash.size(), false);
			sqlite3pp::query::iterator qit = textureQry.begin();
			if (qit != textureQry.end())
			{
				source_id = qit->get<sqlite_int64>(0);
				level = qit->get<int>(1);
			}
			else
			{
				boost::lock_guard<boost::mutex> lock(mDBMutex);
				source_id = sqlite3pp::query(mDB, "SELECT MAX(id) FROM Textures").begin()->get<sqlite_int64>(0) + 1;
				sqlite3pp::command textureStmt(mDB, "INSERT INTO Textures(id, level, nmltexture, nmltexture_sha1hash) VALUES(:id, :level, :nmltexture, :nmltexture_sha1hash)");
				textureStmt.bind(":id", source_id);
				textureStmt.bind(":level", level);
				textureStmt.bind(":nmltexture", textureString.data(), textureString.size(), false);
				textureStmt.bind(":nmltexture_sha1hash", textureHash.data(), textureHash.size(), false);
				if (textureStmt.execute() != 0)
				{
					reportError("saveNMLModelLODTreeNode", "Unable to insert texture info into database", _SEVERITY_ERROR);
					return;
				}
			}

			// Create texture binding
			sqlite3pp::command bindingStmt(mDB, "INSERT INTO ModelLODTreeNodeTextures(modellodtree_id, node_id, local_id, texture_id, level) VALUES(:modellodtree_id, :node_id, :local_id, :texture_id, :level)");
			bindingStmt.bind(":modellodtree_id", mModelLODTreeId);
			bindingStmt.bind(":node_id", nmlLODTreeNode.id());
			bindingStmt.bind(":local_id", nmlTexture.id().c_str(), false);
			bindingStmt.bind(":texture_id", source_id);
			bindingStmt.bind(":level", level);
			if (bindingStmt.execute() != 0)
			{
				reportError("saveNMLModelLODTreeNode", "Unable to insert texture binding into database", _SEVERITY_ERROR);
				return;
			}
		}

		// Success, commit changes to db
		xct.commit();

		// We have saved meshes/textures, so remove them from node to keep node memory footprint small
		nmlLODTreeNode.mutable_model()->mutable_meshes()->Clear();
		nmlLODTreeNode.mutable_model()->mutable_textures()->Clear();
	}

	//--------------------------------------------------------------------
	void GeoJson2SqliteConverter::saveNMLModelLODTree(const NMLPackage::ModelLODTree &nmlLODTree)
	{
		reportProgress("saveNMLModelLODTree", "Saving model LOD tree");

		// Serialize tree
		std::string lodTreeString;
		nmlLODTree.SerializeToString(&lodTreeString);
		std::string lodTreeHash = calculateSHA1(lodTreeString);

		// Start new transaction
		sqlite3pp::transaction xct(mDB);

		// Save model LOD tree
		sqlite3pp::command deleteStmt(mDB, "DELETE FROM ModelLODTrees WHERE nmlmodellodtree_sha1hash=:nmlmodellodtree_sha1hash AND nmlmodellodtree=:nmlmodellodtree");
		deleteStmt.bind(":nmlmodellodtree", lodTreeString.data(), lodTreeString.size(), false);
		deleteStmt.bind(":nmlmodellodtree_sha1hash", lodTreeHash.data(), lodTreeHash.size(), false);
		if (deleteStmt.execute() != 0)
		{
			reportError("saveNMLModelLODTree", "Unable to delete old model lod tree from database", SEVERITY_WARNING);
		}

		// Save LOD tree, by updating existing empty record
		sqlite3pp::command updateStmt(mDB, "UPDATE ModelLODTrees SET nmlmodellodtree=:nmlmodellodtree, nmlmodellodtree_sha1hash=:nmlmodellodtree_sha1hash WHERE id=:id");
		updateStmt.bind(":nmlmodellodtree", lodTreeString.data(), lodTreeString.size(), false);
		updateStmt.bind(":nmlmodellodtree_sha1hash", lodTreeHash.data(), lodTreeHash.size(), false);
		updateStmt.bind(":id", mModelLODTreeId);
		if (updateStmt.execute() != 0)
		{
			reportError("saveNMLModelLODTree", "Unable to update tree info in database", _SEVERITY_ERROR);
			return;
		}

		// Success, commit changes to db
		xct.commit();
	}

	//--------------------------------------------------------------------
	void GeoJson2SqliteConverter::saveModelInfoMap(sqlite_int64 modelLODTreeId, const ModelInfoMap &modelInfoMap)
	{
		std::string customTags;
		std::string customFields;
		for (size_t i = 0; i < getOptions().customProperties.size(); i++)
		{
			std::string customProperty = getOptions().customProperties[i];
			std::string field = getCustomPropertyField(customProperty);
			customTags.append(", :" + field);
			customFields.append(", " + field);
		}

		// Start new transaction
		sqlite3pp::transaction xct(mDB);

		// Delete existing duplicate model info
		boost::lock_guard<boost::mutex> lock(mDBMutex);
		sqlite3pp::command infoDeleteStmt(mDB, "DELETE FROM ModelInfo WHERE modellodtree_id=:modellodtree_id");
		infoDeleteStmt.bind(":modellodtree_id", modelLODTreeId);
		if (infoDeleteStmt.execute() != 0)
		{
			reportError("saveModelInfoMap", "Unable to delete old model info from database", _SEVERITY_ERROR);
			return;
		}

		// Save model info
		for (ModelInfoMap::const_iterator it = modelInfoMap.begin(); it != modelInfoMap.end(); it++)
		{
			int modelId = it->first;
			const ModelInfo &modelInfo = it->second;

			// Insert new info record
			Vector3 mapPos = calculateContourOrigin(modelInfo.contour);
			sqlite3pp::command infoInsertStmt(mDB, ("INSERT INTO ModelInfo(modellodtree_id, model_id, mappos_x, mappos_y, groundheight, folder" + customFields + ") VALUES(:modellodtree_id, :model_id, :mappos_x, :mappos_y, :groundheight, :folder" + customTags + ")").c_str());
			infoInsertStmt.bind(":modellodtree_id", modelLODTreeId);
			//infoInsertStmt.bind(":global_id", modelInfo.id.c_str(), false);
			infoInsertStmt.bind(":model_id", modelId);
			infoInsertStmt.bind(":folder", modelInfo.folder.c_str(), false);
			infoInsertStmt.bind(":mappos_x", mapPos.x);
			infoInsertStmt.bind(":mappos_y", mapPos.y);
			infoInsertStmt.bind(":groundheight", mapPos.z);
			for (std::map<std::string, std::string>::const_iterator it = modelInfo.customPropertyMap.begin(); it != modelInfo.customPropertyMap.end(); it++)
			{
				std::string customTag = ":" + it->first;
				infoInsertStmt.bind(customTag.c_str(), it->second.c_str());
			}
			if (infoInsertStmt.execute() != 0)
			{
				reportError("saveModelInfoMap", "Unable to insert model info into database", _SEVERITY_ERROR);
				return;
			}
		}

		// Success, commit changes to db
		xct.commit();
	}

	//--------------------------------------------------------------------
	void GeoJson2SqliteConverter::saveMapTile(sqlite_int64 modelLODTreeId, const Vector3 &mapPos, const Bounds3 &localBounds)
	{
		reportProgress("saveMapTile", "Saving map tile");

		// Start new transaction
		sqlite3pp::transaction xct(mDB);

		// Delete existing duplicate tile (based on location)
		// TODO: delete also referenced ModelLODTree that correspond to deleted MapTiles?
		boost::lock_guard<boost::mutex> lock(mDBMutex);
		sqlite3pp::command deleteStmt(mDB, "DELETE FROM MapTiles WHERE mappos_x=:mappos_x AND mappos_y=:mappos_y AND groundheight=:groundheight AND modellodtree_id=:modellodtree_id");
		deleteStmt.bind(":mappos_x", mapPos.x);
		deleteStmt.bind(":mappos_y", mapPos.y);
		deleteStmt.bind(":groundheight", mapPos.z);
		deleteStmt.bind(":modellodtree_id", modelLODTreeId);
		if (deleteStmt.execute() != 0)
		{
			reportError("saveMapTile", "Unable to delete old map tile from database", SEVERITY_WARNING);
		}

		// Save tile
		Matrix4 globalTransform = internalWgs84Transformation(webMercatorToWgs84(mapPos));
		Bounds3 mapBounds(globalTransform * localBounds.min, globalTransform * localBounds.max);
		sqlite3pp::command insertStmt(mDB, "INSERT INTO MapTiles(modellodtree_id, mappos_x, mappos_y, groundheight, mapbounds_x0, mapbounds_y0, mapbounds_x1, mapbounds_y1) VALUES(:modellodtree_id, :mappos_x, :mappos_y, :groundheight, :mapbounds_x0, :mapbounds_y0, :mapbounds_x1, :mapbounds_y1)");
		insertStmt.bind(":modellodtree_id", modelLODTreeId);
		insertStmt.bind(":mappos_x", mapPos.x);
		insertStmt.bind(":mappos_y", mapPos.y);
		insertStmt.bind(":groundheight", mapPos.z);
		insertStmt.bind(":mapbounds_x0", mapBounds.min.x);
		insertStmt.bind(":mapbounds_y0", mapBounds.min.y);
		insertStmt.bind(":mapbounds_x1", mapBounds.max.x);
		insertStmt.bind(":mapbounds_y1", mapBounds.max.y);
		if (insertStmt.execute() != 0)
		{
			reportError("saveMapTile", "Unable to insert map tile into database", _SEVERITY_ERROR);
			return;
		}

		// Success, commit changes to db
		xct.commit();
	}

	//--------------------------------------------------------------------
	std::string GeoJson2SqliteConverter::getCustomPropertyField(const std::string &prop)
	{
		std::string field = prop;
		std::replace(field.begin(), field.end(), ' ', '_');
		std::replace(field.begin(), field.end(), '-', '_');
		return field;
	}

} // namespace POI2Sqlite
